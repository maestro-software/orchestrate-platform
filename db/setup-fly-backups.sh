#!/bin/bash

b2_bucket="$1"
b2_account="$2"
b2_key="$3"

b2_config="b2-backup"
rclone_config_path=/tmp/rclone.conf
cat <<EOF > $rclone_config_path
[$b2_config]
type = b2
account = $b2_account
key = $b2_key
EOF

fly machine run . --dockerfile backup.Dockerfile \
    --schedule daily \
    --env B2_CONFIG=$b2_config \
    --env B2_BUCKET=$b2_bucket \
    --env BACKUP_RETENTION_DAYS=30 \
    --app orchestrate-api \
    --region syd  \
    --restart on-fail \
    --metadata role=db-backup \
    --file-local /root/.config/rclone/rclone.conf=$rclone_config_path
