# Specify base image
FROM debian:bullseye-slim

# Set build arguments for versions
ARG PG_DUMP_VERSION="16"

# Set environment variables for runtime
ENV DATABASE_URL= \
    B2_CONFIG= \
    B2_BUCKET= \
    BACKUP_RETENTION_DAYS="30"

# Install dependencies and tools
RUN apt-get update && apt-get install -y --no-install-recommends \
    python3 \
    rclone \
    ca-certificates \
    time \
    gnupg \
    postgresql-common \
    && /usr/share/postgresql-common/pgdg/apt.postgresql.org.sh -y \
    && apt-get install -y postgresql-client-${PG_DUMP_VERSION} \
    && apt-get remove -y gnupg postgresql-common \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh

# Set entrypoint
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
