# Orchestrate Platform

This project aims to provide a management system for community music groups to
use for asset, member, ensemble and performance records. It was born out of the
desire for the [Leeming Area Community Bands](http://www.lacb.org.au/) to have
a proper asset register and has since evolved into a full on management system
for the band as well as other groups.

## Get started

A standard development environment uses VSCode on Ubuntu (WSL if on Windows).
To begin, clone the repository and open it in VSCode.

### VSCode

Install the following extensions:

-   C#
-   Angular Language Service
-   EditorConfig

### API

-   Install [Postgresql](https://www.postgresql.org/download/)
-   Install [.NET Core SDK 9.0](https://dotnet.microsoft.com/download/dotnet/9.0)
    -   Install `dotnet tool install --global dotnet-ef --version 9.0`
-   Start postgres: `sudo service postgresql start`
-   Set a password on the default postgres user:
    -   `sudo su - postgres`
    -   `psql -c "alter user postgres with password 'postgres'"`
    -   `exit`
-   Optional: Install and set up [pgAdmin4](https://www.pgadmin.org/download/)
    -   If you have a database dump to restore do it here
-   Copy `api/OrchestrateApi/appsettings.json` to `appsettings.Development.json` and fill out the empty fields
-   `dotnet run api/OrchestrateApi` to start the development server
-   Create a global admin user and setup required table data: `curl -X POST http://localhost:5000/first-run -H 'Content-Type: application/json' -d '{"email":"your@email.here","password":"whatever you like"}'`

### Web Client

-   Install the latest [Node 22](https://nodejs.org/en/) (for Ubuntu use [NodeSource](https://github.com/nodesource/distributions/blob/master/README.md#installation-instructions))
-   Install NPM packages `cd web-client; npm install`
-   Copy `app-config.template.json` to `app-config.json`, removing the comments
-   Create file `web-client/src/app/dal/breeze-metadata.json` with contents `{}`
-   Run `npm run start` to start the development server
-   Access the application at `http://localhost:5200/`

## Run Tests

### API

-   Copy `api/OrchestrateApi.Tests/appsettings.json` to `appsettings.Testing.json` and alter `DATABASE_URL` for your development environment
-   Run `dotnet test api/OrchestrateApi.Tests`

### Web Client

-   Enter the `web-client` directory
-   Run `npm run test`
