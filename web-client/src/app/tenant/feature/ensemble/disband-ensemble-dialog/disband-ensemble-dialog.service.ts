import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AppConfig } from "app/common/app-config";
import { Ensemble } from "app/dal/model/ensemble";
import { switchMap } from "rxjs";

export interface DisbandImpactDto {
    membershipEndedMemberNames: string[];
    removedFromMailingLists: string[];
}

@Injectable()
export class DisbandEnsembleDialogService {
    public constructor(
        private httpClient: HttpClient,
        private appConfig: AppConfig,
    ) {}

    public getImpact(ensemble: Ensemble) {
        return this.appConfig
            .serverEndpoint(`disband-ensemble-dialog/${ensemble.id}`)
            .pipe(switchMap((url) => this.httpClient.get<DisbandImpactDto>(url)));
    }

    public disbandEnsemble(ensemble: Ensemble) {
        return this.appConfig
            .serverEndpoint(`disband-ensemble-dialog/${ensemble.id}`)
            .pipe(switchMap((url) => this.httpClient.post(url, {})));
    }
}
