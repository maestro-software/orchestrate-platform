import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatCardModule } from "@angular/material/card";
import { MatDividerModule } from "@angular/material/divider";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatIconModule } from "@angular/material/icon";
import { MatTooltipModule } from "@angular/material/tooltip";
import { AgChartsModule } from "ag-charts-angular";
import { AppAuthModule } from "app/auth/auth.module";
import { AppBadgeModule } from "app/common-ux/badge/badge.module";
import { AppButtonModule } from "app/common-ux/button/button.module";
import { AppChipSelectionModule } from "app/common-ux/chip-selection/chip-selection.module";
import { AppDataGridModule } from "app/common-ux/data-grid/data-grid.module";
import { AppDialogModule } from "app/common-ux/dialog/dialog.module";
import { AppFormModule } from "app/common-ux/form/form.module";
import { AppPageWrapperModule } from "app/common-ux/page-wrapper/page-wrapper.module";
import { AppSaveCancelButtonsModule } from "app/common-ux/save-cancel-buttons/save-cancel-buttons.module";
import { DisbandEnsembleDialogComponent } from "./disband-ensemble-dialog/disband-ensemble-dialog.component";
import { EditEnsembleDialogComponent } from "./edit-ensemble-dialog/edit-ensemble-dialog.component";
import { EnsemblePageComponent } from "./ensemble-page/ensemble-page.component";
import { EnsemblePerformancePanelComponent } from "./ensemble-performance-panel/ensemble-performance-panel.component";
import { AppEnsembleRoutingModule } from "./ensemble-routing.module";
import { EnsemblesPageComponent } from "./ensembles-page/ensembles-page.component";

@NgModule({
    imports: [
        CommonModule,
        MatCardModule,
        MatDividerModule,
        MatIconModule,
        MatExpansionModule,
        MatTooltipModule,
        AgChartsModule,
        AppEnsembleRoutingModule,
        AppAuthModule,
        AppDataGridModule,
        AppPageWrapperModule,
        AppChipSelectionModule,
        AppButtonModule,
        AppFormModule,
        AppSaveCancelButtonsModule,
        AppDialogModule,
        AppBadgeModule,
    ],
    declarations: [
        EnsemblesPageComponent,
        EnsemblePageComponent,
        EditEnsembleDialogComponent,
        DisbandEnsembleDialogComponent,
        EnsemblePerformancePanelComponent,
    ],
})
export class AppEnsembleModule {}
