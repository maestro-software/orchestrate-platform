import { GridIconRenderer } from "app/common-ux/data-grid/grid-icon-renderer";
import {
    EnsembleMembershipType,
    EnsembleMembershipTypeMetadata,
} from "app/dal/model/ensemble-membership";

export class EnsembleMembershipTypeIconRenderer extends GridIconRenderer<EnsembleMembershipType> {
    protected icon(value: EnsembleMembershipType): string {
        return EnsembleMembershipTypeMetadata[value].icon;
    }
    protected tooltip(value: EnsembleMembershipType): string {
        return EnsembleMembershipTypeMetadata[value].label;
    }
}
