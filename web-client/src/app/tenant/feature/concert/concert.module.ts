import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { MatCardModule } from "@angular/material/card";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatListModule } from "@angular/material/list";
import { MatSelectModule } from "@angular/material/select";
import { AppAuthModule } from "app/auth/auth.module";
import { AppBadgeModule } from "app/common-ux/badge/badge.module";
import { AppButtonModule } from "app/common-ux/button/button.module";
import { AppDataGridModule } from "app/common-ux/data-grid/data-grid.module";
import { AppDialogModule } from "app/common-ux/dialog/dialog.module";
import { AppFormModule } from "app/common-ux/form/form.module";
import { AppPageWrapperModule } from "app/common-ux/page-wrapper/page-wrapper.module";
import { AppSaveCancelButtonsModule } from "app/common-ux/save-cancel-buttons/save-cancel-buttons.module";
import { AppStorageModule } from "app/storage/storage.module";
import { AppMemberWidgetModule } from "app/tenant/feature/member/member-widget.module";
import { AppScoreWidgetModule } from "app/tenant/feature/score/score-widget.module";
import { ConcertPageComponent } from "./concert-page/concert-page.component";
import { AppConcertRoutingModule } from "./concert-routing.module";
import { ConcertService } from "./concert.service";
import { ConcertsPageComponent } from "./concerts-page/concerts-page.component";
import { CreateConcertDialogComponent } from "./create-concert-dialog/create-concert-dialog.component";
import { CreatePerformanceDialogComponent } from "./create-performance-dialog/create-performance-dialog.component";
import { EditConcertPageComponent } from "./edit-concert-page/edit-concert-page.component";
import { EditConcertComponent } from "./edit-concert/edit-concert.component";
import { EditPerformanceComponent } from "./edit-performance/edit-performance.component";
import { PerformancePanelComponent } from "./performance-panel/performance-panel.component";
import { PerformanceService } from "./performance.service";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,

        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        MatSelectModule,
        MatCardModule,
        MatExpansionModule,
        MatListModule,

        AppConcertRoutingModule,
        AppScoreWidgetModule,
        AppMemberWidgetModule,
        AppPageWrapperModule,
        AppBadgeModule,
        AppDataGridModule,
        AppAuthModule,
        AppButtonModule,
        AppSaveCancelButtonsModule,
        AppFormModule,
        AppDialogModule,
        AppStorageModule,
    ],
    declarations: [
        ConcertsPageComponent,
        ConcertPageComponent,
        PerformancePanelComponent,
        EditConcertPageComponent,
        EditConcertComponent,
        EditPerformanceComponent,
        CreateConcertDialogComponent,
        CreatePerformanceDialogComponent,
    ],
    providers: [
        ConcertService,
        PerformanceService,
    ],
})
export class AppConcertModule {}
