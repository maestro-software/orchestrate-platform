import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FeaturePermission } from "app/auth/feature-permission";
import { AppRoutes } from "app/common/app-routing-types";
import { helpPages } from "app/help/route/help-list";
import { ConcertPageComponent } from "./concert-page/concert-page.component";
import { ConcertsPageComponent } from "./concerts-page/concerts-page.component";
import { EditConcertPageComponent } from "./edit-concert-page/edit-concert-page.component";

const concertRoutes: AppRoutes = [
    {
        path: "",
        component: ConcertsPageComponent,
        data: {
            title: "Concerts",
            icon: "event",
            restrictedToPermission: FeaturePermission.ConcertRead,
            help: [helpPages.concerts],
        },
    },
    {
        path: ":concertId",
        component: ConcertPageComponent,
        data: {
            icon: "event",
            restrictedToPermission: FeaturePermission.ConcertRead,
            help: [helpPages.concerts],
        },
    },
    {
        path: ":concertId/edit",
        component: EditConcertPageComponent,
        data: {
            icon: "event",
            restrictedToPermission: FeaturePermission.ConcertWrite,
            help: [helpPages.concerts],
        },
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(concertRoutes),
    ],
    exports: [
        RouterModule,
    ],
})
export class AppConcertRoutingModule {}
