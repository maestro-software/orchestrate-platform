import { Component, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { ColDef, IRowNode, ValueGetterParams } from "ag-grid-community";
import { DataGridComponent } from "app/common-ux/data-grid/data-grid.component";
import { buildEditColumn } from "app/common-ux/data-grid/grid-edit-column";
import { wrapTextColDef } from "app/common-ux/data-grid/grid-wrap-column";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { Score } from "app/dal/model/score";
import { Observable, ReplaySubject } from "rxjs";
import { map, switchMap, tap } from "rxjs/operators";
import { EditScoreDialogComponent } from "../edit-score-dialog/edit-score-dialog.component";
import { ScoreCategoryIconRenderer } from "../score-table/score-category-icon-renderer";
import { ScoreService } from "../score.service";

@Component({
    selector: "app-select-scores",
    templateUrl: "./select-scores.component.html",
    standalone: false,
})
export class SelectScoresComponent {
    @Input() public selectedScoreIds?: number[];
    @Output() public selectedScoreIdsChange = new EventEmitter<number[]>();

    @ViewChild(DataGridComponent)
    public set dataGrid(v: DataGridComponent<Score>) {
        this.dataGrid$.next(v);
    }
    private dataGrid$ = new ReplaySubject<DataGridComponent<Score>>(1);

    public scores$: Observable<Score[]>;
    public columns$: Observable<ColDef<Score>[]>;

    public constructor(
        private scoreService: ScoreService,
        private dialogService: DialogService,
    ) {
        this.scores$ = scoreService.getAll();
        this.columns$ = this.dataGrid$.pipe(map((dataGrid) => this.getColumns(dataGrid)));
    }

    public addScore = () =>
        this.scoreService.create().pipe(
            switchMap((score) => this.dialogService.open(EditScoreDialogComponent, score)),
            tap((score) => {
                this.selectedScoreIds = [...(this.selectedScoreIds ?? []), score.id];
            }),
        );

    public editScore = (score: Score) => this.dialogService.open(EditScoreDialogComponent, score);

    public scoreToId(score: Score) {
        return score.id;
    }

    private getColumns(dataGrid: DataGridComponent<Score>): ColDef[] {
        return [
            {
                cellRenderer: ScoreCategoryIconRenderer,
                headerName: "",
                field: "category",
                width: 88,
                cellStyle: { padding: "0", "padding-left": "1rem" },
            },
            {
                headerName: "Title",
                valueGetter: (params: ValueGetterParams<Score>) => params.data?.formattedName ?? "",
                sort: "asc",
                comparator: (
                    titleA: string,
                    titleB: string,
                    nodeA: IRowNode<Score>,
                    nodeB: IRowNode<Score>,
                ) => {
                    const aIsSelected = this.selectedScoreIds!.includes(nodeA.data!.id);
                    const bIsSelected = this.selectedScoreIds!.includes(nodeB.data!.id);

                    if (aIsSelected && !bIsSelected) {
                        return -1;
                    } else if (bIsSelected && !aIsSelected) {
                        return 1;
                    } else {
                        return titleA.localeCompare(titleB);
                    }
                },
                flex: 1,
                minWidth: 300,
                ...wrapTextColDef(),
            },
            buildEditColumn<any>(dataGrid),
        ];
    }
}
