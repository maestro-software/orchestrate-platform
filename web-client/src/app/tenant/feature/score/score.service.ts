import { Injectable } from "@angular/core";
import { BreezeEntityService } from "app/dal/breeze-domain.service";
import { Score, ScoreBreezeModel, ScoreCategory } from "app/dal/model/score";
import { EntityQuery, Predicate } from "breeze-client";
import { switchMap } from "rxjs/operators";

export interface ScoreMetadataDto {
    score: Score;
    playCount: number;
    attachmentCount: number;
}

@Injectable({
    providedIn: "root",
})
export class ScoreService extends BreezeEntityService<Score> {
    protected readonly breezeModel = ScoreBreezeModel;

    public create() {
        return this.breezeService.create(ScoreBreezeModel, {});
    }

    public getAllWithMetadata() {
        return this.getScoresWithMetadata();
    }

    public getScoreCountInLibrary() {
        return this.breezeService.getCountByPredicate(
            ScoreBreezeModel,
            new Predicate("inLibrary", "==", true),
        );
    }

    public getScoreByCategoryWithMetadata(category: ScoreCategory[]) {
        return this.getScoresWithMetadata((q) => {
            // A bit of a hack for now, will need to revisit with #69
            if (category.length === 1) {
                return q.where(
                    new Predicate(
                        "score.inLibrary",
                        "==",
                        category[0] === ScoreCategory.MusicLibrary,
                    ),
                );
            } else {
                return q;
            }
        });
    }

    private getScoresWithMetadata(customiseQuery: (q: EntityQuery) => EntityQuery = (q) => q) {
        return this.breezeService.entityManager$.pipe(
            switchMap(async (em) => {
                const query = customiseQuery(new EntityQuery("ScoresWithMetadata"));
                const queryResult = await em.executeQuery(query);
                return queryResult.results as ScoreMetadataDto[];
            }),
        );
    }
}
