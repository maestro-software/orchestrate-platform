import { ChangeDetectionStrategy, Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";
import { ICellRendererParams } from "ag-grid-community";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { ScorePerformanceListDialogComponent } from "../score-performance-list-dialog/score-performance-list-dialog.component";
import { ScoreMetadataDto } from "../score.service";

@Component({
    template: `
        <div
            *ngIf="scoreMetadata"
            class="play-count-container"
        >
            {{ scoreMetadata.playCount }}
            <button
                *ngIf="scoreMetadata.playCount > 0"
                mat-icon-button
                (click)="openPerformanceList(scoreMetadata)"
                title="View performances"
            >
                <mat-icon>search</mat-icon>
            </button>
        </div>
    `,
    styles: [
        `
            .play-count-container {
                display: flex;
                align-items: center;
            }

            button {
                margin-top: -1px;
                margin-left: auto;
            }
        `,
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: false,
})
export class ScorePlayCountRendererComponent implements ICellRendererAngularComp {
    public scoreMetadata?: ScoreMetadataDto;

    public constructor(private dialogService: DialogService) {}

    public openPerformanceList(scoreMetadata: ScoreMetadataDto) {
        this.dialogService.open(ScorePerformanceListDialogComponent, scoreMetadata.score, {
            autoFocus: false,
        });
    }

    public agInit(params: ICellRendererParams): void {
        this.scoreMetadata = params.data as ScoreMetadataDto;
    }

    public refresh(): boolean {
        return false;
    }
}
