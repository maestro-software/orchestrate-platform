import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { FeaturePermission } from "app/auth/feature-permission";
import { BaseDialog } from "app/common-ux/dialog/base-dialog";
import { Score } from "app/dal/model/score";
import {
    EntityAttachmentMetadata,
    EntityAttachmentSensitivity,
} from "app/storage/entity-attachment-metadata";
import { EntityAttachmentsDialogOptions } from "app/storage/entity-attachments-dialog/entity-attachments-dialog.component";

@Component({
    selector: "app-score-attachments-dialog",
    template: `<app-entity-attachments-dialog [options]="options"></app-entity-attachments-dialog>`,
    standalone: false,
})
export class ScoreAttachmentsDialogComponent extends BaseDialog<Score, EntityAttachmentMetadata[]> {
    public readonly dialogName = "ScoreAttachmentDialog";

    public options: EntityAttachmentsDialogOptions;

    public constructor(
        dialogRef: MatDialogRef<ScoreAttachmentsDialogComponent>,
        @Inject(MAT_DIALOG_DATA) score: Score,
    ) {
        super(dialogRef);
        this.options = {
            storageStrategyName: "ScoreAttachment",
            entityId: score.id,
            defaultSensitivity: EntityAttachmentSensitivity.Sensitive,
            entityFormattedText: score.formattedName,
            editPermission: FeaturePermission.ScoreWrite,
            dialogClose$: this.dialogClose$,
            setDisableClose: (isDisabled) => (dialogRef.disableClose = isDisabled),
            resolve: (a) => this.resolve(a),
        };
    }
}
