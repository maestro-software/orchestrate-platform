import { HttpClient } from "@angular/common/http";
import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { ActivatedRoute } from "@angular/router";
import { BaseDialog } from "app/common-ux/dialog/base-dialog";
import { AppConfig } from "app/common/app-config";
import { Score } from "app/dal/model/score";
import { Observable, switchMap } from "rxjs";

interface ScoreConcertDto {
    concertId: number;
    occasion: string;
    date: Date;
    performances: ScorePerformanceDto[];
}

interface ScorePerformanceDto {
    ensembleName: string;
}

@Component({
    templateUrl: "./score-performance-list-dialog.component.html",
    styles: [
        `
            [matDialogTitle] {
                margin-bottom: 0;
            }

            .content {
                padding: 0;
            }

            .subtitle {
                padding: 0 1.5rem;
            }

            .concert-icon[matListItemIcon] {
                color: rgba(0, 0, 0, 0.87);
                margin-right: 0.5rem;
            }
        `,
    ],
    standalone: false,
})
export class ScorePerformanceListDialogComponent extends BaseDialog<Score> {
    public readonly dialogName = "ScorePerformanceList";
    public concertList$: Observable<ScoreConcertDto[]>;

    public constructor(
        dialogRef: MatDialogRef<ScorePerformanceListDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public score: Score,
        appConfig: AppConfig,
        httpClient: HttpClient,
        public route: ActivatedRoute,
    ) {
        super(dialogRef);
        this.concertList$ = appConfig
            .serverEndpoint(`music-library/score-concerts/${score.id}`)
            .pipe(switchMap((url) => httpClient.get<ScoreConcertDto[]>(url)));
    }
}
