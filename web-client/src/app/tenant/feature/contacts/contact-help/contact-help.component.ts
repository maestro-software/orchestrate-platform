import { CommonModule } from "@angular/common";
import { Component } from "@angular/core";
import { HelpBrokerService } from "app/help/help-broker.service";
import { AppHelpScaffoldModule } from "app/help/scaffold/help-scaffold.module";
import { contactsPageId } from "../contacts-consts";

@Component({
    templateUrl: "./contact-help.component.html",
    imports: [
        CommonModule,
        AppHelpScaffoldModule,
    ],
})
export class ContactHelpComponent {
    public contactsHooks$ = this.helpBroker.helpablePageDataHooks(contactsPageId);

    public constructor(private helpBroker: HelpBrokerService) {}
}
