import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AppConfig } from "app/common/app-config";
import { Observable, map, switchMap } from "rxjs";
import { Contact, ContactCategory } from "./contact";
import { shallowMergeObjects } from "app/common/utils";

export type CreateContactDto = Omit<Contact, "id">;
export type UpdateContactDto = Partial<Omit<Contact, "id">>;

@Injectable()
export class ContactService {
    public constructor(
        private appConfig: AppConfig,
        private httpClient: HttpClient,
    ) {}

    public create(contact: CreateContactDto): Observable<Contact> {
        return this.appConfig
            .serverEndpoint(`contacts`)
            .pipe(switchMap((url) => this.httpClient.post<Contact>(url, contact)));
    }

    public getAll(category?: ContactCategory) {
        let params = new HttpParams();
        if (typeof category !== "undefined") {
            params = params.append("category", category);
        }
        return this.appConfig.serverEndpoint(`contacts`).pipe(
            switchMap((url) =>
                this.httpClient.get<Contact[]>(url, {
                    params,
                }),
            ),
        );
    }

    public update(id: number, contact: UpdateContactDto): Observable<Contact> {
        return this.appConfig
            .serverEndpoint(`contacts/${id}`)
            .pipe(switchMap((url) => this.httpClient.patch<Contact>(url, contact)));
    }

    public archive(contact: Contact): Observable<Contact> {
        return this.appConfig.serverEndpoint(`contacts/${contact.id}/archive`).pipe(
            switchMap((url) => this.httpClient.post<Contact>(url, "")),
            map((updatedContact) => shallowMergeObjects(contact, updatedContact)),
        );
    }

    public unarchive(contact: Contact): Observable<Contact> {
        return this.appConfig.serverEndpoint(`contacts/${contact.id}/unarchive`).pipe(
            switchMap((url) => this.httpClient.post<Contact>(url, "")),
            map((updatedContact) => shallowMergeObjects(contact, updatedContact)),
        );
    }

    public delete(id: number) {
        return this.appConfig
            .serverEndpoint(`contacts/${id}`)
            .pipe(switchMap((url) => this.httpClient.delete<void>(url)));
    }
}
