import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from "@angular/core";
import { roleLabel } from "app/auth/role";
import { RoleName } from "app/auth/role.enum";
import { deleteArrayElement } from "app/common/utils";

@Component({
    selector: "app-edit-roles",
    templateUrl: "./edit-roles.component.html",
    standalone: false,
})
export class EditRolesComponent implements OnInit, OnChanges {
    @Input() public hint?: string;

    @Input() public roles: RoleName[] = [];
    @Output() public roleAdded = new EventEmitter<RoleName>();
    @Output() public roleRemoved = new EventEmitter<RoleName>();

    public selectedRole?: RoleName;
    public availableRoles: RoleName[] = [];
    public allRoles: RoleName[] = Object.values(RoleName);

    public ngOnInit() {
        this.updateUserData();
    }

    public ngOnChanges() {
        this.updateUserData();
    }

    private updateUserData() {
        this.availableRoles = this.allRoles.filter((r) => !this.roles.some((rn) => rn === r));
    }

    public formatRole = (role: RoleName) => roleLabel[role];

    public addRole(role: RoleName) {
        this.roles.push(role);
        this.updateUserData();
        this.roleAdded.emit(role);
    }

    public deleteRole(role: RoleName) {
        deleteArrayElement(this.roles, role);
        this.updateUserData();
        this.roleRemoved.emit(role);
    }
}
