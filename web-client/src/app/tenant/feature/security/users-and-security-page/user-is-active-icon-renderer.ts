import { GridIconRenderer } from "app/common-ux/data-grid/grid-icon-renderer";
import { UserStatus } from "../tenant-security.service";

const icon: Record<UserStatus, string> = {
    [UserStatus.Active]: "verified_user",
    [UserStatus.Pending]: "schedule",
    [UserStatus.NoAccess]: "gpp_bad",
};

const tooltip: Record<UserStatus, string> = {
    [UserStatus.Active]: "Member is active and can access Orchestrate",
    [UserStatus.Pending]:
        "Member has been sent an invite, but they haven't set up their account yet",
    [UserStatus.NoAccess]: "Member has no access and can't access Orchestrate",
};

export class UserIsActiveIconRenderer extends GridIconRenderer<UserStatus> {
    protected icon(value: UserStatus): string {
        return icon[value];
    }
    protected tooltip(value: UserStatus): string {
        return tooltip[value];
    }
}
