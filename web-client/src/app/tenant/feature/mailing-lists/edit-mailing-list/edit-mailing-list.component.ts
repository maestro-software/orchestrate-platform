import { CommonModule } from "@angular/common";
import { Component, Input, OnChanges } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { AppFormModule } from "app/common-ux/form/form.module";
import { map, of, startWith } from "rxjs";
import { MailingList, MailingListAddressTemplateParameters } from "../mailing-list";

export type EditableMailingList = Omit<MailingList, "id">;

@Component({
    selector: "app-edit-mailing-list",
    templateUrl: "./edit-mailing-list.component.html",
    styleUrls: ["./edit-mailing-list.component.scss"],
    imports: [
        CommonModule,
        AppFormModule,
    ],
})
export class EditMailingListComponent implements OnChanges {
    @Input() public formGroup = EditMailingListComponent.buildFormGroup();
    @Input() public addressParameters?: MailingListAddressTemplateParameters;

    protected fullAddress$ = of("");

    private autoSetSlug = false;

    public ngOnChanges(): void {
        this.autoSetSlug = !this.formGroup.controls.slug.value;
        this.fullAddress$ = this.formGroup.controls.slug.valueChanges.pipe(
            startWith(this.formGroup.controls.slug.value),
            map((slug) => {
                if (!this.addressParameters || !slug) {
                    return "";
                }

                const ap = this.addressParameters;
                return `${ap.tenantSlug}#${slug}@${ap.domain}`;
            }),
        );
    }

    public setSlugFromName() {
        if (!this.autoSetSlug || !this.formGroup.controls.slug.pristine) {
            return;
        }

        const name = this.formGroup.controls.name.value ?? "";
        const abbreviation = name.trim().toLocaleLowerCase().replaceAll(" ", "-");
        this.formGroup.controls.slug.setValue(abbreviation);
        this.formGroup.controls.slug.markAsTouched();
    }

    public static buildFormGroup(mailingList?: Partial<MailingList>) {
        const fb = new FormBuilder().nonNullable;
        return fb.group({
            name: fb.control(mailingList?.name, {
                validators: [Validators.required],
            }),
            slug: fb.control(mailingList?.slug, {
                validators: [Validators.required],
            }),
            allowedSenders: fb.control(mailingList?.allowedSenders, {
                validators: [Validators.required],
            }),
            replyTo: fb.control(mailingList?.replyTo, {
                validators: [Validators.required],
            }),
            footer: fb.control(mailingList?.footer),
        });
    }
}
