import { Injectable } from "@angular/core";
import { AuthenticationService } from "app/auth/authentication.service";
import { filterForDefinedValue } from "app/common/rxjs-utilities";
import { BreezeService } from "app/dal/breeze.service";
import { EnsembleBreezeModel, EnsembleStatus } from "app/dal/model/ensemble";
import {
    MemberBillingConfig,
    MemberBillingConfigBreezeModel,
} from "app/dal/model/member-billing-config";
import { MemberBillingPeriodBreezeModel } from "app/dal/model/member-billing-period";
import { MemberBillingPeriodConfigBreezeModel } from "app/dal/model/member-billing-period-config";
import { TenantBreezeModel } from "app/dal/model/tenant";
import { Predicate } from "breeze-client";
import { forkJoin, Observable } from "rxjs";
import { first, map, switchMap } from "rxjs/operators";

@Injectable()
export class SettingsService {
    public constructor(
        private authService: AuthenticationService,
        private breezeService: BreezeService,
    ) {}

    public getTenantEntity() {
        return this.breezeService.getAll(TenantBreezeModel).pipe(map((tenant) => tenant[0]));
    }

    public createMemberBillingConfig() {
        return this.authService.user$.pipe(
            filterForDefinedValue(),
            first(),
            switchMap((user) =>
                this.breezeService.create(MemberBillingConfigBreezeModel, {
                    notificationContactName: user.displayName,
                    notificationContactEmail: user.username,
                    invoicePrefix: "INV",
                    nextInvoiceNumber: 1,
                }),
            ),
        );
    }

    public getMemberBillingConfig(): Observable<MemberBillingConfig | undefined> {
        return this.breezeService
            .getAll(MemberBillingConfigBreezeModel)
            .pipe(map((config) => config[0]));
    }

    public getMemberBillingPeriodConfigs() {
        return this.breezeService.getAll(MemberBillingPeriodConfigBreezeModel);
    }

    public createBillingPeriodConfigs(numberToCreate: number) {
        const createdConfigs$ = Array(numberToCreate)
            .fill(undefined)
            .map(() => {
                return this.breezeService.create(MemberBillingPeriodConfigBreezeModel, {});
            });
        return forkJoin(createdConfigs$);
    }

    public getEnsembles() {
        return this.breezeService.getByPredicate(
            EnsembleBreezeModel,
            new Predicate("status", "==", EnsembleStatus.Active),
        );
    }

    public hasUnclosedBillingPeriods() {
        return this.breezeService
            .getCountByPredicate(
                MemberBillingPeriodBreezeModel,
                // TODO Hack, Breeze doesn't seem to query correctly when we use the enum
                // Here should be ('state', '!=', MemberBillingPeriodState.Closed)
                // I think should be fixed in Breeze.Server.Net 5.0.3
                // https://github.com/Breeze/breeze.server.net/issues/101
                new Predicate("state", "!=", 3),
            )
            .pipe(map((c) => c > 0));
    }
}
