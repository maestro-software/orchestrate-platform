import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ICellRendererAngularComp } from "ag-grid-angular";
import { ICellRendererParams } from "ag-grid-community";
import { FeaturePermission } from "app/auth/feature-permission";
import { Asset } from "app/dal/model/asset";
import { IAssetTableContext } from "../asset-table/asset-table.component";
import { AssetMetadataDto } from "../asset.service";

@Component({
    template: `
        <div
            *ngIf="asset"
            class="asset-table-loaned-to"
        >
            <!-- TODO Convert members to Breeze to take advantage of toString -->
            <ng-container *ngIf="currentLoans[asset.id]?.member as member">
                {{ member.firstName }} {{ member.lastName }}
            </ng-container>
            <button
                mat-icon-button
                class="asset-table-loaned-to-edit"
                (click)="editAssetLoan(asset)"
                icon="edit"
                *appIfHasPermission="FeaturePermission.AssetWrite"
            >
                <mat-icon>edit</mat-icon>
            </button>
        </div>
    `,
    styles: [
        `
            .asset-table-loaned-to {
                display: flex;
                align-items: center;
            }

            .asset-table-loaned-to-edit {
                flex: 0 0 auto;
                margin-left: auto;
            }
        `,
    ],
    standalone: false,
})
export class AssetLoanRendererComponent implements ICellRendererAngularComp {
    public readonly FeaturePermission = FeaturePermission;

    public asset?: Asset;
    public context: IAssetTableContext = {
        currentLoans: {},
    };

    public constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
    ) {}

    public get currentLoans() {
        return this.context.currentLoans;
    }

    public async editAssetLoan(asset: Asset) {
        await this.router.navigate([asset.id, "loan"], {
            relativeTo: this.activatedRoute,
        });
    }

    public agInit(params: ICellRendererParams): void {
        this.asset = (params.data as AssetMetadataDto).asset;
        this.context = params.context;
    }

    public refresh(): boolean {
        return false;
    }
}
