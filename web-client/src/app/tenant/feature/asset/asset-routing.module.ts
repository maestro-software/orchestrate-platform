import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FeaturePermission } from "app/auth/feature-permission";
import { AppRoutes } from "app/common/app-routing-types";
import { helpPages } from "app/help/route/help-list";
import { AllAssetsComponent } from "./all-assets/all-assets.component";
import { LoanHistoryComponent } from "./loan-history/loan-history.component";

const assetRoutes: AppRoutes = [
    {
        path: "",
        component: AllAssetsComponent,
        data: {
            title: "Assets",
            icon: "library_books",
            restrictedToPermission: FeaturePermission.AssetRead,
            help: [helpPages.assets],
        },
    },
    {
        path: ":assetId/loan",
        component: LoanHistoryComponent,
        data: {
            title: "Loan History",
            icon: "history_edu",
            restrictedToPermission: FeaturePermission.AssetWrite,
            help: [helpPages.assets],
        },
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(assetRoutes),
    ],
    exports: [
        RouterModule,
    ],
})
export class AppAssetRoutingModule {}
