import { Component, Input } from "@angular/core";
import {
    EnsembleMembership,
    EnsembleMembershipType,
    EnsembleMembershipTypeMetadata,
} from "app/dal/model/ensemble-membership";

@Component({
    selector: "app-membership-type-icon",
    template: `
        <mat-icon
            color="primary"
            [ngClass]="{ 'inactive-membership': isInactive }"
            [ngStyle]="size ?? {}"
            [matTooltip]="tooltipText"
        >
            {{ metadata.icon }}
        </mat-icon>
    `,
    styles: [
        `
            .inactive-membership {
                color: rgba(0, 0, 0, 0.5);
            }
        `,
    ],
    standalone: false,
})
export class MembershipTypeIconComponent {
    @Input() public membership?: EnsembleMembership;
    @Input() public membershipType = EnsembleMembershipType.Regular;
    @Input() public active = false;

    @Input() public showFeeTextInTooltip = true;
    @Input() public tooltipSuffixText?: string;
    @Input() public size?: { "font-size"?: string; width?: string; height?: string };

    public get type() {
        return this.membership ? this.membership.type : this.membershipType;
    }

    public get metadata() {
        return EnsembleMembershipTypeMetadata[this.type];
    }

    public get tooltipText() {
        const feeText = this.metadata.feesAreCharged ? "pays fees" : "doesn't pay fees";
        const suffix = this.tooltipSuffixText ?? "";
        return this.showFeeTextInTooltip
            ? `${this.metadata.label} - ${feeText}${suffix}`
            : `${this.metadata.label}${suffix}`;
    }

    public get isInactive() {
        return this.membership ? !!this.membership.dateLeft : !this.active;
    }
}
