import { CommonModule } from "@angular/common";
import { Component } from "@angular/core";
import { HelpBrokerService } from "app/help/help-broker.service";
import { AppHelpScaffoldModule } from "app/help/scaffold/help-scaffold.module";
import { membersPageId } from "../member-consts";

@Component({
    templateUrl: "./member-help.component.html",
    imports: [
        CommonModule,
        AppHelpScaffoldModule,
    ],
})
export class MemberHelpComponent {
    public memberHelpData$ = this.helpBroker.helpablePageDataHooks(membersPageId);

    public constructor(private helpBroker: HelpBrokerService) {}
}
