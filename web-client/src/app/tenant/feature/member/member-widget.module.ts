import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatMenuModule } from "@angular/material/menu";
import { AppDataGridModule } from "app/common-ux/data-grid/data-grid.module";
import { SelectMembersComponent } from "./select-members/select-members.component";

@NgModule({
    imports: [
        CommonModule,
        MatMenuModule,
        MatButtonModule,
        MatIconModule,
        AppDataGridModule,
    ],
    exports: [
        SelectMembersComponent,
    ],
    declarations: [
        SelectMembersComponent,
    ],
})
export class AppMemberWidgetModule {}
