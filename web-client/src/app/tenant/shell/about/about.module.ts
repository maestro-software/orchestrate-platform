import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatCardModule } from "@angular/material/card";
import { MatTooltipModule } from "@angular/material/tooltip";
import { RouterModule, Routes } from "@angular/router";
import { AppBadgeModule } from "app/common-ux/badge/badge.module";
import { AppLoadingModule } from "app/common-ux/loading/loading.module";
import { AppPageWrapperModule } from "app/common-ux/page-wrapper/page-wrapper.module";
import { AboutPageComponent } from "./about-page/about-page.component";
import { TermsOfUseComponent } from "./terms-of-use/terms-of-use.component";
import { WhatsNewComponent } from "./whats-new/whats-new.component";

const routes: Routes = [
    {
        path: "",
        component: AboutPageComponent,
        data: {
            title: "About",
            icon: "info",
        },
    },
    {
        path: "release-notes",
        component: WhatsNewComponent,
    },
    {
        path: "terms-of-use",
        component: TermsOfUseComponent,
    },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),

        MatCardModule,
        MatTooltipModule,

        AppPageWrapperModule,
        AppBadgeModule,
        AppLoadingModule,
    ],
    declarations: [
        AboutPageComponent,
        WhatsNewComponent,
        TermsOfUseComponent,
    ],
})
export class AppAboutModule {}
