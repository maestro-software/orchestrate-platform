import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AppConfig } from "app/common/app-config";
import { BreezeEntity } from "app/dal/breeze-entity";
import { BreezeModel } from "app/dal/breeze-model";
import { BreezeService } from "app/dal/breeze.service";

@Injectable()
export abstract class BreezeDomainService {
    public constructor(
        protected appConfig: AppConfig,
        protected breezeService: BreezeService,
        protected http: HttpClient,
    ) {}

    protected get apiEndpoint$() {
        return this.appConfig.serverEndpoint$;
    }

    public delete(entity: BreezeEntity) {
        this.breezeService.delete(entity);
    }

    public deleteAndSave(...entities: BreezeEntity[]) {
        for (const entity of entities) {
            this.breezeService.delete(entity);
        }
        return this.saveChanges(...entities);
    }

    public cancelChanges(...entities: BreezeEntity[]) {
        return this.breezeService.cancelChanges(...entities);
    }

    public saveChanges(...entities: BreezeEntity[]) {
        return this.breezeService.saveChanges(...entities);
    }
}

@Injectable()
export abstract class BreezeEntityService<T extends BreezeEntity> extends BreezeDomainService {
    protected abstract readonly breezeModel: BreezeModel<T>;

    public getById(id: number) {
        return this.breezeService.getById(this.breezeModel, id);
    }

    public getAll() {
        return this.breezeService.getAll(this.breezeModel);
    }
}
