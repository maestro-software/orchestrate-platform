import { CommonModule } from "@angular/common";
import { Component } from "@angular/core";
import { MatIconModule } from "@angular/material/icon";
import { MatMenuModule } from "@angular/material/menu";
import { MatTabsModule } from "@angular/material/tabs";
import { RouterModule } from "@angular/router";
import { UserDropdownComponent } from "app/common-ux/user-dropdown/user-dropdown.component";
import { ApiUserComponent } from "../api-user/api-user.component";
import { DemoSeedComponent } from "../demo-seed/demo-seed.component";

@Component({
    templateUrl: "./admin-page.component.html",
    styleUrl: "./admin-page.component.scss",
    imports: [
        CommonModule,
        RouterModule,

        MatMenuModule,
        MatIconModule,
        MatTabsModule,

        DemoSeedComponent,
        ApiUserComponent,

        UserDropdownComponent,
    ],
})
export class AdminPageComponent {}
