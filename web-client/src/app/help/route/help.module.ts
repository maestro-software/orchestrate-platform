import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatIconModule } from "@angular/material/icon";
import { MatStepperModule } from "@angular/material/stepper";
import { AppAuthModule } from "app/auth/auth.module";
import { AppButtonModule } from "app/common-ux/button/button.module";
import { AppHelpScaffoldModule } from "../scaffold/help-scaffold.module";
import { GetStartedHelpComponent } from "./get-started-help/get-started-help.component";
import { HelpContainerComponent } from "./help-container/help-container.component";
import { AppHelpRoutingModule } from "./help-routing.module";

@NgModule({
    imports: [
        CommonModule,

        MatIconModule,
        MatStepperModule,

        AppHelpRoutingModule,
        AppButtonModule,
        AppAuthModule,
        AppHelpScaffoldModule,
    ],
    declarations: [
        HelpContainerComponent,
        GetStartedHelpComponent,
    ],
})
export class AppHelpModule {}
