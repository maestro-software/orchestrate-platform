import { FeaturePermission } from "app/auth/feature-permission";
import { helpPages } from "../help-list";

// Must be in same order as steps in HTML
export enum GetStartedStep {
    Welcome,
    Ensembles,
    Members,
    MusicLibrary,
    Concerts,
    MemberBilling,
    Assets,
    Contacts,
    MailingLists,
    Settings,
    Done,
}

export interface GetStartedStepData {
    step: GetStartedStep;
    readPermission: FeaturePermission;
    stepLabel: string;
    firstRunLabel: string;
    summary: string;
    firstRunPrompt: string;
    callToActionText: string;
    callToActionPrimaryRouteCommands: unknown[];
    callToActionHelpRouteCommands: unknown[];
}

export const allSteps: GetStartedStepData[] = [
    {
        step: GetStartedStep.Ensembles,
        readPermission: FeaturePermission.EnsembleRead,
        stepLabel: "Ensembles",
        firstRunLabel: "Create your ensemble(s)",
        summary: `
            Ensembles are the people that you rehearse and perform with. Some groups only have
            the one, others have many.
        `,
        firstRunPrompt: `
            First we need an ensemble (or two) for your members to play in. Let's go and add one.
        `,
        callToActionText: "Add ensemble",
        callToActionPrimaryRouteCommands: ["ensembles"],
        callToActionHelpRouteCommands: ["view", "ensembles"],
    },
    {
        step: GetStartedStep.Members,
        readPermission: FeaturePermission.MemberDetailRead,
        stepLabel: "Members",
        firstRunLabel: "Add some members",
        summary: `
            Members are the life-blood of any community music group, so it makes sense
            to have an effective way to manage them. Orchestrate aims to make this
            easy and intuitive.
        `,
        firstRunPrompt: `
            Now let's add some members to the ensemble you have created.
        `,
        callToActionText: "Add some members",
        callToActionPrimaryRouteCommands: ["members"],
        callToActionHelpRouteCommands: ["view", "members"],
    },
    {
        step: GetStartedStep.MusicLibrary,
        readPermission: FeaturePermission.ScoreRead,
        stepLabel: "Music Library",
        firstRunLabel: "Populate your music library",
        summary: `
            Keeping tabs of all of your sheet music is a real chore, so
            Orchestrate aims to take the pain out of it for you.
        `,
        firstRunPrompt: `
            Hopefully you have some music that you own, let's start tracking some of it.
        `,
        callToActionText: "Populate music library",
        callToActionPrimaryRouteCommands: ["music-library"],
        callToActionHelpRouteCommands: ["view", "music-library"],
    },
    {
        step: GetStartedStep.Concerts,
        readPermission: FeaturePermission.ConcertRead,
        stepLabel: "Concerts",
        firstRunLabel: "Record a concert",
        summary: `
            Of course, every now and then you put some real effort into your
            rehearsals, because you're performing for your friends and family.
        `,
        firstRunPrompt: `
            Let's add one of your concerts into Orchestrate.
        `,
        callToActionText: "Add a concert",
        callToActionPrimaryRouteCommands: ["concerts"],
        callToActionHelpRouteCommands: ["view", "concerts"],
    },
    {
        step: GetStartedStep.MemberBilling,
        readPermission: FeaturePermission.MemberBillingRead,
        stepLabel: "Member Billing",
        firstRunLabel: "Setup automated billing",
        summary: `
            A time-consuming job in most community groups is managing
            member invoices. Orchestrate takes the pain out of this
            by automatically generating invoices.
        `,
        firstRunPrompt: `
            Before that can happen though, we need to set it up.
        `,
        callToActionText: "Configure billing",
        callToActionPrimaryRouteCommands: ["member-billing"],
        callToActionHelpRouteCommands: ["view", "member-billing"],
    },
    {
        step: GetStartedStep.Assets,
        readPermission: FeaturePermission.AssetRead,
        stepLabel: "Assets",
        firstRunLabel: "Track your assets",
        summary: `
            Keeping track of everything your group owns can be difficult,
            but storing their information in Orchestrate makes it easier.
        `,
        firstRunPrompt: `
            If you own any, let's add them to Orchestrate now.
        `,
        callToActionText: "Add an asset",
        callToActionPrimaryRouteCommands: ["assets"],
        callToActionHelpRouteCommands: ["view", "assets"],
    },
    {
        step: GetStartedStep.Contacts,
        readPermission: FeaturePermission.ContactsRead,
        stepLabel: "Contacts",
        firstRunLabel: "Populate your contacts",
        summary: `
            Never lose the contact details for people you regularly interact with
            by storing them in Orchestrate.
        `,
        firstRunPrompt: `
            Let's add your first contact now.
        `,
        callToActionText: "Add a contact",
        callToActionPrimaryRouteCommands: ["contacts"],
        callToActionHelpRouteCommands: helpPages.contacts.commands,
    },
    {
        step: GetStartedStep.MailingLists,
        readPermission: FeaturePermission.MailingListRead,
        stepLabel: "Mailing Lists",
        firstRunLabel: "Create mailing lists",
        summary: `
            Orchestrate also integrates mailing lists so you can easily email members and
            contacts.
        `,
        firstRunPrompt: `
            Let's start by creating a mailing list for your ensemble.
        `,
        callToActionText: "Add a mailing list",
        callToActionPrimaryRouteCommands: ["mailing-lists"],
        callToActionHelpRouteCommands: helpPages.mailingLists.commands,
    },
    {
        step: GetStartedStep.Settings,
        readPermission: FeaturePermission.SettingsWrite,
        stepLabel: "Settings",
        firstRunLabel: "Tweak your settings",
        summary: `
            You won't need to change these often, but Orchestrate has a number of
            settings that are worth being aware of.
        `,
        firstRunPrompt: `
            You may want to update the contact email to be your official organisational
            email address.
        `,
        callToActionText: "Update settings",
        callToActionPrimaryRouteCommands: ["settings"],
        callToActionHelpRouteCommands: helpPages.settings.commands,
    },
];
