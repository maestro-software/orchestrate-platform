import { Component, HostBinding, Input } from "@angular/core";
import { ActivatedRoute, NavigationEnd, Router, UrlTree } from "@angular/router";
import { ResponsiveService } from "app/common-ux/responsive.service";
import { AppRouteData } from "app/common/app-routing-types";
import {
    findShellRouteSnapshot,
    getCurrentPrimaryRouteLeaf,
    helpOutletName,
} from "app/common/routing-utils";
import { propagateMap } from "app/common/rxjs-utilities";
import { HelpPage } from "app/help/route/help-page";
import { Observable, combineLatest, filter, map, startWith, switchMap } from "rxjs";

@Component({
    selector: "app-help-center-page",
    templateUrl: "./help-center-page.component.html",
    styleUrls: ["./help-center-page.component.scss"],
    standalone: false,
})
export class HelpCenterPageComponent {
    @Input() public helpTitle?: string;
    public continueTourUrl$: Observable<UrlTree | undefined>;
    public otherApplicableHelp$: Observable<HelpPage[]>;
    public isXl$ = this.responsiveService.isXl$;

    @Input()
    @HostBinding("class.padded-content")
    public padContent = true;

    @Input()
    public prependCurrentPageHelp = true;

    public constructor(
        route: ActivatedRoute,
        router: Router,
        private responsiveService: ResponsiveService,
    ) {
        this.continueTourUrl$ = route.paramMap.pipe(
            map((params) => params.get("continueTour") ?? undefined),
            propagateMap((continueTour) => router.parseUrl(continueTour)),
        );
        const currentPageHelp$ = router.events.pipe(
            filter((e) => e instanceof NavigationEnd),
            map(() => route),
            startWith(route),
            map((r) => getCurrentPrimaryRouteLeaf(r)),
            switchMap((r) => r.data),
            map((routeData: AppRouteData) => {
                return Array.isArray(routeData.help) ? routeData.help : [];
            }),
        );
        const currentHelpCommands$ = router.events.pipe(
            filter((e) => e instanceof NavigationEnd),
            map(() => route),
            startWith(route),
            map((r) => findShellRouteSnapshot(r.snapshot)),
            map((snapshot) => {
                let helpRoute = snapshot.children.find((r) => r.outlet === helpOutletName) ?? null;
                if (!helpRoute) {
                    return [];
                }

                const helpCommands = [];
                while (helpRoute) {
                    const url = helpRoute.url[0];
                    if (url) {
                        helpCommands.push(url.path);
                    }

                    helpRoute = helpRoute.firstChild;
                }

                return helpCommands;
            }),
        );
        this.otherApplicableHelp$ = combineLatest([
            currentPageHelp$,
            currentHelpCommands$,
        ]).pipe(
            map(([currentPageHelp, currentHelpCommands]) => {
                return currentPageHelp.filter((h) => {
                    if (h.commands.length !== currentHelpCommands.length) {
                        return true;
                    }

                    for (let i = 0; i < currentHelpCommands.length; i++) {
                        if (h.commands[i] !== currentHelpCommands[i]) {
                            return true;
                        }
                    }

                    return false;
                });
            }),
        );
    }
}
