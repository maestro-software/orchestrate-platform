import { Directive, HostBinding, HostListener, Input, OnDestroy } from "@angular/core";
import { ActivatedRoute, NavigationEnd, Router, UrlTree } from "@angular/router";
import { buildHelpUrlTree, helpOutletName } from "app/common/routing-utils";
import { BehaviorSubject, combineLatest, filter, map, startWith, Subscription } from "rxjs";

@Directive({
    standalone: true,
    selector: "[appHelpRouterLink]",
})
export class HelpRouterLinkDirective implements OnDestroy {
    @Input("appHelpRouterLink") public set helpCommands(commands: unknown[] | null) {
        this.helpCommands$.next(commands);
    }
    private helpCommands$ = new BehaviorSubject<unknown[] | null>(null);

    @HostBinding("href") public href?: string;
    private urlTree?: UrlTree;

    private subscription: Subscription;

    public constructor(
        private router: Router,
        route: ActivatedRoute,
    ) {
        const otherNavigations = this.router.events.pipe(
            filter((e): e is NavigationEnd => e instanceof NavigationEnd),
            map(() => route),
            startWith(route),
            filter((r) => r.outlet !== helpOutletName),
        );
        this.subscription = combineLatest([
            otherNavigations,
            this.helpCommands$,
        ]).subscribe(([r, commands]) => {
            this.urlTree = buildHelpUrlTree(r.snapshot, commands);
            this.href = this.router.serializeUrl(this.urlTree);
        });
    }

    public ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    @HostListener("click", ["$event"])
    public async navigate(event: Event) {
        if (!this.urlTree) {
            return;
        }

        event.preventDefault();
        await this.router.navigateByUrl(this.urlTree);
    }
}
