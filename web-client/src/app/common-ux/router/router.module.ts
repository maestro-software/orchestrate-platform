import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { HelpRouterLinkDirective } from "./help-router-link.directive";
import { PrimaryRouterLinkDirective } from "./primary-router-link.directive";

@NgModule({
    imports: [
        RouterModule,

        PrimaryRouterLinkDirective,
        HelpRouterLinkDirective,
    ],
    exports: [
        PrimaryRouterLinkDirective,
        HelpRouterLinkDirective,
    ],
})
export class AppRouterModule {}
