import { Directive, ElementRef, Input, OnDestroy, OnInit } from "@angular/core";
import { FormControl, FormGroup, FormGroupDirective, ValidationErrors } from "@angular/forms";
import { Subscription } from "rxjs";
import { startWith } from "rxjs/operators";

@Directive({
    selector: "mat-error[appAggregateFormControlNameErrors]",
    standalone: false,
})
export class AggregateFormControlNameErrorsDirective implements OnInit, OnDestroy {
    @Input("appAggregateFormControlNameErrors") controlName?: string;
    private subscription?: Subscription;

    public constructor(
        private elementRef: ElementRef<HTMLElement>,
        private formGroup: FormGroupDirective,
    ) {}

    public ngOnInit() {
        if (!this.controlName || !(this.formGroup.form instanceof FormGroup)) {
            throw new Error("Must be defined & be a FormGroup");
        }

        const control = this.formGroup.form.get(this.controlName);
        if (!control || !(control instanceof FormControl)) {
            throw new Error("Control not found or is not a FormControl");
        }

        this.subscription = control.statusChanges.pipe(startWith(control.status)).subscribe(() => {
            if (control.invalid && control.errors) {
                this.elementRef.nativeElement.innerText = this.getErrorMessages(
                    control.errors,
                ).join(", ");
            } else {
                this.elementRef.nativeElement.innerText = "";
            }
        });
    }

    private getErrorMessages(errors: ValidationErrors): string[] {
        const angularErrorKeys: Record<string, string> = {
            required: "Value is required",
            email: "Not a valid email address",
        };

        return Object.entries(errors)
            .map(([key, value]) => {
                if (typeof value === "string") {
                    return value;
                } else if (angularErrorKeys[key]) {
                    return angularErrorKeys[key];
                } else {
                    return null;
                }
            })
            .filter((e): e is string => typeof e === "string");
    }

    public ngOnDestroy() {
        this.subscription?.unsubscribe();
    }
}
