import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
    selector: "app-mobile-collapsible-filter",
    template: `
        <app-mobile-collapsible-content
            #mobileCollapsible
            [allowMobileCollapse]="allowMobileCollapse"
            [collapsedColour]="buttonColour"
            collapsedIcon="filter_alt"
            collapsedTooltip="Filter"
        >
            <mat-form-field subscriptSizing="dynamic">
                <mat-label>Filter</mat-label>
                <input
                    matInput
                    (keyup)="emitFilterText($event)"
                    (keyup.enter)="mobileCollapsible.closeOverlay()"
                />
            </mat-form-field>
        </app-mobile-collapsible-content>
    `,
    standalone: false,
})
export class MobileCollpsibleFilterComponent {
    @Input() public allowMobileCollapse = true;
    @Output() public filterTextChange = new EventEmitter<string>();

    public buttonColour?: "accent";

    public emitFilterText(event: KeyboardEvent) {
        const filterText = (event.target as HTMLInputElement).value.trim();
        this.buttonColour = filterText ? "accent" : undefined;
        this.filterTextChange.emit(filterText);
    }
}
