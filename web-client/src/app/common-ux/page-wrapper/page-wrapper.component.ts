import { Component, Input, OnDestroy, TemplateRef } from "@angular/core";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { AppService } from "app/app.service";
import { AppRouteData } from "app/common/app-routing-types";
import { BehaviorSubject, Observable, combineLatest } from "rxjs";
import { filter, map, startWith, switchMap } from "rxjs/operators";

@Component({
    selector: "app-page-wrapper",
    templateUrl: "./page-wrapper.component.html",
    styleUrls: ["./page-wrapper.component.scss"],
    standalone: false,
})
export class PageWrapperComponent implements OnDestroy {
    @Input() public hideDefaultToolbar = false;
    @Input() public hasDefaultContainer = true;

    @Input() public set title(value: string | undefined) {
        this.appService.setPageTitleOverride(value);
    }
    @Input() public set icon(value: string | undefined) {
        this.inputIcon$.next(value);
    }
    private inputIcon$ = new BehaviorSubject<string | undefined>(undefined);

    public title$: Observable<string | undefined>;
    public icon$: Observable<string | undefined>;
    public pageActionsTemplate?: TemplateRef<unknown>;

    public constructor(
        router: Router,
        activatedRoute: ActivatedRoute,
        private appService: AppService,
    ) {
        this.title$ = appService.pageTitle$;

        const routeData$ = router.events.pipe(
            filter((e) => e instanceof NavigationEnd),
            map(() => activatedRoute),
            startWith(activatedRoute),
            switchMap((r) => r.data as Observable<AppRouteData>),
        );
        this.icon$ = combineLatest([this.inputIcon$, routeData$]).pipe(
            map(([inputIcon, routeData]) => inputIcon ?? routeData.icon),
        );
    }

    public ngOnDestroy() {
        this.appService.setPageTitleOverride(undefined);
    }

    public setPageActionsTemplate(templateRef: TemplateRef<unknown>) {
        setTimeout(() => (this.pageActionsTemplate = templateRef));
    }

    public removePageActionsTemplate() {
        setTimeout(() => (this.pageActionsTemplate = undefined));
    }

    public removeDefaultContainer() {
        this.hasDefaultContainer = false;
    }
}
