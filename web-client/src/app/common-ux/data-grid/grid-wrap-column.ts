import { ColDef } from "ag-grid-community";

export function wrapTextColDef<T>(): ColDef<T> {
    return {
        autoHeight: true,
        // Issue here if column has also explicitly set a cellStyle value, the below values will be
        // overwritten
        cellStyle: {
            "white-space": "normal",
            "word-wrap": "break-word",
            // Just gone with something that looks half decent with one line or multiple here
            // Not sure if there's a better way of doing it?
            "padding-top": "0.75rem",
            "padding-bottom": "0.75rem",
            "line-height": "20px",
        },
    };
}
