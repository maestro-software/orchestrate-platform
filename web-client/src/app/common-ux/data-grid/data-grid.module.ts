import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatTooltipModule } from "@angular/material/tooltip";
import { AgGridModule } from "ag-grid-angular";
import { DataGridComponent } from "app/common-ux/data-grid/data-grid.component";
import { AppLoadingModule } from "../loading/loading.module";
import { AppMobileCollapsibleContentModule } from "../mobile-collapsible-content/mobile-collapsible-content.module";
import { DefaultGridDirective } from "./default-grid.directive";
import { GridCurrencyCellEditorComponent } from "./grid-currency-cell-editor";
import { GridFillHeightDirective } from "./grid-fill-height.directive";
import { GridFitColumnsDirective } from "./grid-fit-columns.directive";
import { GridMultipleSelectionDirective } from "./grid-multiple-selection.directive";
import { GridSingleSelectionDirective } from "./grid-single-selection.directive";
import { GridTextCellEditorComponent } from "./grid-text-cell-editor";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,

        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        MatTooltipModule,

        AgGridModule,

        AppLoadingModule,
        AppMobileCollapsibleContentModule,
        GridFillHeightDirective,
    ],
    exports: [
        AgGridModule,
        AppLoadingModule,
        DataGridComponent,
        DefaultGridDirective,
        GridFillHeightDirective,
        GridFitColumnsDirective,
        GridSingleSelectionDirective,
        GridMultipleSelectionDirective,
    ],
    declarations: [
        DataGridComponent,
        DefaultGridDirective,
        GridFitColumnsDirective,
        GridSingleSelectionDirective,
        GridMultipleSelectionDirective,
        GridCurrencyCellEditorComponent,
        GridTextCellEditorComponent,
    ],
})
export class AppDataGridModule {}
