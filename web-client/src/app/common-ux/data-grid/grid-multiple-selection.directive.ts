import {
    Directive,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    Output,
    SimpleChanges,
} from "@angular/core";
import { AgGridAngular } from "ag-grid-angular";
import { areSameArray } from "app/common/utils";
import { Subscription } from "rxjs";

@Directive({
    selector: "ag-grid-angular[appGridMultipleSelection]",
    standalone: false,
})
export class GridMultipleSelectionDirective<TRow, TSelection = TRow>
    implements OnChanges, OnDestroy
{
    @Input("appGridMultipleSelection") public selection?: TSelection[];
    // eslint-disable-next-line @angular-eslint/no-output-rename
    @Output("appGridMultipleSelectionChange") public selected = new EventEmitter<TSelection[]>();

    private subscription: Subscription;
    private isReady = false;
    private changesSelecting = false;

    @Input() public selectionRepresentation: (row: TRow) => TSelection = (r) =>
        r as unknown as TSelection;

    public constructor(private agGrid: AgGridAngular<TRow>) {
        agGrid.rowSelection = {
            mode: "multiRow",
            enableClickSelection: true,
            enableSelectionWithoutKeys: true,
        };

        agGrid.firstDataRendered.subscribe(() => {
            this.isReady = true;
            this.selectRows();
        });
        this.subscription = agGrid.selectionChanged.subscribe(() => {
            if (this.changesSelecting) {
                return;
            }

            // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
            const selection = agGrid.api?.getSelectedRows() ?? [];
            this.selected.emit(selection.map(this.selectionRepresentation));
            agGrid.api.onSortChanged();
        });
    }

    public ngOnChanges(changes: SimpleChanges) {
        const currLength = changes.selection?.currentValue?.length ?? 0;
        const prevLength = changes.selection?.previousValue?.length ?? 0;
        if (!this.isReady || (currLength === 0 && prevLength === 0)) {
            return;
        }

        if (areSameArray(changes.selection?.currentValue, changes.selection?.previousValue)) {
            return;
        }

        this.selectRows();
    }

    public selectRows() {
        this.changesSelecting = true;
        this.agGrid.api.deselectAll();
        this.agGrid.api.forEachNode((node) => {
            const row = node.data;

            if (row && this.selection?.some((s) => this.selectionRepresentation(row) === s)) {
                node.setSelected(true);
            }
        });
        this.changesSelecting = false;
    }

    public ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
