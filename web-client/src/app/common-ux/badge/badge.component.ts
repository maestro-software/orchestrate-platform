import { Component } from "@angular/core";

@Component({
    selector: "app-badge",
    template: ` <ng-content></ng-content> `,
    styles: [
        `
            :host {
                display: inline-flex;
                align-items: center;
                padding: 7px 12px;
                border-radius: 16px;
                background-color: #e0e0e0;
                color: rgba(0, 0, 0, 0.87);
            }

            :host-context(mat-toolbar) {
                font-size: 16px;
                line-height: 20px;
            }
        `,
    ],
    standalone: false,
})
export class BadgeComponent {}
