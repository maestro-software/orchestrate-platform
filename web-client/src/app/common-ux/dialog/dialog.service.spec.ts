import { TestBed } from "@angular/core/testing";
import { MatDialogModule } from "@angular/material/dialog";
import { MatSnackBarModule } from "@angular/material/snack-bar";

import { DialogService } from "./dialog.service";

describe("DialogService", () => {
    beforeEach(() =>
        TestBed.configureTestingModule({
            imports: [
                MatDialogModule,
                MatSnackBarModule,
            ],
            providers: [
                DialogService,
            ],
        }),
    );

    it("should be created", async () => {
        const service: DialogService = TestBed.inject(DialogService);
        await expect(service).toBeTruthy();
    });
});
