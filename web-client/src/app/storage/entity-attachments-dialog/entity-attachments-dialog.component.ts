import { AfterViewInit, Component, Input, OnInit, ViewChild } from "@angular/core";
import { FeaturePermission } from "app/auth/feature-permission";
import { EntityAttachmentService } from "app/storage/entity-attachment.service";
import { EntityAttachmentsComponent } from "app/storage/entity-attachments.component/entity-attachments.component";
import { StorageService } from "app/storage/storage.service";
import { TenantAuthorisationService } from "app/tenant/module/tenant-authorisation.service";
import { Observable, of, takeUntil } from "rxjs";
import {
    EntityAttachmentMetadata,
    EntityAttachmentSensitivity,
} from "../entity-attachment-metadata";

export interface EntityAttachmentsDialogOptions {
    storageStrategyName: string;
    entityId: number;
    defaultSensitivity: EntityAttachmentSensitivity;
    entityFormattedText: string;
    editPermission: FeaturePermission;
    dialogClose$: Observable<unknown>;
    setDisableClose: (isDisabled: boolean) => void;
    resolve: (attachments: EntityAttachmentMetadata[]) => void;
}

@Component({
    selector: "app-entity-attachments-dialog",
    templateUrl: "./entity-attachments-dialog.component.html",
    styleUrls: ["./entity-attachments-dialog.component.scss"],
    standalone: false,
})
export class EntityAttachmentsDialogComponent implements OnInit, AfterViewInit {
    @Input() options?: EntityAttachmentsDialogOptions;

    public entityFormattedText = "";
    public attachmentService?: EntityAttachmentService;
    public canEdit$: Observable<boolean> = of(false);
    public uploadInProgress$: Observable<boolean> = of(false);

    @ViewChild(EntityAttachmentsComponent)
    public entityAttachmentsComponent!: EntityAttachmentsComponent;

    public constructor(
        private storageService: StorageService,
        private authorisationService: TenantAuthorisationService,
    ) {}

    public ngOnInit() {
        if (!this.options) {
            throw new Error("Expected options to be defined");
        }

        const options = this.options;
        this.entityFormattedText = options.entityFormattedText;
        this.attachmentService = this.storageService.buildEntityAttachmentService(
            options.storageStrategyName,
            options.entityId,
            options.defaultSensitivity,
        );
        this.canEdit$ = this.authorisationService.hasPermission(options.editPermission);
    }

    public ngAfterViewInit(): void {
        if (!this.options) {
            throw new Error("Expected options to be defined");
        }

        const options = this.options;
        this.uploadInProgress$ = this.entityAttachmentsComponent.uploadInProgress$;
        this.uploadInProgress$
            .pipe(takeUntil(options.dialogClose$))
            .subscribe((uploadInProgress) => {
                options.setDisableClose(uploadInProgress);
            });
    }
}
