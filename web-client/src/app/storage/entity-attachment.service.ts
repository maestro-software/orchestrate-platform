import { map } from "rxjs";
import {
    EntityAttachementLinkData,
    EntityAttachmentAccessData,
    EntityAttachmentMetadata,
    EntityAttachmentSensitivity,
} from "./entity-attachment-metadata";
import { StorageService } from "./storage.service";

export class EntityAttachmentService {
    public constructor(
        private storageService: StorageService,
        private storageStrategyName: string,
        private entityId: number,
        private defaultSensitivity: EntityAttachmentSensitivity,
    ) {}

    public upload(file: File) {
        return this.storageService.uploadBlob<
            EntityAttachmentAccessData,
            EntityAttachementLinkData
        >(
            {
                storageStrategyName: this.storageStrategyName,
                accessData: { sensitivity: this.defaultSensitivity },
                linkData: { entityId: this.entityId },
            },
            file,
        );
    }

    public getMetadata() {
        return this.storageService.listBlobsLinkedBy<EntityAttachmentAccessData>(
            this.storageStrategyName,
            { entityId: this.entityId },
        );
    }

    public download(blob: EntityAttachmentMetadata) {
        return this.storageService.getBlobContent(blob.id).pipe(
            map((res) => {
                // TODO does this pull into memory?
                const a = document.createElement("a");
                a.href = URL.createObjectURL(res);
                a.download = blob.name;
                a.click();

                URL.revokeObjectURL(a.href);
            }),
        );
    }

    public setSensitivity(
        blob: EntityAttachmentMetadata,
        sensitivity: EntityAttachmentSensitivity,
    ) {
        return this.storageService.updateAccessData(blob.id, { sensitivity });
    }

    public delete(blob: EntityAttachmentMetadata) {
        return this.storageService.delete(blob.id);
    }
}
