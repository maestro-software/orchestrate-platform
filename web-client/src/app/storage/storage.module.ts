import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatIconModule } from "@angular/material/icon";
import { MatListModule } from "@angular/material/list";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatTooltipModule } from "@angular/material/tooltip";
import { AppButtonModule } from "app/common-ux/button/button.module";
import { AppDialogModule } from "app/common-ux/dialog/dialog.module";
import { AppLoadingModule } from "app/common-ux/loading/loading.module";
import { BlobImageDirective } from "./blob-image.directive";
import { EntityAttachmentsDialogComponent } from "./entity-attachments-dialog/entity-attachments-dialog.component";
import { EntityAttachmentsComponent } from "./entity-attachments.component/entity-attachments.component";
import { FileIconComponent } from "./file-icon.component/file-icon.component";
import { FormatBytesPipe } from "./format-bytes.pipe";
import { SelectFilesComponent } from "./select-files.component/select-files.component";
import { SelectImageComponent } from "./select-image/select-image.component";
import { StorageService } from "./storage.service";

@NgModule({
    imports: [
        CommonModule,
        MatListModule,
        MatIconModule,
        MatProgressBarModule,
        MatTooltipModule,
        AppDialogModule,
        AppButtonModule,
        AppLoadingModule,
    ],
    exports: [
        BlobImageDirective,
        SelectImageComponent,
        EntityAttachmentsComponent,
        EntityAttachmentsDialogComponent,
    ],
    declarations: [
        BlobImageDirective,
        SelectImageComponent,
        EntityAttachmentsDialogComponent,
        EntityAttachmentsComponent,
        SelectFilesComponent,
        FileIconComponent,
        FormatBytesPipe,
    ],
    providers: [
        StorageService,
    ],
})
export class AppStorageModule {}
