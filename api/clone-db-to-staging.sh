#!/bin/bash

# Ensure you have installed [flyctl](https://fly.io/docs/flyctl/installing/), set up the
# [Wireguard VPN](https://fly.io/docs/reference/private-networking/#creating-your-tunnel-configuration)
# and connected to it prior to running this script.

DB_HOST=orchestrate-pg-16.internal
PROD_DB=orchestrate_api
STAGING_DB=orchestrate_staging_api

echo -n Postgres password:
read -s POSTGRES_PASSWORD
echo # Force new line for Postgres output
export PGPASSWORD="$POSTGRES_PASSWORD"

PROD_ARGS="-h $DB_HOST -U postgres -d $PROD_DB"
STAGING_ARGS="-h $DB_HOST -U postgres -d $STAGING_DB"
PSQL_STAGING="psql $STAGING_ARGS"

$PSQL_STAGING -c "DROP SCHEMA IF EXISTS public CASCADE;"
$PSQL_STAGING -c "CREATE SCHEMA IF NOT EXISTS public AUTHORIZATION pg_database_owner;"

# Saving to a file rather than piping uses less memory on the DB
# So we'll do this while our DB is small
DUMP_FILE=tmp.dump
pg_dump $PROD_ARGS --verbose --format=custom --file $DUMP_FILE
# Blobs can take a long time, so don't allow a timeout for restoring them
# Probably can remove when we store blobs on a dedicated storage provider
PGOPTIONS="-c statement_timeout=0" pg_restore $STAGING_ARGS --verbose $DUMP_FILE
rm $DUMP_FILE

# Clear sensitive data
clear_column_data() {
    TABLE=$1
    COLUMN=$2
    VALUE=$3

    EXISTS=$($PSQL_STAGING --no-align --tuples-only --quiet -c "SELECT 1 FROM information_schema.columns WHERE table_name = '$TABLE' AND column_name = '$COLUMN'")
    if [ -n "$EXISTS" ]; then
        $PSQL_STAGING -c "UPDATE $TABLE set $COLUMN = $VALUE WHERE $COLUMN IS NOT NULL"
    else
        echo "$TABLE.$COLUMN not found, skipping"
    fi
}
clear_column_data tenant plan_stripe_customer_id NULL
clear_column_data plan_metadata stripe_product_id "''"
clear_column_data plan_metadata stripe_price_id "''"
