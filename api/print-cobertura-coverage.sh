#!/usr/bin/env bash

if [[ $# -lt 1 ]]; then
    echo >&2 "You have to provide the coverage report path at least."
    exit 2
fi

REPORT_PATH="${1}"
LINE_RATE="$(head -n 2 ${REPORT_PATH} | sed 'N;s/.*line-rate="\([^" ]*\).*/\1/g')"
COVERAGE=$(echo - | awk "{print ${LINE_RATE} * 100}")

printf "TOTAL_COVERAGE=%2.2f\n" "$COVERAGE"
