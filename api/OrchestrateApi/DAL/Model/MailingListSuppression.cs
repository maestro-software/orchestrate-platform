using System.ComponentModel.DataAnnotations.Schema;

namespace OrchestrateApi.DAL.Model;

public class MailingListSuppression : ITenanted
{
    public int Id { get; set; }
    public int MailingListId { get; set; }
    public required string Email { get; set; }
    public DateTime AddedAt { get; set; } = DateTime.UtcNow;
    public required string Reason { get; set; }
    public bool IsRemovable { get; set; }

    [ForeignKey(nameof(MailingListId))]
    public MailingList? MailingList { get; set; }
}
