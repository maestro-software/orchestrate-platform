using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OrchestrateApi.Common;

namespace OrchestrateApi.DAL.Model
{
    public enum FeeClass
    {
        Full,
        Concession,
        Special,
    }

    public class Member : ITenanted, IMembership, ITemporalLifetime
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; } = null!;

        [Required]
        public string LastName { get; set; } = null!;

        // TODO Convert DateJoined to computed property
        [TemporalLifetimeStart]
        public DateTime DateJoined { get; set; }

        // TODO Convert DateLeft to computed property
        [TemporalLifetimeFinish]
        public DateTime? DateLeft { get; set; }

        public virtual MemberDetails? Details { get; set; }

        public virtual ICollection<AssetLoan> AssetLoan { get; set; } = new HashSet<AssetLoan>();

        public virtual ICollection<EnsembleMembership> EnsembleMembership { get; set; } = new HashSet<EnsembleMembership>();

        public virtual ICollection<MemberInstrument> MemberInstrument { get; set; } = new HashSet<MemberInstrument>();

        public virtual ICollection<PerformanceMember> PerformanceMember { get; set; } = new HashSet<PerformanceMember>();
    }

    public class MemberDetails : ITenanted
    {
        [Key]
        public int MemberId { get; set; }

        [Required]
        [Phone]
        public string PhoneNo { get; set; } = null!;

        [EmailAddress]
        public string? Email { get; set; }

        [Required]
        public string Address { get; set; } = null!;

        public FeeClass FeeClass { get; set; }

        public string? Notes { get; set; }

        public Guid? UserId { get; set; }

        [ForeignKey(nameof(MemberId))]
        public virtual Member? Member { get; set; }

        [ForeignKey(nameof(UserId))]
        public virtual OrchestrateUser? User { get; set; }
    }
}
