using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OrchestrateApi.Common;

namespace OrchestrateApi.DAL.Model
{
    public class AssetLoan : IEntity, ITenanted, ITemporalLifetime
    {
        [Key]
        public int Id { get; set; }

        public int MemberId { get; set; }

        public int AssetId { get; set; }

        [TemporalLifetimeStart]
        public DateTime DateBorrowed { get; set; }

        [TemporalLifetimeFinish]
        public DateTime? DateReturned { get; set; }

        [ForeignKey(nameof(AssetId))]
        public virtual Asset? Asset { get; set; }

        [ForeignKey(nameof(MemberId))]
        public virtual Member? Member { get; set; }
    }
}
