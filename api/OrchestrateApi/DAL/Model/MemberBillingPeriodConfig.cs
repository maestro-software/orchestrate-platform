
using System.ComponentModel.DataAnnotations;

namespace OrchestrateApi.DAL.Model
{
    public class MemberBillingPeriodConfig : ITenanted
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public required string Name { get; set; }

        [Range(1, 12)]
        public int AutoGenerateMonth { get; set; }

        [Range(1, 12)]
        public int EndOfPeriodMonth { get; set; }
    }
}
