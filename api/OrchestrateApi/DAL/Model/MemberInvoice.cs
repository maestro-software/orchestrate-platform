using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrchestrateApi.DAL.Model
{
    public enum MemberInvoiceSendStatus
    {
        Pending,
        Delivered,
        Opened,
        Failed,
    }

    public class MemberInvoice : ITenanted
    {
        [Key]
        public int Id { get; set; }

        public int MemberBillingPeriodId { get; set; }

        public int MemberId { get; set; }

        public bool IsConcession { get; set; }

        [Required]
        public required string Reference { get; set; }

        public DateTime? Sent { get; set; }

        public MemberInvoiceSendStatus? SendStatus { get; set; }

        public DateTime? SendStatusUpdated { get; set; }

        public string? SendStatusMessage { get; set; }

        public DateTime? Paid { get; set; }

        public string? Notes { get; set; }

        [ForeignKey(nameof(MemberBillingPeriodId))]
        public MemberBillingPeriod? MemberBillingPeriod { get; set; }

        [ForeignKey(nameof(MemberId))]
        public Member? Member { get; set; }

        public virtual ICollection<MemberInvoiceLineItem> LineItems { get; set; } = new HashSet<MemberInvoiceLineItem>();
    }

    public class MemberInvoiceLineItem : ITenanted
    {
        [Key]
        public int Id { get; set; }

        public int MemberInvoiceId { get; set; }

        [Required]
        public required string Description { get; set; }

        public int AmountCents { get; set; }

        public int Ordinal { get; set; }

        [ForeignKey(nameof(MemberInvoiceId))]
        public MemberInvoice? MemberInvoice { get; set; }
    }
}
