using System.ComponentModel.DataAnnotations;

namespace OrchestrateApi.DAL.Model
{
    public enum EnsembleStatus
    {
        Active,
        Disbanded,
    }

    public class Ensemble : ITenanted
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public required string Name { get; set; }

        public EnsembleStatus Status { get; set; }

        public int MemberBillingPeriodFeeDollars { get; set; }

        public virtual ICollection<EnsembleMembership> EnsembleMembership { get; set; } = new HashSet<EnsembleMembership>();
        public virtual ICollection<EnsembleRole> EnsembleRole { get; set; } = new HashSet<EnsembleRole>();
        public virtual ICollection<Performance> Performance { get; set; } = new HashSet<Performance>();
    }
}
