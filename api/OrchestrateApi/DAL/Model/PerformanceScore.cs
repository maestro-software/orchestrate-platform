using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrchestrateApi.DAL.Model
{
    public class PerformanceScore : ITenanted
    {
        [Key]
        public int Id { get; set; }

        public int PerformanceId { get; set; }

        public int ScoreId { get; set; }

        [ForeignKey(nameof(PerformanceId))]
        public Performance? Performance { get; set; }

        [ForeignKey(nameof(ScoreId))]
        public Score? Score { get; set; }
    }
}
