using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrchestrateApi.DAL.Model
{
    public class Performance : ITenanted
    {
        [Key]
        public int Id { get; set; }

        public int EnsembleId { get; set; }
        public int ConcertId { get; set; }

        [ForeignKey(nameof(ConcertId))]
        public Concert? Concert { get; set; }

        [ForeignKey(nameof(EnsembleId))]
        public Ensemble? Ensemble { get; set; }

        public ICollection<PerformanceMember> PerformanceMembers { get; set; } = new HashSet<PerformanceMember>();

        public ICollection<PerformanceScore> PerformanceScores { get; set; } = new HashSet<PerformanceScore>();
    }
}
