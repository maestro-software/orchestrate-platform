using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OrchestrateApi.Common;

namespace OrchestrateApi.DAL.Model
{
    public class Asset : ITenanted, ITemporalLifetime
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Description { get; set; } = null!;

        [Required]
        public int Quantity { get; set; }

        public string? SerialNo { get; set; }

        [TemporalLifetimeStart]
        public DateTime? DateAcquired { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal? ValuePaid { get; set; }

        [TemporalLifetimeFinish]
        public DateTime? DateDiscarded { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal? ValueDiscarded { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal? InsuranceValue { get; set; }

        public string? CurrentLocation { get; set; }

        public string? Notes { get; set; }

        public virtual ICollection<AssetLoan> AssetLoan { get; set; } = new HashSet<AssetLoan>();

        public virtual ICollection<AssetAttachment> Attachments { get; set; } = new HashSet<AssetAttachment>();
    }

    public class AssetAttachment : ITenanted
    {
        public int Id { get; set; }

        public int AssetId { get; set; }

        public Guid BlobId { get; set; }

        [ForeignKey(nameof(AssetId))]
        public Asset? Asset { get; set; }
    }
}
