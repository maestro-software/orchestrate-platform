using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;

namespace OrchestrateApi.DAL.Access
{
    public static class EnsembleEntityAccess
    {
        public static IEnumerable<IEntityAccessor> Accessors { get; } = new List<IEntityAccessor>
        {
            new EntityAccessorBuilder<Ensemble>()
                .CanReadTableWhen(queryData => queryData.HasPermission(FeaturePermission.EnsembleRead))
                .CanReadAllRows()
                .CanWriteToTableWhen(queryData => queryData.HasPermission(FeaturePermission.EnsembleWrite))
                .Build(),
            new EntityAccessorBuilder<EnsembleRole>()
                .CanReadTableWhen(queryData => queryData.HasPermission(FeaturePermission.EnsembleRead))
                .CanReadAllRows()
                .CanWriteToTableWhen(queryData => queryData.HasPermission(FeaturePermission.PlatformSecurityAccess))
                .Build(),
        };
    }
}
