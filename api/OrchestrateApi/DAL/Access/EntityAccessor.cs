using System.Linq.Expressions;

namespace OrchestrateApi.DAL.Access
{
    public interface IEntityAccessor
    {
        Type Type { get; }

        bool CanReadTable(SecuredQueryData queryData);

        Expression? CanReadRow(SecuredQueryData queryData);

        bool CanWriteToTable(SecuredQueryData queryData);
    }

    public abstract class EntityAccessor<T> : IEntityAccessor
    {
        public Type Type => typeof(T);

        public abstract bool CanReadTable(SecuredQueryData queryData);

        Expression? IEntityAccessor.CanReadRow(SecuredQueryData queryData)
        {
            return CanReadRow(queryData);
        }

        public abstract Expression<Func<T, bool>>? CanReadRow(SecuredQueryData queryData);

        public abstract bool CanWriteToTable(SecuredQueryData queryData);
    }
}
