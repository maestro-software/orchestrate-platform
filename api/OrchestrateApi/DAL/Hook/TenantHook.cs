using Breeze.Persistence;
using Breeze.Persistence.EFCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.DAL.Hook
{
    public class TenantHookFactory : EntityHookFactory<Tenant>
    {
        public TenantHookFactory()
        {
        }

        public override IEntityHook<Tenant> BuildEntityHook(OrchestratePersistenceManager persistenceManager, Dictionary<Type, List<EntityInfo>> saveMap)
        {
            return new TenantHook
            {
                PersistenceManager = persistenceManager,
                SaveMap = saveMap,
            };
        }
    }

    public class TenantHook : EntityHook<Tenant>
    {
        public override IEnumerable<EntityError> Validate(IEntityInfo<Tenant> entityInfo)
        {
            var errors = new List<EntityError>();

            if (entityInfo.PropertyWasModified(m => m.Logo)
                && entityInfo.Entity.Logo != null
                && !FileTypeDetector.Images.AnyMatchingFileType(entityInfo.Entity.Logo))
            {
                errors.Add(new EFEntityError(
                    entityInfo.Raw,
                    "InvalidImage",
                    "Logo is not a valid image",
                    nameof(Tenant.Logo)));
            }

            return errors;
        }

        public override Task<IEnumerable<EntityError>> BeforeSaveAsync(IEnumerable<IEntityInfo<Tenant>> entityInfos, CancellationToken cancellationToken)
            => Task.FromResult(Enumerable.Empty<EntityError>());

        public override Task<IEnumerable<EntityError>> AfterSaveAsync(IEnumerable<IEntityInfo<Tenant>> entityInfos, CancellationToken cancellationToken)
            => Task.FromResult(Enumerable.Empty<EntityError>());
    }
}
