using OrchestrateApi.Security;
using EntityState = Breeze.Persistence.EntityState;
using SaveMap = System.Collections.Generic.Dictionary<System.Type, System.Collections.Generic.List<Breeze.Persistence.EntityInfo>>;

namespace OrchestrateApi.DAL.CustomSave
{
    public interface ICustomSave
    {
        FeaturePermission RequiredPermission { get; }

        SaveMap BeforeSave(SaveMap saveMap);

        Task AfterSaveAsync(OrchestratePersistenceManager persistenceManager, SaveMap saveMap, CancellationToken cancellationToken = default);
    }

    public abstract class SingleEntityCustomSave<T> : ICustomSave
    {
        public abstract FeaturePermission RequiredPermission { get; }

        public SaveMap BeforeSave(SaveMap saveMap)
        {
            if (saveMap.Count != 1 || !saveMap.TryGetValue(typeof(T), out var entityInfos)
                || entityInfos.Count != 1 || entityInfos[0].EntityState != EntityState.Unchanged)
            {
                throw new InvalidSaveDataException($"Expected single unchanged {typeof(T).Name} in save");
            }
            if (saveMap[typeof(T)][0].Entity is not T entity || !EntityIsPreSaveValid(entity))
            {
                throw new InvalidSaveDataException("Entity is not valid for this custom save");
            }

            return saveMap;
        }

        // Internal here (and below) to allow for easier testing of implementations
        /// <summary>Returns true if the target is in a valid state to allow this custom save</summary>
        internal abstract bool EntityIsPreSaveValid(T? entity);

        public async Task AfterSaveAsync(OrchestratePersistenceManager persistenceManager, SaveMap saveMap, CancellationToken cancellationToken = default)
        {
            var entityInfo = saveMap[typeof(T)][0];
            var entity = (T)entityInfo.Entity;

            var entry = persistenceManager.Context.Entry(entity);
            if (entry.State == Microsoft.EntityFrameworkCore.EntityState.Detached)
            {
                entry.State = Microsoft.EntityFrameworkCore.EntityState.Unchanged;
            }
            await ModifyEntity(persistenceManager.Context, entity, cancellationToken);

            // Detect changes so we can just use the below IsModifed flag
            entry.DetectChanges();

            entityInfo.OriginalValuesMap ??= new();
            foreach (var property in entry.Properties.Where(p => p.IsModified))
            {
                entityInfo.OriginalValuesMap[property.Metadata.Name] = property.OriginalValue;
            }
            if (entityInfo.OriginalValuesMap.Count > 0)
            {
                entityInfo.EntityState = EntityState.Modified;
            }

            var modifiedEntities = await SaveLogicAsync(persistenceManager.Context, entity, cancellationToken);
            foreach (var modifiedEntity in modifiedEntities)
            {
                saveMap.Add(persistenceManager.CreateEntityInfo(modifiedEntity, EntityState.Modified));
            }
        }

        /// <summary>Modify the target entity, any changes will be returned to the client</summary>
        internal abstract Task ModifyEntity(OrchestrateContext context, T entity, CancellationToken cancellationToken);

        /// <summary>Modify other entities, returning the ones that should be returned to the client</summary>
        internal abstract Task<IEnumerable<object>> SaveLogicAsync(OrchestrateContext context, T entity, CancellationToken cancellationToken);
    }
}
