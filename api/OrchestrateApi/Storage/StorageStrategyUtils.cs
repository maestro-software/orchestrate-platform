using System.Diagnostics.CodeAnalysis;
using System.Text.Json;

namespace OrchestrateApi.Storage;

public static class StorageStrategyUtils
{
    private static readonly JsonSerializerOptions serializerOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

    /// <summary>
    /// Helper method to parse document to JSON returning true if it is valid and not null, false otherwise.
    /// Uses <see cref="JsonSerializerDefaults.Web" />
    /// </summary>
    public static bool TryParseRequiredJsonData<T>(JsonDocument rawData, [NotNullWhen(true)] out object? data)
    {
        var isValid = TryParseRequiredJsonData(rawData, out T? parsedData);
        data = parsedData;
        return isValid;
    }

    /// <summary>
    /// Helper method to parse document to JSON returning true if it is valid and not null, false otherwise.
    /// Uses <see cref="JsonSerializerDefaults.Web" />
    /// </summary>
    public static bool TryParseRequiredJsonData<T>(JsonDocument rawData, out T? data)
    {
        try
        {
            data = JsonSerializer.Deserialize<T>(rawData, serializerOptions);
            return data is not null;
        }
        catch (JsonException)
        {
            data = default;
            return false;
        }
    }
}
