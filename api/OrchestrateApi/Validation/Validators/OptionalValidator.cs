using System.Diagnostics.CodeAnalysis;
using FluentValidation;
using FluentValidation.Results;
using FluentValidation.Validators;
using OrchestrateApi.Common;

namespace OrchestrateApi.Validation.Validators;

public class OptionalValidator<T> : AbstractValidator<Optional<T>>
{
    private readonly IValidator<T> actualValidator;

    public OptionalValidator(IValidator<T> actualValidator)
    {
        this.actualValidator = actualValidator;
    }

    public override ValidationResult Validate(ValidationContext<Optional<T>> context)
    {
        return context.InstanceToValidate.HasValue
            ? this.actualValidator.Validate(GetChildContext(context))
            : base.Validate(context);
    }

    public override Task<ValidationResult> ValidateAsync(ValidationContext<Optional<T>> context, CancellationToken cancellation = default)
    {
        return context.InstanceToValidate.HasValue
            ? this.actualValidator.ValidateAsync(GetChildContext(context), cancellation)
            : base.ValidateAsync(context, cancellation);
    }

    // Based off https://github.com/FluentValidation/FluentValidation/blob/a796e460fff7339bc686e146d70cc94811f15be5/src/FluentValidation/Validators/ChildValidatorAdaptor.cs#L94
    private static ValidationContext<T> GetChildContext(ValidationContext<Optional<T>> parentContext)
    {
        return parentContext.CloneForChildValidator(
            parentContext.InstanceToValidate.Value,
            preserveParentContext: true);
    }
}

public class OptionalPropertyValidator<T, TProperty> : PropertyValidator<T, Optional<TProperty>>
{
    private readonly IPropertyValidator<T, TProperty> actualValidator;

    public OptionalPropertyValidator(IPropertyValidator<T, TProperty> actualValidator)
    {
        this.actualValidator = actualValidator;
        Name = $"Optional{this.actualValidator.Name}";
    }

    public override string Name { get; }

    [SuppressMessage("", "CA1725")]
    public override bool IsValid(ValidationContext<T> context, Optional<TProperty> optional)
    {
        return !optional.HasValue || this.actualValidator.IsValid(context, optional.Value);
    }

    protected override string GetDefaultMessageTemplate(string errorCode)
    {
        return this.actualValidator.GetDefaultMessageTemplate(errorCode);
    }
}

public class OptionalAsyncPropertyValidator<T, TProperty> : AsyncPropertyValidator<T, Optional<TProperty>>
{
    private readonly IAsyncPropertyValidator<T, TProperty> actualValidator;

    public OptionalAsyncPropertyValidator(IAsyncPropertyValidator<T, TProperty> actualValidator)
    {
        this.actualValidator = actualValidator;
        Name = $"Optional{this.actualValidator.Name}";
    }

    public override string Name { get; }

    [SuppressMessage("", "CA1725")]
    public override Task<bool> IsValidAsync(
        ValidationContext<T> context, Optional<TProperty> optional, CancellationToken cancellation)
    {
        return optional.HasValue
            ? this.actualValidator.IsValidAsync(context, optional.Value, cancellation)
            : Task.FromResult(true);
    }

    protected override string GetDefaultMessageTemplate(string errorCode)
    {
        return this.actualValidator.GetDefaultMessageTemplate(errorCode);
    }
}
