using System.Linq.Expressions;
using FluentValidation;
using FluentValidation.Validators;
using LinqKit;
using Microsoft.EntityFrameworkCore;

namespace OrchestrateApi.Validation.Validators;

public class CaseInsensitiveUniqueValidator<T, TEntity> : AsyncPropertyValidator<T, string>
{
    private readonly IQueryable<TEntity> entities;
    private readonly Expression<Func<TEntity, string>> getField;
    private readonly Tuple<int, Expression<Func<TEntity, int>>>? idInfo;

    public CaseInsensitiveUniqueValidator(
        IQueryable<TEntity> entities,
        Expression<Func<TEntity, string>> getField)
    {
        this.entities = entities;
        this.getField = getField;
    }

    public CaseInsensitiveUniqueValidator(
        int? entityId,
        IQueryable<TEntity> entities,
        Expression<Func<TEntity, int>> getId,
        Expression<Func<TEntity, string>> getField)
    {
        this.entities = entities;
        this.getField = getField;

        if (entityId.HasValue)
        {
            this.idInfo = Tuple.Create(entityId.Value, getId);
        }
    }

    public override string Name => "CaseInsensitiveUniqueValidator";

    public string ErrorMessage { get; set; } = "That value already exists";

    public async override Task<bool> IsValidAsync(ValidationContext<T> context, string value, CancellationToken cancellation)
    {
        var entities = this.entities.AsExpandable();
        if (idInfo is not null)
        {
            entities = entities.Where(e => idInfo.Item2.Invoke(e) != idInfo.Item1);
        }

        var exists = await entities.AnyAsync(e => EF.Functions.ILike(getField.Invoke(e), value.Trim()), cancellation);
        return !exists;
    }

    protected override string GetDefaultMessageTemplate(string errorCode) => ErrorMessage;
}
