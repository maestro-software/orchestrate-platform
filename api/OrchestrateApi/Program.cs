using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;
using Sentry.AspNetCore;

namespace OrchestrateApi
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1052:Static holder types should be Static or NotInheritable", Justification = "Used as type parameter")]
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            var command = args.FirstOrDefault();
            switch (command)
            {
                case "generate-metadata":
                    var metadata = host.GetBreezeMetadata();
                    await Console.Out.WriteLineAsync(metadata);
                    break;
                case "migrate-database":
                    await host.MigrateDatabaseAsync();
                    break;
                case "run-periodic-jobs":
                    await host.TryRunPeriodicJobsAsync();
                    break;
                default:
                    await host.RunAsync();
                    break;
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseUrls("http://*:5000", "http://*:9091");
                    webBuilder.UseSentry((WebHostBuilderContext webHost, SentryAspNetCoreOptions options) =>
                    {
                        // If we don't explicitly set this, then Sentry seems to use the
                        // lowercase environment name in performance transactions for some reason
                        // (even if we set options.AdjustStandardEnvironmentNameCasing = false)
                        // See https://github.com/getsentry/sentry-dotnet/issues/1267
                        options.Environment = webHost.HostingEnvironment.EnvironmentName;
                        // Ensure we send the user who performed the request
                        options.SendDefaultPii = true;
                        options.SetBeforeSend(@event =>
                        {
                            // Don't send development events, we should already see these errors!
                            return webHost.HostingEnvironment.IsDevelopment()
                                ? null
                                : @event;
                        });
                        options.TransactionNameProvider = httpContext =>
                        {
                            // Just return the requested path for unknown routes
                            // Sentry prepends this with the HTTP method so we don't need to do that
                            return httpContext.Request.Path;
                        };
                        options.TracesSampler = context =>
                        {
                            // Ideally we'd filter out requests to unknown routes here as well
                            // But there doesn't seem to be a way to detect that from the parameters
                            // given. See https://github.com/getsentry/sentry-dotnet/issues/1468
                            if (context.TryGetHttpMethod() == "OPTIONS")
                            {
                                return 0.0;
                            }
                            else if (IsUntracedPath(context.TryGetHttpPath()))
                            {
                                return 0.0;
                            }

                            return null;
                        };
                        options.TracesSampleRate = webHost.HostingEnvironment.IsDevelopment()
                            ? 0.0
                            // Will need to be adjusted as load increases
                            : 0.5;
                    });
                });

        private static readonly string[] UntracedPathPrefixes = { "/health-check", "/metrics" };
        private static bool IsUntracedPath(string? path)
        {
            return path != null && UntracedPathPrefixes.Any(p => path.StartsWith(p, StringComparison.Ordinal));
        }
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "We want to log all exceptions")]
    internal static class ProgramExtensions
    {
        public static async Task<IHost> MigrateDatabaseAsync(this IHost host)
        {
            using var scope = host.Services.CreateScope();
            var serviceProvider = scope.ServiceProvider;

            var logger = serviceProvider.GetRequiredService<ILogger<Program>>();
            logger.LogInformation("Attempting to migrate database");

            try
            {
                var context = serviceProvider.GetRequiredService<OrchestrateContext>();
                await context.Database.MigrateAsync();
                logger.LogInformation("Database migrated successfully!");

                var roleService = serviceProvider.GetRequiredService<RoleManager<OrchestrateRole>>();
                foreach (var roleName in StandardRole.AllRoleNames)
                {
                    if (!await roleService.RoleExistsAsync(roleName))
                    {
                        var result = await roleService.CreateAsync(new OrchestrateRole
                        {
                            Name = roleName,
                            ConcurrencyStamp = Guid.NewGuid().ToString(),
                        });
                        if (result.Succeeded)
                        {
                            logger.LogInformation("Created missing role {RoleName}", roleName);
                        }
                        else
                        {
                            logger.LogError(
                                "Unable to create {RoleName} role: {ErrorText}",
                                roleName,
                                string.Join(", ", result.Errors.Select(e => e.Description)));
                        }
                    }
                }
                logger.LogInformation("Role checks completed");
            }
            catch (Exception e)
            {
                logger.LogError(e, "Failed to migrate database");
            }

            return host;
        }

        public static string GetBreezeMetadata(this IHost host)
        {
            using var scope = host.Services.CreateScope();

            var serviceProvider = scope.ServiceProvider;
            var persistenceManager = serviceProvider.GetRequiredService<OrchestratePersistenceManager>();
            return persistenceManager.Metadata();
        }

        public static async Task TryRunPeriodicJobsAsync(this IHost host)
        {
            using var scope = host.Services.CreateScope();
            var dbContext = scope.ServiceProvider.GetRequiredService<OrchestrateContext>();
            var logger = scope.ServiceProvider.GetRequiredService<ILogger<Program>>();
            var appConfiguration = scope.ServiceProvider.GetRequiredService<IAppConfiguration>();
            var lifetime = scope.ServiceProvider.GetRequiredService<IHostApplicationLifetime>();

            logger.LogInformation("Checking & running periodic jobs");

            foreach (var periodicJob in scope.ServiceProvider.GetServices<IPeriodicJob>())
            {
                using var logScope = logger.BeginScope(periodicJob.Name);
                var runner = new PeriodicJobRunner(dbContext, periodicJob, logger)
                {
                    MaxExpectedRunTime = appConfiguration.PeriodicJobMaxExpectedTime,
                };
                await runner.TryRun(lifetime.ApplicationStopping);
            }

            logger.LogInformation("All periodic jobs completed");
        }
    }
}
