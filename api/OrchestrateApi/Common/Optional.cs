using System.Diagnostics.CodeAnalysis;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Json.Serialization.Metadata;
namespace OrchestrateApi.Common;

public interface IOptional
{
    bool HasValue { get; }
}

[SuppressMessage("", "CA1716")]
public struct Optional<T> : IOptional, IEquatable<Optional<T>>
{
    private readonly T value;

    public Optional()
    {
        HasValue = false;
        this.value = default!;
    }

    public Optional(T value)
    {
        HasValue = true;
        this.value = value;
    }

    public bool HasValue { get; private set; }

    public readonly T Value => HasValue ? value : throw new InvalidOperationException("Attempted to retrieve unset optional value");

    public readonly bool TryGet(out T value)
    {
        value = HasValue ? this.value : default!;
        return HasValue;
    }

    [SuppressMessage("", "CA2225")]
    public static implicit operator Optional<T>(T value) => new Optional<T>(value);

    public static bool operator ==(Optional<T> lhs, Optional<T> rhs)
    {
        return lhs.Equals(rhs);
    }

    public static bool operator !=(Optional<T> lhs, Optional<T> rhs)
    {
        return !lhs.Equals(rhs);
    }

    public static bool operator ==(Optional<T> lhs, T rhs)
    {
        return lhs.Equals(rhs);
    }

    public static bool operator !=(Optional<T> lhs, T rhs)
    {
        return !lhs.Equals(rhs);
    }

    public static bool operator ==(T lhs, Optional<T> rhs)
    {
        return rhs.Equals(lhs);
    }

    public static bool operator !=(T lhs, Optional<T> rhs)
    {
        return !rhs.Equals(lhs);
    }

    public override readonly bool Equals(object? obj)
    {
        return obj is Optional<T> o && Equals(o)
            || obj is T o2 && Equals(o2);
    }

    public readonly bool Equals(Optional<T> other)
    {
        if (HasValue)
        {
            return other.HasValue && Equals(this.value, other.value);
        }
        else
        {
            return HasValue == other.HasValue;
        }
    }

    public override readonly int GetHashCode()
    {
        return HasValue
            ? this.value is T v ? v.GetHashCode() : 0
            : HasValue.GetHashCode();
    }
}

// Implementation modelled off https://learn.microsoft.com/en-us/dotnet/standard/serialization/system-text-json/converters-how-to?pivots=dotnet-6-0#sample-factory-pattern-converter
public class OptionalJsonConverter : JsonConverterFactory
{
    public override bool CanConvert(Type typeToConvert)
    {
        return typeToConvert.IsGenericType
            && typeToConvert.GetGenericTypeDefinition() == typeof(Optional<>);
    }

    public override JsonConverter? CreateConverter(Type typeToConvert, JsonSerializerOptions options)
    {
        var wrappedType = typeToConvert.GetGenericArguments()[0];
        var converter = (JsonConverter?)Activator.CreateInstance(
            typeof(OptionalJsonConverterInner<>).MakeGenericType(wrappedType));
        return converter;
    }

    private class OptionalJsonConverterInner<T> : JsonConverter<Optional<T?>>
    {
        public override Optional<T?> Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            // If we got to this point, then we have a value (even if it is null)
            var value = JsonSerializer.Deserialize<T?>(ref reader, options);
            return new Optional<T?>(value);
        }

        public override void Write(Utf8JsonWriter writer, Optional<T?> value, JsonSerializerOptions options)
        {
            if (!value.HasValue)
            {
                throw new InvalidOperationException($"Unable to write Optional with no value."
                    + $" Make sure you have added {nameof(OptionalJsonConverter.IgnoreUnsetOptionalValues)}"
                    + $" to the {nameof(JsonSerializerOptions.TypeInfoResolver)}"
                    + $" {nameof(DefaultJsonTypeInfoResolver.Modifiers)} property.");
            }

            JsonSerializer.Serialize(writer, value.Value, options);
        }
    }

    public static void IgnoreUnsetOptionalValues(JsonTypeInfo typeInfo)
    {
        foreach (var propertyInfo in typeInfo.Properties)
        {
            if (propertyInfo.PropertyType.IsAssignableTo(typeof(IOptional)))
            {
                propertyInfo.ShouldSerialize = static (obj, value) =>
                    value != null && ((IOptional)value).HasValue;
            }
        }
    }
}

public class OptionalMapper<T> : AutoMapper.ITypeConverter<Optional<T>, T>
{
    public T Convert(Optional<T> source, T destination, AutoMapper.ResolutionContext context)
    {
        return source.HasValue
            ? source.Value
            : destination;
    }
}
