using System.Reflection;

namespace OrchestrateApi.Common
{
    public class EmbeddedFile
    {
        private readonly Assembly? assembly;
        private readonly string filePath;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmbeddedFile"/> class.
        /// Creates a reference to a file which has a base path of the given
        /// type at the given relative path.
        /// </summary>
        /// <param name="type">A type to consider as the the base path.</param>
        /// <param name="relativePath">A dot separated path for the file.</param>
        public EmbeddedFile(Type type, string relativePath)
        {
            this.assembly = Assembly.GetAssembly(type);
            this.filePath = type.Namespace + "." + relativePath;
        }

        public byte[] AsByteArray()
        {
            using var fileStream = OpenReadStream();
            using var memoryStream = new MemoryStream();
            fileStream.CopyTo(memoryStream);
            return memoryStream.ToArray();
        }

        public string AsString()
        {
            using var fileStream = OpenReadStream();
            using var streamReader = new StreamReader(fileStream);
            return streamReader.ReadToEnd();
        }

        public Stream OpenReadStream()
        {
            if (assembly?.GetManifestResourceStream(filePath) is not { } fileStream)
            {
                throw new FileNotFoundException("Unable to find embedded resource, did you add it " +
                    "to the csproj file?");
            }

            return fileStream;
        }
    }
}
