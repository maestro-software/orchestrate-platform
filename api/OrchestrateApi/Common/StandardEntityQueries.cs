using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;

namespace OrchestrateApi.Common
{
    public class StandardEntityQueries<T>
        where T : class, IEntity
    {
        private readonly ControllerBase controller;
        private readonly OrchestrateContext context;

        public StandardEntityQueries(ControllerBase controller, OrchestrateContext context)
        {
            this.controller = controller;
            this.context = context;
        }

        public async Task<ActionResult<IEnumerable<T>>> Get()
        {
            return await context.Set<T>().ToListAsync();
        }

        public async Task<ActionResult<T>> Get(int id)
        {
            var entity = await context.Set<T>().FindAsync(id);

            if (entity == null)
            {
                return controller.NotFound();
            }

            return entity;
        }

        public async Task<IActionResult> Patch(int id, JsonPatchDocument<T> patch)
        {
            if (await context.Set<T>().FindAsync(id) is T entity)
            {
                patch.ApplyTo(entity);
                await context.SaveChangesAsync();
                return controller.NoContent();
            }
            else
            {
                return controller.NotFound();
            }
        }

        public async Task<ActionResult<T>> Post(T entity, string actionName)
        {
            context.Set<T>().Add(entity);
            await context.SaveChangesAsync();

            return controller.CreatedAtAction(actionName, new { id = entity.Id }, entity);
        }

        public async Task Delete(int id)
        {
            var entity = await context.Set<T>().FindAsync(id);
            if (entity == null)
            {
                return;
            }

            context.Set<T>().Remove(entity);
            await context.SaveChangesAsync();
        }
    }
}
