using System.Text.Json;
using System.Text.Json.Serialization;

namespace OrchestrateApi.Common;

public class JsonDocumentConverter : JsonConverter<JsonDocument>
{
    public override JsonDocument Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        return JsonDocument.ParseValue(ref reader);
    }

    public override void Write(Utf8JsonWriter writer, JsonDocument value, JsonSerializerOptions options)
    {
        Write(writer, value.RootElement, options);
    }

    public void Write(Utf8JsonWriter writer, JsonElement value, JsonSerializerOptions options)
    {
        switch (value.ValueKind)
        {
            case JsonValueKind.Object:
                var policy = options.PropertyNamingPolicy;
                writer.WriteStartObject();
                foreach (var pair in value.EnumerateObject())
                {
                    writer.WritePropertyName(policy?.ConvertName(pair.Name) ?? pair.Name);
                    Write(writer, pair.Value, options);
                }
                writer.WriteEndObject();
                break;
            case JsonValueKind.Array:
                writer.WriteStartArray();
                foreach (var item in value.EnumerateArray())
                    Write(writer, item, options);
                writer.WriteEndArray();
                break;
            default:
                value.WriteTo(writer);
                break;
        }
    }
}
