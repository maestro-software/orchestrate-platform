using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;

namespace OrchestrateApi.Common;

public record class TenantContext(Guid Id, string Name);

public class TenantContextIterator(
    OrchestrateContext dbContext,
    ITenantService tenantService,
    ILogger<TenantContextIterator> logger)
{
    /// <summary>
    /// Iterate over all tenants in the database, continuing even if
    /// the callback throws an exception.
    /// </summary>
    /// <param name="transactionName">The name of the transaction executed by the callback</param>
    /// <param name="doTenantAction">Action to take for each callback</param>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pending>")]
    public async Task ForEachTenant(
        string transactionName,
        Func<TenantContext, Task> doTenantAction,
        CancellationToken cancellationToken = default)
    {
        var tenants = await dbContext.Tenant
            .IgnoreQueryFilters()
            .Select(t => new TenantContext(t.Id, t.Name))
            .ToListAsync(cancellationToken);

        foreach (var tenant in tenants)
        {
            var transaction = SentrySdk.StartTransaction(tenant.Name, transactionName);
            SentrySdk.ConfigureScope(scope => scope.Transaction = transaction);

            using var loggerScope = logger.BeginScope(tenant.Name);
            using var tenantServiceOverride = tenantService.ForceOverrideTenantId(tenant.Id);

            try
            {
                await doTenantAction(tenant);
            }
            catch (Exception e)
            {
                logger.LogError(e, "Error during {TransactionName}, continuing with next tenant", transactionName);
            }
            finally
            {
                transaction.Finish();
            }
        }
    }
}
