using System.Threading.Channels;
using Sentry;

namespace OrchestrateApi.Common
{
    // Implementations here taken/adapted from
    // https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/hosted-services?view=aspnetcore-5.0&tabs=visual-studio#queued-background-tasks
    // https://github.com/dotnet/extensions/issues/805#issuecomment-399864251

    public interface IBackgroundWorkItem
    {
        Task DoWork(IServiceProvider services, CancellationToken cancellationToken);
    }

    public interface IBackgroundTaskQueue
    {
        ValueTask QueueAsync(IBackgroundWorkItem workItem);

        ValueTask<IBackgroundWorkItem> DequeueAsync(CancellationToken cancellationToken);
    }

    public class BackgroundTaskQueue : IBackgroundTaskQueue
    {
        private readonly Channel<IBackgroundWorkItem> queue;

        public BackgroundTaskQueue()
        {
            // Might want to make this bounded at some point, maybe have some logging to notify
            // that it got full etc
            // But we're not expecting this to get hammered with work, so don't worry about it
            // right now
            this.queue = Channel.CreateUnbounded<IBackgroundWorkItem>();
        }

        public async ValueTask QueueAsync(IBackgroundWorkItem workItem)
        {
            ArgumentNullException.ThrowIfNull(workItem);
            await this.queue.Writer.WriteAsync(workItem);
        }

        public async ValueTask<IBackgroundWorkItem> DequeueAsync(CancellationToken cancellationToken)
        {
            var workItem = await this.queue.Reader.ReadAsync(cancellationToken);
            return workItem;
        }
    }

    public class BackgroundTaskHostedService : BackgroundService
    {
        private readonly IServiceProvider serviceProvider;
        private readonly IBackgroundTaskQueue taskQueue;
        private readonly ILogger logger;

        public BackgroundTaskHostedService(
            IServiceProvider serviceProvider,
            IBackgroundTaskQueue taskQueue,
            ILogger<BackgroundTaskHostedService> logger)
        {
            this.serviceProvider = serviceProvider;
            this.taskQueue = taskQueue;
            this.logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            this.logger.LogInformation($"Starting {nameof(BackgroundTaskHostedService)}");
            await BackgroundProcessing(stoppingToken);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("", "CA1031")]
        private async Task BackgroundProcessing(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var workItem = await this.taskQueue.DequeueAsync(stoppingToken);
                var transaction = SentrySdk.StartTransaction(workItem.GetType().Name, nameof(BackgroundTaskQueue));
                SentrySdk.ConfigureScope(scope => scope.Transaction = transaction);

                try
                {
                    this.logger.LogInformation("Beginning work on {WorkItem}", workItem.GetType().Name);

                    using var scope = this.serviceProvider.CreateScope();
                    await workItem.DoWork(scope.ServiceProvider, stoppingToken);

                    this.logger.LogInformation("Completed work on {WorkItem}", workItem.GetType().Name);
                }
                catch (Exception ex)
                {
                    this.logger.LogError(ex, "Error occurred executing {WorkItem}.", workItem.GetType().Name);
                }
                finally
                {
                    transaction.Finish();
                }
            }
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            this.logger.LogInformation($"Stopping {nameof(BackgroundTaskHostedService)}");
            await base.StopAsync(cancellationToken);
        }
    }
}
