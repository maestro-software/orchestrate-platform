using System.Linq.Expressions;
using System.Reflection;

namespace OrchestrateApi.Common
{
    public static class ReflectionUtils
    {
        public static MethodInfo GetGenericMethodInfo(Expression<Action> lambda)
        {
            return GetMethodInfo(lambda).GetGenericMethodDefinition();
        }

        // Adapted from https://gist.github.com/duncansmart/901329
        public static MethodInfo GetMethodInfo(Expression<Action> lambda)
        {
            if (lambda.Body is not MethodCallExpression callExpression)
            {
                throw new ArgumentException($"Unexpected lamda type: {lambda.Body.Type}");
            }

            return callExpression.Method;
        }
    }
}
