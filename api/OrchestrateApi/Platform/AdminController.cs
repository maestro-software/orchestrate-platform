using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OrchestrateApi.Common;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email.Postmark;
using OrchestrateApi.Security;

namespace OrchestrateApi.Platform;

[ApiController]
[Route("administration")]
[IsGlobalAdminAuthorize]
public class AdminController(
    PostmarkService postmarkService,
    UserManager<OrchestrateUser> userManager)
    : ControllerBase
{
    [HttpPost("reset-api-user-password")]
    public async Task<IActionResult> ResetApiUserPassword([FromBody] ApiUserPasswordDto dto)
    {
        var user = await userManager.FindByNameAsync(UserService.ApiUsername);
        if (user == null)
        {
            user = new OrchestrateUser
            {
                UserName = UserService.ApiUsername,
                Email = $"{UserService.ApiUsername}@orchestrate.community",
                SecurityStamp = Guid.NewGuid().ToString(),
            };
            var createResult = await userManager.CreateAsync(user, dto.Password);
            if (!createResult.Succeeded)
            {
                return IdentityError("Failed to create api user", createResult.Errors);
            }

            return Ok();
        }

        var token = await userManager.GeneratePasswordResetTokenAsync(user);
        var result = await userManager.ResetPasswordAsync(user, token, dto.Password);
        if (!result.Succeeded)
        {
            return IdentityError("Failed to reset api user password", result.Errors);
        }

        await postmarkService.UpdateWebhookApiUserPasswords(dto.Password);

        return Ok();
    }

    private ObjectResult IdentityError(string message, IEnumerable<IdentityError> errors)
    {
        return StatusCode(
            StatusCodes.Status500InternalServerError,
            new BasicErrorResponse(message, errors.Select(e => new ResultError
            {
                Code = e.Code,
                Description = e.Description
            }).ToArray()));
    }
}

public class ApiUserPasswordDto
{
    public required string Password { get; set; }
}
