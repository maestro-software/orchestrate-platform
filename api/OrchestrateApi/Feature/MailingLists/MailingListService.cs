using System.Linq.Expressions;
using System.Net.Mail;
using OrchestrateApi.Common;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Feature.MailingLists;

public record class MailingListAddressParameters(string TenantSlug, string Domain);

public class MailingListService(
    IAppConfiguration appConfiguration,
    ILogger<MailingListService> logger)
{
    public string ApexDomain => appConfiguration.MailingListApexDomain;

    public MailingListAddressParameters AddressParameters(Tenant tenant)
        => new(tenant.Abbreviation, ApexDomain);

    public string MailingListEmail(Tenant tenant, MailingList mailingList)
        => MailingListEmail(tenant, mailingList.Slug);

    public string MailingListEmail(Tenant tenant, string slug)
         => $"{tenant.Abbreviation}#{slug}@{ApexDomain}";


    public Expression<Func<MailingList, string>> MailingListEmailExpr(Tenant tenant)
    {
        return mailingList => $"{tenant.Abbreviation}#{mailingList.Slug}@{ApexDomain}";
    }

    public bool TryParseTenantAndSlug(
        string rawEmail,
        out string tenantAbbreviation,
        out string mailingListSlug)
    {
        tenantAbbreviation = string.Empty;
        mailingListSlug = string.Empty;

        if (!MailAddress.TryCreate(rawEmail, out var address))
        {
            return false;
        }

        if (address.Host != ApexDomain)
        {
            logger.LogDebug(
                "Received email not meant for this instance: {Email}. Expected domain to be {Domain}",
                rawEmail,
                ApexDomain);
            return false;
        }

        var tenantSplit = address.User.Split('#', 2, StringSplitOptions.TrimEntries);
        if (tenantSplit.Length != 2)
        {
            logger.LogDebug(
                "Received email that doesn't adhere to <tenant#slug> schema: {Email}",
                rawEmail);
            return false;
        }

        // Discard plus address
        var plusAddrSplit = tenantSplit[1].Split('+', 2, StringSplitOptions.TrimEntries);

        tenantAbbreviation = tenantSplit[0];
        mailingListSlug = plusAddrSplit[0];
        return !string.IsNullOrWhiteSpace(tenantAbbreviation)
            && !string.IsNullOrWhiteSpace(mailingListSlug);
    }
}
