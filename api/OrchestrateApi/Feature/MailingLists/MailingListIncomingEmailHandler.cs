using System.Globalization;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;
using OrchestrateApi.Email.Postmark;
using OrchestrateApi.Security;

namespace OrchestrateApi.Feature.MailingLists;

public class MailingListIncomingEmailHandler : IIncomingEmailHandler
{
    public const string MailingListEmailType = "MailingList";
    public const string MailingListIdMetadataKey = "MailingListId";
    public const string MailingListSlugMetadataKey = "MailingListSlug";

    public const string MessageIdHeaderName = "Message-ID";
    public const string InReplyToHeaderName = "In-Reply-To";
    public const string ReferencesHeaderName = "References";

    public const int MaxBodySizeBytes = PostmarkEmailProvider.MaxBodySizeBytes;
    public const int MaxMessageSizeBytes = PostmarkEmailProvider.MaxMessageSizeBytes;

    private readonly OrchestrateContext dbContext;
    private readonly MailingListService mailingListService;
    private readonly RecipientListService recipientListService;
    private readonly IEmailService emailService;
    private readonly ITenantService tenantService;
    private readonly UserManager<OrchestrateUser> userManager;
    private readonly ILogger logger;

    public MailingListIncomingEmailHandler(
        OrchestrateContext dbContext,
        MailingListService mailingListService,
        RecipientListService recipientListService,
        IEmailService emailService,
        ITenantService tenantService,
        UserManager<OrchestrateUser> userManager,
        ILogger<MailingListIncomingEmailHandler> logger)
    {
        this.dbContext = dbContext;
        this.mailingListService = mailingListService;
        this.recipientListService = recipientListService;
        this.emailService = emailService;
        this.tenantService = tenantService;
        this.userManager = userManager;
        this.logger = logger;
    }

    public async Task HandleAsync(IncomingEmail email)
    {
        if (email.SpamInfo is { } spamInfo && spamInfo.IsSpam)
        {
            // If this gets triggered a lot, we may have to drop them.
            // (Maybe only if AllowedSenders == All?)
            // Ideally we maybe would put in a queue for committee to manually review
            this.logger.LogError(
                "Detected spam (score {Score}) sent from {From} with subject {Subject}. Letting through... for now.",
                spamInfo.Score,
                email.From,
                email.Subject);
        }
        if (email.SpfInfo is { } spfInfo
            && spfInfo.Status != IncomingEmailSpfStatus.Neutral
            && spfInfo.Status != IncomingEmailSpfStatus.Pass)
        {
            this.logger.LogError(
                "Detected SPF header sent from {From} with subject {Subject}: {SpfMessage}. Letting through... for now",
                email.From,
                email.Subject,
                spfInfo.Message);
        }

        var textSize = Encoding.UTF8.GetByteCount(email.TextContent);
        var htmlSize = Encoding.UTF8.GetByteCount(email.HtmlContent);
        var attachmentSizes = email.Attachments
            .Select(e => new { e.Name, Size = Encoding.UTF8.GetByteCount(e.Base64Content) })
            .ToList();
        if (textSize > MaxBodySizeBytes
            || htmlSize > MaxBodySizeBytes
            || (textSize + htmlSize + attachmentSizes.Sum(a => a.Size) > MaxMessageSizeBytes))
        {
            this.logger.LogWarning(
                "Email is too large to process: Text={TextSize}B; Html={HtmlSize}B; {AttachmentSizes}",
                textSize,
                htmlSize,
                string.Join("; ", attachmentSizes.Select(a => $"{a.Name}={a.Size}B")));
            await this.emailService.SendAsync(BuildTooBigRejectionEmail(email));
            return;
        }

        var incomingRecipients = email.To
            .Union(email.Cc)
            .Union(email.Bcc)
            .DistinctBy(e => e.Email.ToUpperInvariant());
        foreach (var incomingRecipient in incomingRecipients)
        {
            if (!mailingListService.TryParseTenantAndSlug(
                incomingRecipient.Email,
                out var tenantAbbreviation,
                out var mailingListSlug))
            {
                this.logger.LogDebug("Not a mailing list address: {EmailAddress}", incomingRecipient);
                continue;
            }

            var tenant = await dbContext.Tenant
                .IgnoreQueryFilters()
                .SingleOrDefaultAsync(e => EF.Functions.ILike(e.Abbreviation, tenantAbbreviation));
            if (tenant == null)
            {
                this.logger.LogWarning("Unable to find tenant from email {Email}", incomingRecipient);
                await this.emailService.SendAsync(BuildUndeliverableEmail(email, incomingRecipient));
                continue;
            }

            using var tenantLogScope = this.logger.BeginScope("{TenantName}", tenant.Name);
            using var tenantOverride = this.tenantService.ForceOverrideTenantId(tenant.Id);

            var mailingList = await this.dbContext.MailingList
                .SingleOrDefaultAsync(e => EF.Functions.ILike(e.Slug, mailingListSlug));
            if (mailingList == null)
            {
                this.logger.LogWarning("Unable to find mailing list from email {Email}", incomingRecipient);
                await this.emailService.SendAsync(BuildUndeliverableEmail(email, incomingRecipient));
                continue;
            }

            using var listLogScope = this.logger.BeginScope("{MailingListName}", mailingList.Name);

            var recipients = await this.recipientListService.GetDeliverableRecipientListAsync(mailingList);
            if (!await AllowedToSend(mailingList, email.From, recipients))
            {
                this.logger.LogWarning("Sender ({Sender}) doesn't have permission to send to mailing list", email.From.Email);
                await this.emailService.SendAsync(BuildUndeliverableEmail(email, incomingRecipient));
                continue;
            }

            var outgoingRecipients = recipients
                .Where(r => !r.Email.Equals(email.From.Email, StringComparison.OrdinalIgnoreCase))
                .Select(r => new EmailMessageAddress(r.Name, r.Email!))
                .ToList();
            this.logger.LogInformation("Sending to {RecipientCount} recipients", outgoingRecipients.Count);

            var fromName = string.IsNullOrWhiteSpace(email.From.Name)
                ? email.From.Email
                : email.From.Name;
            var from = new EmailMessageAddress(
                $"{fromName} via Orchestrate",
                this.mailingListService.MailingListEmail(tenant, mailingList));
            EmailMessageAddress? replyTo = mailingList.ReplyTo switch
            {
                ReplyStrategy.MailingList => new(mailingList.Name, incomingRecipient.Email),
                ReplyStrategy.Sender => new(email.From.Name, email.From.Email),
                _ => null,
            };

            var textFooter = string.Empty;
            var htmlFooter = string.Empty;
            if (!string.IsNullOrWhiteSpace(mailingList.Footer))
            {
                textFooter += "\n\n---\n\n" + mailingList.Footer;
                htmlFooter += "<hr/>" + mailingList.Footer
                    .ReplaceLineEndings()
                    .Replace(Environment.NewLine, "<br/>");
            }

            textFooter += "\n\n---\n\n"
                + "You received this email because you are a member of the "
                + $"{tenant.Name} {mailingList.Name} mailing list ({incomingRecipient.Email}). "
                + "To unsubscribe, please click this link: "
                + $"{EmailHelpers.UnsubscribeLinkStartSentinal}{EmailHelpers.UnsubscribeLinkEndSentinal}";
            htmlFooter += $@"
                <hr/>
                <p>
                    You received this email because you are a member of the
                    {tenant.Name} {mailingList.Name} mailing list ({incomingRecipient.Email}).
                    To unsubscribe, please click
                    {EmailHelpers.UnsubscribeLinkHtmlEncodedStartSentinal}
                    {EmailHelpers.BuildUnsubscribeHtml(string.Empty)}
                    {EmailHelpers.UnsubscribeLinkHtmlEncodedEndSentinal}.
                </p>";

            var textContent = AppendFooterIfNotPresent(email.TextContent, textFooter);
            var htmlContent = AppendFooterIfNotPresent(email.HtmlContent, htmlFooter);

            var headers = new Dictionary<string, string>();
            var propogatingHeaders = new[] { MessageIdHeaderName, InReplyToHeaderName, ReferencesHeaderName };
            foreach (var headerName in propogatingHeaders)
            {
                if (email.Headers.FirstOrDefault(h => IsHeader(h, headerName)) is { } header)
                {
                    headers.Add(headerName, header.Value);
                }
            }

            var emails = outgoingRecipients.Select(r => new EmailMessage
            {
                From = from,
                ReplyTo = replyTo,
                To = r,
                Subject = email.Subject,
                TextContent = textContent,
                HtmlContent = htmlContent,
                Attachments = email.Attachments,
                Type = MailingListEmailType,
                Metadata =
                {
                    [MailingListIdMetadataKey] = mailingList.Id.ToString(CultureInfo.InvariantCulture),
                    [MailingListSlugMetadataKey] = mailingList.Slug,
                },
                Headers = headers,
                SendContext = new()
                {
                    Tenant = tenant,
                    MailingList = mailingList,
                },
            })
            .ToList();

            var results = await this.emailService.SendAsync(emails);
            this.dbContext.Add(new MailingListMessageMetadata
            {
                MailingList = mailingList,
                Sent = DateTime.UtcNow,
            });
            await this.dbContext.SaveChangesAsync();

            var failures = results.Where(e => e.Status == EmailResultStatus.Error).ToList();
            if (failures.Count > 0)
            {
                // TODO Notify sender?
                this.logger.LogError(
                    "Unable to send to {UndeliverableCount} recipients: {Recipients}",
                    failures.Count,
                    string.Join(", ", failures.Select(b => $"{b.Email.To} ({b.StatusMessage})")));
            }
        }
    }

    private static bool IsHeader(IncomingEmailHeader header, string name)
        => header.Name.Equals(name, StringComparison.OrdinalIgnoreCase);

    private static string? AppendFooterIfNotPresent(string? content, string footer)
    {
        if (string.IsNullOrWhiteSpace(content) || EmailHelpers.ContainsFooterSentinal(content))
        {
            return content;
        }

        return content + EmailHelpers.FooterSentinal + footer;
    }

    private async Task<bool> AllowedToSend(
        MailingList mailingList,
        IncomingEmailRecipient from,
        IList<MailingListDeliverableRecipient> recipientList)
    {
        if (mailingList.AllowedSenders == AllowedSenders.Anyone)
        {
            return true;
        }

        if (mailingList.AllowedSenders == AllowedSenders.RecipientsOnly
            && recipientList.Any(r => from.Email.Equals(r.Email, StringComparison.OrdinalIgnoreCase)))
        {
            return true;
        }

        var sender = await this.userManager.FindByEmailAsync(from.Email);
        if (sender == null)
        {
            return false;
        }

        var permissions = await userManager.GetPermissionsAsync(sender);
        return permissions.Contains(FeaturePermission.MailingListGlobalSend);
    }

    private EmailMessage BuildUndeliverableEmail(IncomingEmail email, IncomingEmailRecipient recipient) => new EmailMessage
    {
        From = this.emailService.DefaultFrom,
        To = new(email.From.Name, email.From.Email),
        Subject = $"Undeliverable: {email.Subject}",
        TextContent = BuildIfNotNull(email.TextContent, t => "Hi,\n"
            + $"{recipient.Email} either doesn't exist or you don't have permission to send to it."
            + " Please double check the address and/or get in touch with a committee member to"
            + " request access. The original email is included below, for your reference."
            + "\n\n---\n\n"
            + t),
        HtmlContent = BuildIfNotNull(email.HtmlContent, t => $@"
            <p>Hi,<p>
            <p>
                {recipient.Email} either doesn't exist or you don't have permission to send to it.
                Please double check the address and/or get in touch with a committee member to
                request access. The original email is included below, for your reference.
            </p>
            <hr/>
            {t}
        "),
    };

    private EmailMessage BuildTooBigRejectionEmail(IncomingEmail email) => new EmailMessage
    {
        From = this.emailService.DefaultFrom,
        To = new(email.From.Name, email.From.Email),
        Subject = $"Rejected: {email.Subject}",
        TextContent = "Hi,\n"
            + $@"Unfortunately the email you tried to send with the subject ""{email.Subject}"""
            + " is too large to be processed by Orchestrate (maximum size of"
            + $" {MaxMessageSizeBytes / 1024 / 1024}MB)."
            + " If you are sending any large attachments please upload them to a file sharing"
            + " service and send a link instead.",
        HtmlContent = $@"
            <p>Hi,<p>
            <p>
                Unfortunately the email you tried to send with the subject ""{email.Subject}""
                is too large to be processed by Orchestrate (maximum size of
                {MaxMessageSizeBytes / 1024 / 1024}MB).
                If you are sending any large attachments please upload them to a file sharing
                service and send a link instead.
            </p>
        ",
    };

    private static string? BuildIfNotNull(string? input, Func<string, string> build)
        => string.IsNullOrWhiteSpace(input)
            ? null
            : build(input);
}
