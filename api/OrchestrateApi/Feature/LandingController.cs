using System.Security.Claims;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;

namespace OrchestrateApi.Feature;

[ApiController]
[Route("landing")]
[HasTenantPermissionAuthorize(FeaturePermission.PlatformAccess)]
public class LandingController(
    LandingService landingService,
    ITenantService tenantService,
    OrchestrateContext dbContext) : ControllerBase
{
    [HttpGet("summary")]
    public async Task<LandingSummaryDto> GetLandingSummaryAsync()
    {
        return new LandingSummaryDto
        {
            Personal = await GetPersonalDataAsync(),
            Tenant = await GetTenantDataAsync(),
        };
    }

    private async Task<PersonalDataDto?> GetPersonalDataAsync()
    {
        var memberId = await dbContext.Member
            .Select(e => e.Details!)
            .Where(e => e.UserId == User.UserId())
            .Select(e => (int?)e.MemberId)
            .SingleOrDefaultAsync();
        if (!memberId.HasValue)
        {
            return null;
        }

        return await landingService.BuildPersonalDataDtoAsync(memberId.Value);
    }

    private async Task<TenantDataDto> GetTenantDataAsync()
    {
        if (tenantService.TenantId is not Guid tenantId)
        {
            throw new InvalidOperationException("Expected tenantId to be populated");
        };

        var permissions = User.PermissionsInTenant(tenantId).ToList();
        return await landingService.BuildTenantDataDtoAsync(permissions);
    }
}

public class LandingSummaryDto
{
    public required PersonalDataDto? Personal { get; set; }

    public required TenantDataDto Tenant { get; set; }
}

public class PersonalDataDto
{
    public required int MemberId { get; set; }

    public required PersonalDetailsDto Details { get; set; }

    public required IEnumerable<MembershipDto> CurrentMembership { get; set; }

    public required int TotalPerformanceCount { get; set; }

    public required IEnumerable<PerformanceHistoryDto> PerformanceHistory { get; set; }

    public required InvoiceDto? LastInvoice { get; set; }

    public required IEnumerable<InvoiceDto> UnpaidInvoices { get; set; }

    public required IEnumerable<LoanedAssetDto> LoanedAssets { get; set; }

    public required IEnumerable<MailingListMembershipDto> MailingLists { get; set; }
}

public class PersonalDetailsDto
{
    public required string Name { get; set; }

    public required string? PhoneNo { get; set; }

    public required string? Address { get; set; }

    public required string? Email { get; set; }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public required FeeClass FeeClass { get; set; }

    public required IEnumerable<string> Instruments { get; set; }
}

public class MembershipDto
{
    public required int EnsembleId { get; set; }
    public required string EnsembleName { get; set; }

    public required DateTime DateJoined { get; set; }
}

public class PerformanceHistoryDto
{
    public required int Year { get; set; }

    public required string EnsembleName { get; set; }

    public required int PerformanceCount { get; set; }
}

public class InvoiceDto
{
    public required string Reference { get; set; }

    public required string BillingPeriodName { get; set; }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public required MemberBillingPeriodState BillingPeriodState { get; set; }

    public required decimal Total { get; set; }

    public required DateTime? EarlyBirdDue { get; set; }

    public required DateTime Due { get; set; }

    public required DateTime? Paid { get; set; }
}

public class LoanedAssetDto
{
    public required string AssetDescription { get; set; }

    public required string? SerialNo { get; set; }

    public required DateTime DateBorrowed { get; set; }
}

public class MailingListMembershipDto
{
    public required int Id { get; set; }
    public required string Name { get; set; }

    public required string Address { get; set; }
}

public class TenantDataDto
{
    public required TenantDetailsDto Details { get; set; }

    public required AssetSummaryDto? Assets { get; set; }

    public required MusicLibrarySummaryDto? MusicLibrary { get; set; }

    public required ContactsSummaryDto? Contacts { get; set; }

    public required IEnumerable<EnsembleSummaryDto> EnsembleSummaries { get; set; }

    public required MembershipSnapshotDto? MembershipSnapshot { get; set; }

    public required ConcertDto? LastConcert { get; set; }

    public required IEnumerable<BillingPeriodDto> LatestBillingPeriods { get; set; }

    public required IEnumerable<MailingListUsage> MailingListUsage { get; set; }
}

public class TenantDetailsDto
{
    public required string Name { get; set; }

    public required string? Website { get; set; }

    public required DateTime? DateFounded { get; set; }
}

public class AssetSummaryDto
{
    public required int Count { get; set; }

    public required int OnLoanCount { get; set; }
}

public class MusicLibrarySummaryDto
{
    public required int Count { get; set; }

    public required int NeverPlayedCount { get; set; }

    public required int PerformedInLastYearCount { get; set; }
}

public class ContactsSummaryDto
{
    public required int CurrentCount { get; set; }
}

public class EnsembleSummaryDto
{
    public required int Id { get; set; }

    public required string Name { get; set; }

    public required int MemberCount { get; set; }

    public required DateTime? FirstConcertDate { get; set; }

    public required int ConcertCount { get; set; }
}

public class MembershipSnapshotDto
{
    public required int JoinedCount { get; set; }

    public required int StayedCount { get; set; }

    public required int LeftCount { get; set; }

    public required int JoinedAndLeftCount { get; set; }
}

public class ConcertDto
{
    public required int Id { get; set; }

    public required string Occasion { get; set; }

    public required string? Location { get; set; }

    public required DateTime Date { get; set; }

    public required Guid? CoverPhotoBlobId { get; set; }

    public required IEnumerable<PerformanceDto> Performances { get; set; }
}

public class PerformanceDto
{
    public required int Id { get; set; }

    public required string EnsembleName { get; set; }

    public required int MemberCount { get; set; }

    public required int PieceCount { get; set; }
}

public class BillingPeriodDto
{
    public required int Id { get; set; }

    public required string Name { get; set; }

    public required DateTime? EarlyBirdDue { get; set; }

    public required DateTime Due { get; set; }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public required MemberBillingPeriodState State { get; set; }

    public required decimal DollarsReceived { get; set; }

    public required int InvoiceCount { get; set; }

    public required int PaidEarly { get; set; }

    public required int PaidOnTime { get; set; }

    public required int PaidLate { get; set; }

    public required int Unpaid { get; set; }
}

public class MailingListUsage
{
    public required int Id { get; init; }
    public required string Name { get; init; }
    public required int UsageCount { get; init; }
}
