using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.PaymentProcessor;
using OrchestrateApi.Security;

namespace OrchestrateApi.Feature.Settings;

[ApiController]
[Route("settings/subscription")]
[HasTenantPermissionAuthorize(FeaturePermission.SettingsWrite)]
public class SubscriptionSettingsController : ControllerBase
{
    private const decimal NfpPercentDiscountMultiplier = 0.5M;

    private readonly OrchestrateContext dbContext;
    private readonly SubscriptionSettingsService subscriptionService;
    private readonly IPaymentProcessor paymentProcessor;
    private readonly ILogger logger;

    public SubscriptionSettingsController(
        OrchestrateContext dbContext,
        SubscriptionSettingsService subscriptionService,
        IPaymentProcessor paymentProcessor,
        ILogger<SubscriptionSettingsController> logger)
    {
        this.dbContext = dbContext;
        this.subscriptionService = subscriptionService;
        this.paymentProcessor = paymentProcessor;
        this.logger = logger;
    }

    [HttpGet("details")]
    public async Task<SubscriptionDetailsDto> GetSubscriptionDetailsAsync()
    {
        var tenant = await this.dbContext.Tenant.SingleAsync();
        using var loggerScope = this.logger.BeginScope(tenant.Name);

        var dto = await this.subscriptionService.DefaultDetailsAsync(tenant);
        if (tenant.Plan.StripeCustomerId == null)
        {
            return dto;
        }

        var customer = await this.paymentProcessor.GetCustomerWithSubscriptionsAsync(tenant.Plan.StripeCustomerId);
        if (customer == null)
        {
            this.logger.LogWarning("Unknown CustomerId: {CustomerId}", tenant.Plan.StripeCustomerId);
            dto.CanAccessCustomerPortal = false;
            return dto;
        }

        var paidPlanMetadata = await this.dbContext.PaidPlanMetadata.ToListAsync();
        dto.PreviousInvoices = await this.subscriptionService.GetPreviousInvoiceDtosAsync(paidPlanMetadata, customer);
        dto.ContactDetails = this.subscriptionService.CustomerToContactDetailsDto(customer);

        var subscription = customer.Subscriptions?.FirstOrDefault();
        this.subscriptionService.PlanTypeSanityChecks(paidPlanMetadata, tenant, subscription);
        this.subscriptionService.NotForProfitDiscountSanityChecks(tenant, customer);

        if (subscription == null)
        {
            return dto;
        }

        dto.PaymentMethod = await this.subscriptionService.GetPaymentMethodDtoAsync(subscription);
        dto.MonthlyFeeDollars = subscription.Items.Sum(i => i.Quantity * i.Price.UnitAmount.GetValueOrDefault(0))
            * ((100 - (customer.Discount?.Coupon?.PercentOff ?? 0)) / 100M)
            / 100M;
        dto.UpcomingInvoice = await this.subscriptionService.GetUpcomingInvoiceDtoAsync(
            paidPlanMetadata, tenant, subscription);

        if (subscription.Status == Stripe.SubscriptionStatuses.Trialing)
        {
            dto.CurrentPlan = PlanType.Trial;
            dto.NextPlan = tenant.Plan.Type;
        }

        if (subscription.CancelAtPeriodEnd)
        {
            dto.NextPlan = PlanType.Free;
        }

        return dto;
    }

    [HttpGet("paid-plans")]
    public async Task<IList<PaidPlanDto>> GetPaidPlansAsync()
    {
        return await this.dbContext
            .PaidPlanMetadata
            .Select(e => new PaidPlanDto
            {
                PlanType = e.Type,
                MonthlyFeeDollars = e.MonthlyFeeCents / 100M,
                NfpMonthlyFeeDollars = e.MonthlyFeeCents / 100M * NfpPercentDiscountMultiplier,
                MaxMemberCount = e.MemberLimit,
            })
            .ToListAsync();
    }

    [HttpGet("customer-portal-url")]
    public async Task<ActionResult> GetCustomerPortalUrlAsync([FromQuery] Uri returnUrl)
    {
        var tenant = await this.dbContext.Tenant.SingleAsync();

        var customer = await this.subscriptionService.GetOrCreateCustomerAsync(tenant);
        if (customer == null)
        {
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        var session = await this.paymentProcessor.CreateBillingPortalSessionAsync(customer.Id, returnUrl);
        return Ok(session.Url);
    }

    [HttpGet("subscription-checkout-url")]
    public async Task<ActionResult<string>> GetSubscriptionCheckoutUrlAsync([FromQuery] StartSubscriptionDto dto)
    {
        var tenant = await this.dbContext.Tenant.SingleAsync();
        if (!this.subscriptionService.TenantAllowedToActivatePaidPlan(tenant))
        {
            this.logger.LogWarning("Not allowed to start paid plan from {Plan}", tenant.Plan.Type);
            return BadRequest(new BasicErrorResponse("Cannot start paid plan from current plan"));
        }

        var planMetadata = await this.dbContext.PaidPlanMetadata
            .SingleOrDefaultAsync(e => e.Type == dto.PlanType);
        if (planMetadata == null)
        {
            this.logger.LogWarning("Provided plan {Plan} is not paid", dto.PlanType);
            return BadRequest(new BasicErrorResponse("Cannot checkout with unpaid plan"));
        }

        var customer = await this.subscriptionService.GetOrCreateCustomerAsync(tenant);
        if (customer == null)
        {
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        if (tenant.Plan.IsNotForProfit)
        {
            await this.paymentProcessor.ApplyNotForProfitDiscountAsync(customer);
        }

        var result = await this.paymentProcessor.CreateCheckoutSessionAsync(new CheckoutSessionOptions
        {
            CustomerId = customer.Id,
            Plan = planMetadata,
            PromoCode = dto.PromoCode,
            ReturnUrl = dto.ReturnUrl,
        });
        return ResultToHttp(result, session => session.Url);
    }

    private ActionResult<THttp> ResultToHttp<TData, THttp>(Result<TData> result, Func<TData, THttp> mapToResponse)
    {
        return result switch
        {
            SucceededResult<TData> successResult => mapToResponse(successResult.Data),
            UserErrorResult<TData> userError => BadRequest(new BasicErrorResponse(userError)),
            ServerErrorResult<TData> serverError => StatusCode(
                StatusCodes.Status500InternalServerError, new BasicErrorResponse(serverError)),
            _ => StatusCode(StatusCodes.Status500InternalServerError),

        };
    }
}

public class StartSubscriptionDto
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public required PlanType PlanType { get; set; }

    public string? PromoCode { get; set; }

    public required Uri ReturnUrl { get; set; }
}

public class SubscriptionDetailsDto
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public required PlanType CurrentPlan { get; set; }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public PlanType? NextPlan { get; set; }

    public required bool IsAllowedToActivatePaidPlan { get; set; }

    public required DateTime? AccessEndsDate { get; set; }

    public required decimal MonthlyFeeDollars { get; set; }

    public required bool IsNotForProfit { get; set; }

    public required int? MaximumMemberCount { get; set; }

    public required DateTime DateRegistered { get; set; }

    public required bool CanAccessCustomerPortal { get; set; }

    public ContactDetailsDto? ContactDetails { get; set; }

    public PaymentMethodDto? PaymentMethod { get; set; }

    public UpcomingInvoiceDto? UpcomingInvoice { get; set; }

    public required IEnumerable<PreviousInvoiceDto> PreviousInvoices { get; set; }
}

public class ContactDetailsDto
{
    public required string Name { get; set; }

    public required string Email { get; set; }

    public string? Address { get; set; }
}

public class PaymentMethodDto
{
    public required string Type { get; set; }

    public required string Preview { get; set; }
}

public class UpcomingInvoiceDto
{
    public required DateTime BillingDate { get; set; }

    public required string Description { get; set; }

    public required DateTime StartDate { get; set; }

    public required DateTime EndDate { get; set; }

    public required decimal TotalDollars { get; set; }
}

public class PreviousInvoiceDto
{
    public required string Description { get; set; }

    public required DateTime StartDate { get; set; }

    public required DateTime EndDate { get; set; }

    public required decimal TotalDollars { get; set; }

    public Uri? Url { get; set; }
}

public class PaidPlanDto
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public required PlanType PlanType { get; set; }

    public required decimal MonthlyFeeDollars { get; set; }

    public required decimal NfpMonthlyFeeDollars { get; set; }

    public required int? MaxMemberCount { get; set; }
}
