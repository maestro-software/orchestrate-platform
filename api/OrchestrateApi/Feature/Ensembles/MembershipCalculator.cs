using OrchestrateApi.DAL;

namespace OrchestrateApi.Feature.Ensembles;

public class MembershipCalculator
{
    private readonly DateTime from;
    private readonly DateTime to;

    public MembershipCalculator(DateTime from, DateTime to)
    {
        this.from = from;
        this.to = to;
    }

    public int JoinedCount(IEnumerable<IMembership> membership) => membership.Count(
        m => this.from <= m.DateJoined && m.DateJoined < this.to && (m.DateLeft == null || m.DateLeft >= this.to));

    public int StayedCount(IEnumerable<IMembership> membership) => membership.Count(
        m => m.DateJoined < this.from && (m.DateLeft == null || m.DateLeft >= this.to));

    public int LeftCount(IEnumerable<IMembership> membership) => membership.Count(
        m => m.DateJoined < this.from && m.DateLeft.HasValue && this.from <= m.DateLeft.Value && m.DateLeft.Value < this.to);

    public int JoinedAndLeftCount(IEnumerable<IMembership> membership) => membership.Count(
        m => this.from <= m.DateJoined && m.DateJoined < this.to && m.DateLeft.HasValue && this.from <= m.DateLeft.Value && m.DateLeft.Value < this.to);
}
