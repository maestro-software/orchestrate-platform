using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;

namespace OrchestrateApi.Feature.Ensembles;

[ApiController]
[Route("disband-ensemble-dialog")]
[HasTenantPermissionAuthorize(FeaturePermission.EnsembleWrite)]
[ValidationExceptionFilter]
public class DisbandEnsembleDialogController(OrchestrateContext dbContext) : ControllerBase
{
    [HttpGet("{ensembleId}")]
    public async Task<ActionResult<DisbandImpactDto>> GetImpact(int ensembleId)
    {
        if (await dbContext.Ensemble.SingleOrDefaultAsync(e => e.Id == ensembleId) is not { } ensemble)
        {
            return NotFound();
        }

        if (ensemble.Status == EnsembleStatus.Disbanded)
        {
            return BadRequest();
        }

        return new DisbandImpactDto
        {
            MembershipEndedMemberNames = await dbContext.EnsembleMembership
                .Where(e => e.EnsembleId == ensembleId)
                .WhereIsActiveNow()
                .Select(e => string.Join(" ", new[] { e.Member!.FirstName, e.Member!.LastName }))
                .OrderBy(e => e)
                .ToListAsync(),
            RemovedFromMailingLists = await dbContext.MailingList
                .Where(e => e.Ensembles.Select(e => e.Id).Contains(ensembleId))
                .Select(e => e.Name)
                .OrderBy(e => e)
                .ToListAsync(),
        };
    }

    [HttpPost("{ensembleId}")]
    public async Task<IActionResult> DisbandEnsemble(int ensembleId)
    {
        if (await dbContext.Ensemble.SingleOrDefaultAsync(e => e.Id == ensembleId) is not { } ensemble)
        {
            return NotFound();
        }

        if (ensemble.Status == EnsembleStatus.Disbanded)
        {
            return Ok();
        }

        var currentMemberships = await dbContext.EnsembleMembership
            .Where(e => e.EnsembleId == ensembleId)
            .WhereIsActiveNow()
            .ToListAsync();
        foreach (var membership in currentMemberships)
        {
            membership.DateLeft = DateTime.UtcNow;
        }

        var mailingLists = await dbContext.MailingList
            .Where(e => e.Ensembles.Select(e => e.Id).Contains(ensembleId))
            .Include(e => e.Ensembles.Where(e => e.Id == ensembleId))
            .ToListAsync();
        foreach (var mailingList in mailingLists)
        {
            mailingList.Ensembles.Remove(ensemble);
        }

        ensemble.Status = EnsembleStatus.Disbanded;
        await dbContext.SaveChangesAsync();

        return Ok();
    }
}

public class DisbandImpactDto
{
    public required IList<string> MembershipEndedMemberNames { get; init; }
    public required IList<string> RemovedFromMailingLists { get; init; }
}
