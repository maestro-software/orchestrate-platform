using AutoMapper.QueryableExtensions;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;
using OrchestrateApi.Validation;

namespace OrchestrateApi.Feature.Contacts;

public enum ContactCategory
{
    Current,
    Archived,
}

[ApiController]
[Route("contacts")]
[StandardTenantPermissionAuthorize(FeaturePermission.ContactsRead, FeaturePermission.ContactsWrite)]
[ValidationExceptionFilter]
public class ContactsController : ControllerBase
{
    private readonly OrchestrateContext dbContext;
    private readonly IValidator<CreateContactDto> createDtoValidator;
    private readonly IValidator<UpdateContactDto> updateDtoValidator;
    private readonly AutoMapper.IMapper mapper;

    public ContactsController(
        OrchestrateContext dbContext,
        IValidator<CreateContactDto> createDtoValidator,
        IValidator<UpdateContactDto> updateDtoValidator,
        AutoMapper.IMapper mapper)
    {
        this.dbContext = dbContext;
        this.createDtoValidator = createDtoValidator;
        this.updateDtoValidator = updateDtoValidator;
        this.mapper = mapper;
    }

    [HttpGet]
    public async Task<IList<ContactDto>> GetAllContacts([FromQuery] ContactCategory? category = null)
    {
        var contacts = category switch
        {
            ContactCategory.Current => this.dbContext.Contact.Where(e => !e.ArchivedOn.HasValue),
            ContactCategory.Archived => this.dbContext.Contact.Where(e => e.ArchivedOn.HasValue),
            _ => this.dbContext.Contact,
        };

        return await contacts
            .ProjectTo<ContactDto>(this.mapper.ConfigurationProvider)
            .ToListAsync();
    }

    [HttpPost]
    public async Task<ContactDto> CreateContact(CreateContactDto dto)
    {
        this.createDtoValidator.ValidateAndThrow(dto.SanitiseAndReturn());

        var contact = this.mapper.Map<Contact>(dto);
        this.dbContext.Add(contact);
        await this.dbContext.SaveChangesAsync();
        return this.mapper.Map<ContactDto>(contact);
    }

    [HttpPatch("{id}")]
    public async Task<ActionResult<ContactDto>> UpdateContact(int id, UpdateContactDto dto)
    {
        var contact = await this.dbContext.Contact.FindAsync(id);
        if (contact == null)
        {
            return NotFound();
        }

        this.updateDtoValidator.ValidateAndThrow(dto.SanitiseAndReturn());

        this.mapper.Map(dto, contact);
        await this.dbContext.SaveChangesAsync();
        return this.mapper.Map<ContactDto>(contact);
    }

    [HttpPost("{id}/archive")]
    public async Task<ActionResult<ContactDto>> ArchiveContact(int id)
    {
        var contact = await this.dbContext.Contact.FindAsync(id);
        if (contact == null)
        {
            return NotFound();
        }

        if (!contact.ArchivedOn.HasValue)
        {
            contact.ArchivedOn = DateTime.UtcNow;
            await this.dbContext.SaveChangesAsync();
        }

        return this.mapper.Map<ContactDto>(contact);
    }

    [HttpPost("{id}/unarchive")]
    public async Task<ActionResult<ContactDto>> UnarchiveContact(int id)
    {
        var contact = await this.dbContext.Contact.FindAsync(id);
        if (contact == null)
        {
            return NotFound();
        }

        if (contact.ArchivedOn.HasValue)
        {
            contact.ArchivedOn = null;
            await this.dbContext.SaveChangesAsync();
        }

        return this.mapper.Map<ContactDto>(contact);
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteContact(int id)
    {
        var nrows = await this.dbContext.Contact
            .Where(e => e.Id == id)
            .ExecuteDeleteAsync();
        return nrows > 0
            ? Ok()
            : NotFound();
    }
}

public class ContactsAutoMapperProfile : AutoMapper.Profile
{
    public ContactsAutoMapperProfile()
    {
        CreateMap<Contact, ContactDto>();
        CreateMap<CreateContactDto, Contact>(AutoMapper.MemberList.Source);
        CreateMap<UpdateContactDto, Contact>(AutoMapper.MemberList.Source);
    }
}

public class ContactDto
{
    public int Id { get; set; }

    public required string Name { get; set; }

    public string? Email { get; set; }

    public string? PhoneNo { get; set; }

    public string? Affiliation { get; set; }

    public string? Notes { get; set; }

    public DateTime? ArchivedOn { get; set; }
}

public class CreateContactDto : ISanitisable
{
    public required string Name { get; set; }

    public string? Email { get; set; }

    public string? PhoneNo { get; set; }

    public string? Affiliation { get; set; }

    public string? Notes { get; set; }

    public void Sanitise()
    {
        Name = Sanitiser.StringToEmpty(Name);
        Email = Sanitiser.StringToNull(Email);
        PhoneNo = Sanitiser.StringToNull(PhoneNo);
        Affiliation = Sanitiser.StringToNull(Affiliation);
        Notes = Sanitiser.StringToNull(Notes);
    }
}

public class CreateContactDtoValidator : AbstractValidator<CreateContactDto>
{
    public CreateContactDtoValidator()
    {
        RuleFor(e => e.Name).NotEmpty();
        RuleFor(e => e.Email).EmailAddress();
    }
}

public class UpdateContactDto : ISanitisable
{
    public Optional<string> Name { get; set; }

    public Optional<string?> Email { get; set; }

    public Optional<string?> PhoneNo { get; set; }

    public Optional<string?> Affiliation { get; set; }

    public Optional<string?> Notes { get; set; }

    public void Sanitise()
    {
        Name = Sanitiser.StringToEmpty(Name);
        Email = Sanitiser.StringToNull(Email);
        PhoneNo = Sanitiser.StringToNull(PhoneNo);
        Affiliation = Sanitiser.StringToNull(Affiliation);
        Notes = Sanitiser.StringToNull(Notes);
    }
}

public class UpdateContactDtoValidator : AbstractValidator<UpdateContactDto>
{
    public UpdateContactDtoValidator()
    {
        RuleFor(e => e.Name).Optional().NotEmpty();
        RuleFor(e => e.Email).Optional().EmailAddress();
    }
}
