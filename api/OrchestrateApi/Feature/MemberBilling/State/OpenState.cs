using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Feature.MemberBilling.State
{
    public class OpenState : BillingPeriodState
    {
        public OpenState(BillingPeriodStateContext stateContext, ILogger<OpenState> logger)
            : base(stateContext, logger)
        {
        }

        public override MemberBillingPeriodState State => MemberBillingPeriodState.Open;

        public override async Task PerformTemporalTasks()
        {
            var notificationInterval = AppConfig.BillingPeriodUpcomingActionNotificationInterval;
            var actions = new List<NotificationAction>();
            if (BillingPeriod.EarlyBirdDue is DateTime earlyBirdDue)
            {
                actions.Add(new NotificationAction<BillingPeriodEarlyBirdEndingSoonNotificationEmail>
                {
                    After = earlyBirdDue.Subtract(notificationInterval),
                    State = this,
                });
                actions.Add(new NotificationAction<BillingPeriodEarlyBirdEndedNotificationEmail>
                {
                    After = earlyBirdDue,
                    State = this,
                    SendInvoices = true,
                    DoSendInvoiceLog = () => Logger.LogInformation("Checking to see if early bird reminders have been sent out"),
                });
            }

            actions.Add(new NotificationAction<BillingPeriodDueSoonNotificationEmail>
            {
                After = BillingPeriod.Due.Subtract(notificationInterval),
                State = this,
            });
            actions.Add(new NotificationAction<BillingPeriodOverdueNotificationEmail>
            {
                After = BillingPeriod.Due,
                State = this,
                SendInvoices = true,
                DoSendInvoiceLog = () => Logger.LogInformation("Checking to see if overdue reminders have been sent out"),
            });

            var now = DateTime.UtcNow;
            for (var i = 0; i < actions.Count; i++)
            {
                var rangeStart = actions[i].After;
                var rangeEnd = i + 1 < actions.Count
                    ? actions[i + 1].After
                    : DateTime.MaxValue;

                if (rangeStart >= rangeEnd)
                {
                    Logger.LogError("Unexpected state in BillingPeriod={BillingPeriodId}. {Start} > {End}", BillingPeriod.Id, rangeStart, rangeEnd);
                    return;
                }

                if (rangeStart <= now && now < rangeEnd)
                {
                    await actions[i].DoAction();
                    await DbContext.SaveChangesAsync();
                    // Shouldn't be any more work to do (no overlapping regions), so exit early
                    return;
                }
            }
        }

        public override async Task<StateTransitionResult> CloseBillingPeriod()
        {
            if (BillingPeriod.Due > DateTime.UtcNow && await HasOpenInvoices())
            {
                return StateTransitionResult.NotAllowed;
            }

            BillingPeriod.Closed = DateTime.UtcNow;
            StateContext.SetState(MemberBillingPeriodState.Closed);
            await DbContext.SaveChangesAsync();
            return StateTransitionResult.Successful;
        }

        private Task<bool> HasOpenInvoices()
        {
            return DbContext.MemberInvoice
                .Where(e => e.MemberBillingPeriodId == BillingPeriod.Id)
                .AnyAsync(e => e.Paid == null);
        }

        private abstract class NotificationAction
        {
            public DateTime After { get; set; }
            public required OpenState State { get; set; }
            public bool SendInvoices { get; set; }
            public Action? DoSendInvoiceLog { get; set; }

            protected MemberBillingPeriod BillingPeriod => State.BillingPeriod;
            protected ILogger Logger => State.Logger;


            public async Task DoAction()
            {
                if (SendInvoices)
                {
                    DoSendInvoiceLog?.Invoke();
                    await SendUnpaidInvoiceReminders();
                }

                await EmailNotificationContactIfNecessary();
            }

            private async Task SendUnpaidInvoiceReminders()
            {
                var unpaidInvoices = await State.DbContext.MemberInvoice
                    .Where(e => e.MemberBillingPeriodId == BillingPeriod.Id)
                    // Don't select invoices that haven't been sent. Invoices in this state have been
                    // manually generated and the user has deliberately chosen not to send the invoice
                    .Where(e => !e.Paid.HasValue && e.Sent!.Value < After)
                    .Include(e => e.Member!.Details)
                    .Include(e => e.LineItems)
                    .ToListAsync();
                if (unpaidInvoices.Count > 0)
                {
                    Logger.LogInformation("Sending {Count} invoice reminders to members", unpaidInvoices.Count);
                    await State.MemberInvoiceSender.SendAsync(State.Tenant, State.BillingConfig, unpaidInvoices);
                }
                else
                {
                    Logger.LogInformation($"All invoices paid or already sent reminder, not sending any reminder emails");
                }
            }

            protected abstract Task EmailNotificationContactIfNecessary();
        }

        private class NotificationAction<T> : NotificationAction
            where T : BillingPeriodNotificationEmail, new()
        {
            protected override async Task EmailNotificationContactIfNecessary()
            {
                if (!BillingPeriod.NotificationContactNotified.HasValue
                    || BillingPeriod.NotificationContactNotified.Value < After)
                {
                    var email = BillingPeriodNotificationEmail.Create<T>(
                        State.AppConfig, State.Tenant, State.BillingConfig, State.BillingPeriod);
                    Logger.LogInformation("Emailing Notification contact: {EmailType}", email.GetType().Name);

                    await State.EmailService.SendAsync(email);
                    BillingPeriod.NotificationContactNotified = DateTime.UtcNow;
                }
                else
                {
                    Logger.LogInformation($"Notification contact has already been emailed");
                }
            }
        }
    }
}
