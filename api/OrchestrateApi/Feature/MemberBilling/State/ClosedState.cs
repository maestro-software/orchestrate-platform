using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Feature.MemberBilling.State
{
    public class ClosedState : BillingPeriodState
    {
        public ClosedState(BillingPeriodStateContext stateContext, ILogger<ClosedState> logger)
            : base(stateContext, logger)
        {
        }

        public override MemberBillingPeriodState State => MemberBillingPeriodState.Closed;
    }
}
