using OrchestrateApi.Common;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Feature.MemberBilling
{
    public interface IMemberInvoiceRenderingService
    {
        /// <summary>
        /// Renders member invoice as PDF as a byte array. Caller is responsible for ensuring
        /// memberInvoice has BillingPeriod, Member and LineItems primed
        /// </summary>
        byte[] RenderAsPdf(Tenant tenant, MemberBillingConfig billingConfig, MemberInvoice memberInvoice);

        /// <summary>
        /// Renders member invoice as PDF to the given stream. Caller is responsible for ensuring
        /// memberInvoice has BillingPeriod, Member and LineItems primed
        /// </summary>
        void RenderAsPdf(Tenant tenant, MemberBillingConfig billingConfig, MemberInvoice memberInvoice, Stream stream);
    }

    public class MemberInvoiceRenderingService : IMemberInvoiceRenderingService
    {
        private readonly IHostEnvironment environment;
        private readonly IInvoiceRenderer invoiceRenderer;

        public MemberInvoiceRenderingService(IHostEnvironment environment, IInvoiceRenderer invoiceRenderer)
        {
            this.environment = environment;
            this.invoiceRenderer = invoiceRenderer;
        }

        public byte[] RenderAsPdf(Tenant tenant, MemberBillingConfig billingConfig, MemberInvoice memberInvoice)
        {
            using var memoryStream = new MemoryStream();
            RenderAsPdf(tenant, billingConfig, memberInvoice, memoryStream);
            return memoryStream.ToArray();
        }

        public void RenderAsPdf(Tenant tenant, MemberBillingConfig billingConfig, MemberInvoice memberInvoice, Stream stream)
        {
            var invoice = ConvertToInvoice(tenant, billingConfig, memberInvoice);
            this.invoiceRenderer.RenderToStream(invoice, stream);
        }

        public Invoice ConvertToInvoice(Tenant tenant, MemberBillingConfig billingConfig, MemberInvoice memberInvoice)
        {
            var billingPeriod = memberInvoice.MemberBillingPeriod ?? throw new InvalidOperationException("MemberBillingPeriod should be primed");
            var member = memberInvoice.Member ?? throw new InvalidOperationException("Member should be primed");

            var invoice = new Invoice
            {
                Title = billingPeriod.Name,
                Logo = tenant.Logo ?? GroupLogoPlaceholder(),
                Reference = memberInvoice.Reference,
                Due = billingPeriod.Due.InTimeZone(tenant.TimeZone),
                Paid = memberInvoice.Paid,
                // TODO Add extension method for this
                InvoicedTo = $"{member.FirstName} {member.LastName}",
                ContactInformation = new()
                {
                    OrganisationName = tenant.Name,
                    Website = tenant.Website,
                },
                RemittanceAdvice = new()
                {
                    BankName = billingConfig.BankName,
                    BankAccountName = billingConfig.BankAccountName,
                    BankBsb = billingConfig.BankBsb,
                    BankAccountNo = billingConfig.BankAccountNo,
                    ContactEmail = billingConfig.NotificationContactEmail,
                    Remarks = billingConfig.PaymentInstructions,
                },
            };

            if (!environment.IsProduction())
            {
                invoice.Title = $"*** TEST *** {invoice.Title}";
                invoice.Subtitle = "This invoice has been generated in a test environment. "
                    + "It does not need to be paid.";
            }

            foreach (var lineItem in memberInvoice.LineItems.OrderBy(e => e.Ordinal))
            {
                invoice.AddLineItem(new InvoiceLineItem
                {
                    Description = lineItem.Description,
                    AmountCents = lineItem.AmountCents,
                });
            }

            if (!memberInvoice.Paid.HasValue && DateTime.UtcNow < billingPeriod.EarlyBirdDue)
            {
                var earlyBirdDueInTargetTimezone = billingPeriod.EarlyBirdDue.Value.InTimeZone(tenant.TimeZone);
                invoice.AddLineItem(new InvoiceLineItem
                {
                    Description = $"Early Bird Discount (if paid by {earlyBirdDueInTargetTimezone.ToDateString()})",
                    AmountCents = -1 * billingConfig.EarlyBirdDiscountDollars * 100,
                });
            }

            return invoice;
        }

        private static byte[] GroupLogoPlaceholder()
        {
            var logoFile = new EmbeddedFile(typeof(IInvoiceRenderer), "Resources.GroupLogoPlaceholder.png");
            return logoFile.AsByteArray();
        }
    }
}
