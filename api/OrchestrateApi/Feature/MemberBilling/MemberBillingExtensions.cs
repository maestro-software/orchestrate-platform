using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;

namespace OrchestrateApi.Feature.MemberBilling
{
    public static class MemberBillingExtensions
    {
        public static MemberInvoiceSendStatus ToSendStatus(this EmailResultStatus emailResult)
        {
            return emailResult switch
            {
                EmailResultStatus.Success => MemberInvoiceSendStatus.Pending,
                _ => MemberInvoiceSendStatus.Failed,
            };
        }

        public static MemberInvoiceSendStatus ToSendStatus(this EmailEventStatus emailEvent)
        {
            return emailEvent switch
            {
                EmailEventStatus.Delivered => MemberInvoiceSendStatus.Delivered,
                EmailEventStatus.Opened => MemberInvoiceSendStatus.Opened,
                _ => MemberInvoiceSendStatus.Failed,
            };
        }
    }
}
