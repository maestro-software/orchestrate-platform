using System.Linq.Expressions;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Feature.MemberBilling;

public static class MemberBillingExpressions
{
    public static Expression<Func<MemberBillingPeriod, decimal>> BillingPeriodDollarsReceivedExpr { get; }
        = e => e.Invoices
            .Where(e => e.Paid.HasValue)
            .Sum(e => e.LineItems.Sum(e => e.AmountCents)) / 100M;

    public static Expression<Func<MemberBillingPeriod, int>> PaidEarlyCountExpr { get; }
        = e => e.Invoices.Count(i => i.Paid < e.EarlyBirdDue);

    public static Expression<Func<MemberBillingPeriod, int>> PaidOnTimeCountExpr { get; }
        = e => e.Invoices.Count(i => (!e.EarlyBirdDue.HasValue || e.EarlyBirdDue <= i.Paid) && i.Paid < e.Due);

    public static Expression<Func<MemberBillingPeriod, int>> PaidLateCountExpr { get; }
        = e => e.Invoices.Count(i => i.Paid >= e.Due);

    public static Expression<Func<MemberBillingPeriod, int>> UnpaidCountExpr { get; }
        = e => e.Invoices.Count(i => !i.Paid.HasValue);

    public static Expression<Func<MemberInvoice, decimal>> InvoiceTotalExpr { get; }
         = e => e.LineItems.Sum(e => e.AmountCents) / 100M;
}
