using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.Security;

namespace OrchestrateApi.Feature.Scores;

[ApiController]
[HasTenantPermissionAuthorize(FeaturePermission.ScoreRead)]
[Route("music-library")]
public class ScoreController : ControllerBase
{
    private readonly OrchestrateContext dbContext;

    public ScoreController(OrchestrateContext dbContext)
    {
        this.dbContext = dbContext;
    }

    [HttpGet("score-concerts/{scoreId}")]
    public async Task<IList<ScoreConcertDto>> GetScoreConcerts(int scoreId)
    {
        var dtos = await this.dbContext.Performance
            .Where(e => e.PerformanceScores.Any(ps => ps.ScoreId == scoreId))
            .GroupBy(e => e.Concert)
            .Select(e => new ScoreConcertDto
            {
                ConcertId = e.Key!.Id,
                Occasion = e.Key.Occasion,
                Date = e.Key.Date,
                Performances = e.Select(p => new ScorePerformanceDto
                {
                    EnsembleName = p.Ensemble!.Name,
                })
            })
            .OrderByDescending(e => e.Date)
            .ToListAsync();
        return dtos;

    }
}

public class ScoreConcertDto
{
    public required int ConcertId { get; set; }

    public required string Occasion { get; set; }

    public required DateTime Date { get; set; }

    public required IEnumerable<ScorePerformanceDto> Performances { get; set; }
}

public class ScorePerformanceDto
{
    public required string EnsembleName { get; set; }
}
