using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;
using OrchestrateApi.Storage;

namespace OrchestrateApi.Feature.Scores;

public class ScoreAttachmentStorageStrategy : EntityAttachmentStorageStrategy
{
    public override string Name => "ScoreAttachment";

    public override FeaturePermission SecuredReadPermission => FeaturePermission.ScoreRead;

    public override FeaturePermission WriteAndSensitiveReadPermission => FeaturePermission.ScoreWrite;

    public override object CreateLinkingEntity(Guid blobId, EntityAttachmentLinkData linkData) => new ScoreAttachment
    {
        BlobId = blobId,
        ScoreId = linkData.EntityId,
    };

    public override IQueryable<Guid> LinkedBlobIds(OrchestrateContext dbContext, EntityAttachmentLinkData linkData)
    {
        return dbContext.Score
            .Where(e => e.Id == linkData.EntityId)
            .SelectMany(e => e.Attachments.Select(a => a.BlobId));
    }

    public override IQueryable<Guid> ValidBlobIds(OrchestrateContext dbContext)
    {
        return dbContext.Score.SelectMany(e => e.Attachments.Select(a => a.BlobId));
    }
}
