using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using OrchestrateApi.DAL;

namespace OrchestrateApi.Security;

public sealed class StandardTenantPermissionAuthorizeAttribute : AuthorizeAttribute
{
    public StandardTenantPermissionAuthorizeAttribute(
        FeaturePermission readPermission,
        FeaturePermission writePermission)
    {
        Policy = StandardTenantPermissionPolicy.Encode(readPermission, writePermission);
        ReadPermission = readPermission;
        WritePermission = writePermission;
    }

    public FeaturePermission ReadPermission { get; }
    public FeaturePermission WritePermission { get; }
}

public record class ReadWritePermissions(FeaturePermission ReadPermission, FeaturePermission WritePermission);

public static class StandardTenantPermissionPolicy
{
    internal const string PolicyPrefix = "StandardTenantPermissionAuthorize";
    internal const string PolicySeperator = "__";

    public static string Encode(FeaturePermission readPermission, FeaturePermission writePermission)
    {
        return $"{PolicyPrefix}{PolicySeperator}{readPermission}{PolicySeperator}{writePermission}";
    }

    public static ReadWritePermissions? Decode(string policy)
    {
        if (!policy.StartsWith(PolicyPrefix, StringComparison.InvariantCulture))
        {
            return null;
        }

        var parts = policy.Split(PolicySeperator);
        if (parts.Length != 3)
        {
            return null;
        }

        if (Enum.TryParse<FeaturePermission>(parts[1], out var readPermission)
            && Enum.TryParse<FeaturePermission>(parts[2], out var writePermission))
        {
            return new ReadWritePermissions(readPermission, writePermission);
        }
        else
        {
            return null;
        }
    }
}

public class StandardTenantPermissionRequirement : IAuthorizationRequirement
{
    public FeaturePermission ReadPermission { get; init; }
    public FeaturePermission WritePermission { get; init; }

    public static StandardTenantPermissionRequirement? Parse(string policy)
    {
        if (StandardTenantPermissionPolicy.Decode(policy) is not { } readWritePermissions)
        {
            return null;
        }

        return new StandardTenantPermissionRequirement
        {
            ReadPermission = readWritePermissions.ReadPermission,
            WritePermission = readWritePermissions.WritePermission,
        };
    }
}

public class StandardTenantPermissionAuthorizationHandler : AuthorizationHandler<StandardTenantPermissionRequirement>
{
    private readonly IHttpContextAccessor contextAccessor;

    public StandardTenantPermissionAuthorizationHandler(IHttpContextAccessor contextAccessor)
    {
        this.contextAccessor = contextAccessor;
    }

    protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, StandardTenantPermissionRequirement requirement)
    {
        if (this.contextAccessor.HttpContext is { } httpContext
            && httpContext.TryGetTenantId(out var tenantId))
        {
            var permission = httpContext.Request.Method == "GET"
                ? requirement.ReadPermission
                : requirement.WritePermission;
            if (context.User.HasPermissionInTenant(tenantId, permission))
            {
                context.Succeed(requirement);
            }
        }

        return Task.CompletedTask;
    }
}
