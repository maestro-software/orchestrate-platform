using OrchestrateApi.Common;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Security
{
    public class TenantDto
    {
        public required Guid TenantId { get; set; }

        public required string Name { get; set; }

        public required string Abbreviation { get; set; }

        public string? Logo { get; set; }
    }

    public static class TenantDtoExtensions
    {
        public static IQueryable<TenantDto> SelectTenantDto(this IQueryable<Tenant> tenants)
        {
            return tenants.Select(t => new TenantDto
            {
                TenantId = t.Id,
                Name = t.Name,
                Abbreviation = t.Abbreviation,
                Logo = t.Logo != null
                    ? t.Logo.ToDataUrl()
                    : null,
            });
        }
    }
}
