namespace OrchestrateApi.Security
{
    public class UserDto
    {
        public required Guid Id { get; set; }

        public required string Username { get; set; }

        public string? DisplayName { get; set; }

        public bool IsGlobalAdmin { get; set; }

        public required IEnumerable<TenantRolesDto> TenantRoles { get; set; }
    }

    public class TenantRolesDto : TenantDto
    {
        public required IEnumerable<RoleDto> Roles { get; set; }
    }
}
