using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using OrchestrateApi.DAL;

namespace OrchestrateApi.Security;

public sealed class HasTenantPermissionAuthorizeAttribute : AuthorizeAttribute
{

    public HasTenantPermissionAuthorizeAttribute(FeaturePermission permission)
    {
        Policy = HasTenantPermissionPolicy.Encode(permission);
        Permission = permission;
    }

    public FeaturePermission Permission { get; }
}

public static class HasTenantPermissionPolicy
{
    internal const string PolicyPrefix = "TenantPermissionAuthorize__";

    public static string Encode(FeaturePermission permission)
    {
        return $"{PolicyPrefix}{permission}";
    }

    public static FeaturePermission? Decode(string policy)
    {
        if (!policy.StartsWith(PolicyPrefix, StringComparison.InvariantCulture))
        {
            return null;
        }

        var permissionString = policy.Replace(PolicyPrefix, string.Empty);
        return Enum.TryParse<FeaturePermission>(permissionString, out var permission)
            ? permission
            : null;
    }
}

public class HasTenantPermissionRequirement : IAuthorizationRequirement
{
    public FeaturePermission Permission { get; init; }

    public static HasTenantPermissionRequirement? Parse(string policy)
    {
        if (HasTenantPermissionPolicy.Decode(policy) is not { } permission)
        {
            return null;
        }

        return new HasTenantPermissionRequirement
        {
            Permission = permission
        };
    }
}

public class HasTenantPermissionAuthorizationHandler : AuthorizationHandler<HasTenantPermissionRequirement>
{
    private readonly IHttpContextAccessor contextAccessor;

    public HasTenantPermissionAuthorizationHandler(IHttpContextAccessor contextAccessor)
    {
        this.contextAccessor = contextAccessor;
    }

    protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, HasTenantPermissionRequirement requirement)
    {
        if (this.contextAccessor.HttpContext is { } httpContext
            && httpContext.TryGetTenantId(out var tenantId)
            && context.User.HasPermissionInTenant(tenantId, requirement.Permission))
        {
            context.Succeed(requirement);
        }

        return Task.CompletedTask;
    }
}
