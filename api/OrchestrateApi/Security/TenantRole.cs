namespace OrchestrateApi.Security
{
    public class TenantRole
    {
        public TenantRole(Guid tenantId, string role)
        {
            TenantId = tenantId;
            Role = role;
        }

        public Guid TenantId { get; }

        public string Role { get; }

        public static TenantRole Parse(string input)
        {
            var parts = input.Split("::");
            if (parts.Length != 2) { throw new FormatException("Expected <Guid>::<Role> format"); }
            if (string.IsNullOrWhiteSpace(parts[1])) { throw new FormatException("Role must be populated"); }

            return new TenantRole(new Guid(parts[0]), parts[1]);
        }

        public string Serialise()
        {
            return $"{TenantId}::{Role}";
        }
    }
}
