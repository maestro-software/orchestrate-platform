namespace OrchestrateApi.Security;

// Keep in sync with feature-permission.ts
public enum FeaturePermission
{
    // Placeholder so default(FeaturePermission) doesn't give any permissions
    None,

    PlatformAccess,
    PlatformSecurityAccess,

    SettingsWrite,

    AssetRead,
    AssetWrite,

    ScoreRead,
    ScoreWrite,

    MemberRead,
    MemberWrite,
    MemberDetailRead,
    MemberDetailWrite,

    MemberBillingRead,
    MemberBillingWrite,

    EnsembleRead,
    EnsembleWrite,

    ConcertRead,
    ConcertWrite,

    ContactsRead,
    ContactsWrite,

    MailingListRead,
    MailingListWrite,
    MailingListGlobalSend,
}
