namespace OrchestrateApi.Email
{
    public interface IEmailProvider
    {
        string Name { get; }

        Task<IList<EmailResult>> SendAsync(IList<EmailMessage> emails);
    }
}
