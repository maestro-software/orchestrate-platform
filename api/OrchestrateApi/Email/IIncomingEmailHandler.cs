namespace OrchestrateApi.Email
{
    public interface IIncomingEmailHandler
    {
        /// <summary>
        /// Perform business logic associated with an incoming email
        /// </summary>
        Task HandleAsync(IncomingEmail email);
    }
}
