using System.Diagnostics.CodeAnalysis;
using Postmark.Model.MessageStreams;
using Postmark.Model.Suppressions;
using PostmarkDotNet;
using PostmarkDotNet.Model.Webhooks;

namespace OrchestrateApi.Email.Postmark;

[SuppressMessage("URI-like parameters should not be strings", "CA1054", Justification = "Can't control Postmark API")]
public interface IPostmarkAdapter
{
    Task<IEnumerable<PostmarkResponse>> SendMessagesAsync(params PostmarkMessage[] messages);
    Task<PostmarkMessageStream> CreateMessageStream(string id, MessageStreamType type, string name, string? description = null);
    Task<WebhookConfigurationListingResponse> GetWebhookConfigurationsAsync(string? messageStream = null);
    Task<WebhookConfiguration> CreateWebhookConfigurationAsync(string url, string? messageStream = null, HttpAuth? httpAuth = null, IEnumerable<HttpHeader>? httpHeaders = null, WebhookConfigurationTriggers? triggers = null);
    Task<WebhookConfiguration> EditWebhookConfigurationAsync(long configurationId, string url, HttpAuth? httpAuth = null, IEnumerable<HttpHeader>? httpHeaders = null, WebhookConfigurationTriggers? triggers = null);
    Task<PostmarkSuppressionListing> ListSuppressions(PostmarkSuppressionQuery query, string messageStream = "outbound");
    Task<PostmarkBulkReactivationResult> DeleteSuppressions(IEnumerable<PostmarkSuppressionChangeRequest> suppressionChanges, string messageStream = "outbound");
    Task<PostmarkMessageStreamArchivalConfirmation> ArchiveMessageStream(string id);
}

internal interface IPostmarkAuth
{
    string Token { get; }
}

public class PostmarkAdapter(string serverToken)
    : PostmarkClient(serverToken), IPostmarkAdapter, IPostmarkAuth
{
    public string Token { get; } = serverToken;

    public Task<PostmarkMessageStream> CreateMessageStream(string id, MessageStreamType type, string name, string? description = null)
        => CreateMessageStream(id, type, name, description, null);
}
