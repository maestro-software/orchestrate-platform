using Sentry;

namespace OrchestrateApi.Email;

public class SpamComplainEmailEventHander : IEmailEventHandler
{
    private readonly ILogger logger;

    public SpamComplainEmailEventHander(ILogger<SpamComplainEmailEventHander> logger)
    {
        this.logger = logger;
    }

    public Task<int> HandleAsync(IList<EmailEvent> events)
    {
        var spamComplaintEvents = events.OfType<SpamComplaintEmailEvent>().ToList();

        foreach (var @event in spamComplaintEvents)
        {
            this.logger.LogError("{Recipient} marked email as spam", @event.Recipient);
            SentrySdk.CaptureEvent(new SentryEvent
            {
                Level = SentryLevel.Error,
                Message = "Email marked as spam by user",
                Contexts =
                {
                    ["EmailEvent"] = @event,
                }
            });
        }

        return Task.FromResult(spamComplaintEvents.Count);
    }
}
