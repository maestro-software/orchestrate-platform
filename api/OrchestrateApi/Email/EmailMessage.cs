using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Email
{
    public class EmailMessage
    {
        public EmailMessageAddress? From { get; set; }

        public EmailMessageAddress? ReplyTo { get; set; }

        public EmailMessageAddress? To { get; set; }

        public string Subject { get; set; } = string.Empty;

        public string? TextContent { get; set; }

        public string? HtmlContent { get; set; }

        public IList<EmailAttachment> Attachments { get; init; } = new List<EmailAttachment>();

        public object? CustomData { get; set; }

        /// <summary>
        /// Uniquely identifies the type of this email, to be used when handling Webhook events
        ///</summary>
        public string Type { get; set; } = "Generic";

        /// <summary>Data which will be able to retreived on a webhook event</summary>
        public IDictionary<string, string> Metadata { get; } = new Dictionary<string, string>();

        /// <summary>Custom headers to send with each sent message</summary>
        public IDictionary<string, string> Headers { get; init; } = new Dictionary<string, string>();

        public EmailMessageSendContext? SendContext { get; init; }
    }

    public record class EmailMessageAddress
    {
        public EmailMessageAddress(string address) { Address = address; }

        public EmailMessageAddress(string? name, string address) { Name = name; Address = address; }

        public string? Name { get; init; }

        public string Address { get; init; }

        public override string ToString()
        {
            var name = string.IsNullOrWhiteSpace(Name)
                ? string.Empty
                : $"{Name} ";
            return $"{name}<{Address}>";
        }
    }

    public class EmailAttachment
    {
        public required string MimeType { get; init; }

        public required string Name { get; init; }

        public required string Base64Content { get; init; }
    }

    public class EmailMessageSendContext
    {
        public required Tenant Tenant { get; set; }
        public MailingList? MailingList { get; set; }
    }
}
