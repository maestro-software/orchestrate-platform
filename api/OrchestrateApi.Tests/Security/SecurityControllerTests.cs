using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;

namespace OrchestrateApi.Tests.Security
{
    public class SecurityControllerTests : FullIntegrationTestHarness, IAsyncLifetime
    {
        private const string TestEmail = "test@email.com";
        private const string TestUserName = "test_username";
        private const string TestPassword = "S0meLongPassword!";

        private readonly SecurityController controller;

        public SecurityControllerTests()
        {
            this.controller = new SecurityController(
                UserManager,
                GetService<UserService>(),
                GetService<OrchestrateContext>());
        }

        private UserManager<OrchestrateUser> UserManager => GetService<UserManager<OrchestrateUser>>();

        public async Task InitializeAsync()
        {
            var user = new OrchestrateUser
            {
                UserName = TestUserName,
                Email = TestEmail,
            };
            await UserManager.CreateAsync(user, TestPassword);
        }

        public Task DisposeAsync() => Task.CompletedTask;

        [Theory]
        [InlineData(TestUserName, TestPassword, true)]
        [InlineData(" " + TestUserName, TestPassword, true)]
        [InlineData(TestUserName + " ", TestPassword, true)]
        [InlineData(TestUserName, " " + TestPassword, false)]
        [InlineData(TestUserName, TestPassword + " ", false)]
        [InlineData(TestEmail, TestPassword, false)]
        public async Task LoginAllowsAndDeniesCorrectly(string username, string password, bool isAllowed)
        {
            var result = await this.controller.Login(new SecurityController.LoginModel
            {
                UserName = username,
                Password = password,
            });

            if (isAllowed)
            {
                Assert.Equal(TestUserName, result.Value!.User.Username);
            }
            else
            {
                Assert.IsType<UnauthorizedResult>(result.Result);
            }
        }

        [Fact]
        public async Task UnauthorisedIfNoUser()
        {
            var result = await this.controller.GetLoggedInUser();
            Assert.IsType<UnauthorizedResult>(result.Result);

            this.controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = new ClaimsPrincipal(),
                }
            };
            result = await this.controller.GetLoggedInUser();
            Assert.IsType<UnauthorizedResult>(result.Result);
        }

        [Theory]
        [InlineData(TestEmail, true)]
        [InlineData(" " + TestEmail, true)]
        [InlineData(TestEmail + " ", true)]
        [InlineData(TestUserName, false)]
        public async Task ResetPasswordWorksForValidValues(string email, bool isAllowed)
        {
            var user = await UserManager.FindByEmailAsync(TestEmail);
            Assert.NotNull(user);
            var token = await UserManager.GeneratePasswordResetTokenAsync(user);

            var result = await this.controller.ResetPassword(new SecurityController.ResetPasswordModel
            {
                Email = email,
                Password = "new_passw0rd",
                Token = token,
            });

            if (isAllowed)
            {
                Assert.IsType<OkResult>(result);
                Assert.False(await UserManager.CheckPasswordAsync(user, TestPassword));
                Assert.True(await UserManager.CheckPasswordAsync(user, "new_passw0rd"));
            }
            else
            {
                Assert.IsType<BadRequestResult>(result);
                Assert.True(await UserManager.CheckPasswordAsync(user, TestPassword));
                Assert.False(await UserManager.CheckPasswordAsync(user, "new_passw0rd"));
            }
        }
    }
}
