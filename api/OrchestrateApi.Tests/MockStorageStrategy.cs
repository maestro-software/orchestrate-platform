using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text.Json;
using System.Text.Json.Serialization;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Storage;

namespace OrchestrateApi.Tests;

public class MockStorageStrategy : IBlobStorageStrategy
{
    public virtual string Name => "Mock";

    public IEnumerable<Guid> StaticValidBlobIds { get; set; } = Enumerable.Empty<Guid>();

    public Expression<Func<OrchestrateBlob, bool>> ReadableBlobsExpr(ClaimsPrincipal user, Guid tenantId)
    {
        throw new NotImplementedException();
    }

    public bool TryParseAccessData(JsonDocument rawData, out object? accessData)
    {
        return StorageStrategyUtils.TryParseRequiredJsonData<MockAccessData>(rawData, out accessData);
    }

    public bool IsValidAccessData(object? accessData) => accessData is MockAccessData;

    public bool UserCanRead(ClaimsPrincipal user, Guid tenantId, object? accessData) => ((MockAccessData?)accessData)!.CanRead;

    public bool UserCanWrite(ClaimsPrincipal user, Guid tenantId, object? accessData) => ((MockAccessData?)accessData)!.CanWrite;

    public IQueryable<Guid> ValidBlobIds(OrchestrateContext dbContext) => dbContext.Blobs
        .Where(e => StaticValidBlobIds.Contains(e.Id))
        .Select(e => e.Id);
}

public record class MockAccessData
{
    public MockAccessData(bool canRead, bool canWrite)
    {
        CanRead = canRead;
        CanWrite = canWrite;
    }

    [JsonRequired]
    public bool CanRead { get; init; }

    [JsonRequired]
    public bool CanWrite { get; init; }
};

public class MockLinkedStorageStrategy : MockStorageStrategy, ILinkedStorageStrategy
{
    public override string Name => "MockLink";

    public bool TryParseLinkData(JsonDocument rawData, [NotNullWhen(true)] out object? linkData)
    {
        return StorageStrategyUtils.TryParseRequiredJsonData<MockLinkData>(rawData, out linkData);
    }

    public bool IsValidLinkData(object? linkData) => linkData is MockLinkData;

    public object CreateLinkingEntity(Guid blobId, object linkData) => null!;

    public IQueryable<Guid> LinkedBlobIds(OrchestrateContext dbContext, object linkData) => dbContext.Blobs
        .Where(e => e.Name.Contains(((MockLinkData)linkData).NameContains))
        .Select(e => e.Id);
}

public class MockLinkData
{
    public MockLinkData(string nameContains)
    {
        NameContains = nameContains;
    }

    [JsonRequired]
    public string NameContains { get; init; }
}

public class MockValidatingStorageStrategy : MockStorageStrategy, IValidatingStorageStrategy
{
    public override string Name => "MockValidating";

    public Task<BlobValidationResult> ValidateAsync(IUploadedBlob blob, CancellationToken cancellationToken)
    {
        return Task.FromResult(new BlobValidationResult
        {
            IsValid = blob.Name == "valid.txt",
            Message = "Name must be valid.txt",
        });
    }
}
