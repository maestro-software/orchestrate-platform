using static OrchestrateApi.Email.EmailHelpers;

namespace OrchestrateApi.Tests.Email;

public class EmailHelpersTests
{
    [Theory]
    [InlineData("", false)]
    [InlineData("some text", false)]
    [InlineData(FooterSentinal, true)]
    [InlineData(FooterSentinal + "some text", true)]
    [InlineData("some text" + FooterSentinal, true)]
    [InlineData("some text" + FooterSentinal + "some text", true)]
    public void ContainsFooterSentinalTests(string content, bool expectedContains)
    {
        Assert.Equal(expectedContains, ContainsFooterSentinal(content));
    }

    [Theory]
    [InlineData("text", "new", "text")]
    [InlineData(UnsubscribeLinkStartSentinal + "text", "new", UnsubscribeLinkStartSentinal + "text")]
    [InlineData(
        UnsubscribeLinkStartSentinal + UnsubscribeLinkEndSentinal,
        "new",
        UnsubscribeLinkStartSentinal + "new" + UnsubscribeLinkEndSentinal)]
    [InlineData(
        UnsubscribeLinkStartSentinal + "text" + UnsubscribeLinkEndSentinal,
        "new",
        UnsubscribeLinkStartSentinal + "new" + UnsubscribeLinkEndSentinal)]
    [InlineData(
        "prepend" + UnsubscribeLinkStartSentinal + "text" + UnsubscribeLinkEndSentinal + "postpend",
        "new",
        "prepend" + UnsubscribeLinkStartSentinal + "new" + UnsubscribeLinkEndSentinal + "postpend")]
    public void UnsubscribeLinkIsReplaced(string input, string newLink, string expectedOutput)
    {
        Assert.Equal(expectedOutput, ReplacePlainTextUnsubscribeLink(input, newLink));

        var expectedHtmlOutput = expectedOutput
            .Replace(UnsubscribeLinkStartSentinal, UnsubscribeLinkHtmlEncodedStartSentinal)
            .Replace(UnsubscribeLinkEndSentinal, UnsubscribeLinkHtmlEncodedEndSentinal);
        Assert.Equal(expectedHtmlOutput, ReplaceHtmlUnsubscribeLink(input, newLink));

        var htmlInput = input
            .Replace(UnsubscribeLinkStartSentinal, UnsubscribeLinkHtmlEncodedStartSentinal)
            .Replace(UnsubscribeLinkEndSentinal, UnsubscribeLinkHtmlEncodedEndSentinal);
        Assert.Equal(expectedHtmlOutput, ReplaceHtmlUnsubscribeLink(htmlInput, newLink));
    }
}
