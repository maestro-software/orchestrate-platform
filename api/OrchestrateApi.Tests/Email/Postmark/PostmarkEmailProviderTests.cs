using System.Diagnostics.CodeAnalysis;
using System.Text;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;
using OrchestrateApi.Email.Postmark;
using Postmark.Model.MessageStreams;
using Postmark.Model.Suppressions;
using PostmarkDotNet;
using PostmarkDotNet.Model.Webhooks;
using static OrchestrateApi.Email.Postmark.PostmarkEmailProvider;
using static OrchestrateApi.Email.EmailHelpers;

namespace OrchestrateApi.Tests.Email.Postmark;

[SuppressMessage("Nested types should not be visible", "CA1034")]
[SuppressMessage("Specify IFormatProvider", "CA1305")]
public class PostmarkEmailProviderTests
{
    private readonly PostmarkEmailProvider provider;
    private readonly MockPostmarkApiService mockApi;

    public PostmarkEmailProviderTests()
    {
        this.mockApi = new MockPostmarkApiService();
        this.provider = new(this.mockApi, TestHelper.BuildMockLogger<PostmarkEmailProvider>());
    }

    [Fact]
    public async Task EmailsWillBeGroupedByTenantAndStreamsWillBeSetAppropriately()
    {
        var tenant1 = new Tenant { Abbreviation = "tenant1" };
        var mailingList1 = new MailingList { Name = "ML1", Slug = "ml1" };
        var mailingList2 = new MailingList { Name = "ML2", Slug = "ml2" };
        var tenant2 = new Tenant { Abbreviation = "tenant2" };
        var mailingList3 = new MailingList { Name = "ML3", Slug = "ml3" };

        await this.provider.SendAsync([
            new() { Subject = "No tenant" },
            new() { Subject = "tenant1", SendContext = new() { Tenant = tenant1 } },
            new() { Subject = "tenant1;ml1", SendContext = new() { Tenant = tenant1, MailingList = mailingList1 } },
            new() { Subject = "tenant1;ml2", SendContext = new() { Tenant = tenant1, MailingList = mailingList2 } },
            new() { Subject = "tenant2;ml3", SendContext = new() { Tenant = tenant2, MailingList = mailingList3 } },
        ]);

        var globalBatch = Assert.Single(mockApi.GlobalMock.SentBatches);
        var globalMessage = Assert.Single(globalBatch);
        Assert.Equal("No tenant", globalMessage.Subject);

        var tenant1Batch = Assert.Single(mockApi.Mocks["tenant1"].SentBatches);
        Assert.Equal(3, tenant1Batch.Count);
        Assert.Single(tenant1Batch, m => m.Subject == "tenant1" && m.MessageStream == DefaultPostmarkStream);
        Assert.Single(tenant1Batch, m => m.Subject == "tenant1;ml1" && m.MessageStream == "ml1");
        Assert.Single(tenant1Batch, m => m.Subject == "tenant1;ml2" && m.MessageStream == "ml2");

        var tenant2Batch = Assert.Single(mockApi.Mocks["tenant2"].SentBatches);
        var tenant2Message = Assert.Single(tenant2Batch);
        Assert.Equal("tenant2;ml3", tenant2Message.Subject);
        Assert.Equal("ml3", tenant2Message.MessageStream);
    }

    [Fact]
    public async Task ConversionToPostmarkMessageWorksAsExpected()
    {
        var email = new EmailMessage
        {
            From = new("from@email.com"),
            To = new("Some name", "to@email.com"),
            ReplyTo = new("Reply To", "reply@email.com"),
            Subject = "Subject line",
            TextContent = "Text content",
            HtmlContent = "<p>Some content</p>",
            Metadata = {
                ["my_metadata"] = "value",
            },
            Attachments =
            {
                new EmailAttachment{
                    Name = "Attachment.txt",
                    MimeType = "text/plain",
                    Base64Content = Convert.ToBase64String(Encoding.UTF8.GetBytes("Attachment content")),
                },
            },
            Headers =
            {
                ["custom-header"] = "value",
            }
        };

        var message = await provider.ToPostmarkMessageAsync(new MockPostmarkAdapter(), email);

        Assert.Equal("from@email.com", message.From);
        Assert.Equal("Some name <to@email.com>", message.To);
        Assert.Equal("Reply To <reply@email.com>", message.ReplyTo);
        Assert.Equal("Subject line", message.Subject);
        Assert.Equal("Text content", message.TextBody);
        Assert.Equal("<p>Some content</p>", message.HtmlBody);
        Assert.Single(message.Metadata);
        Assert.Equal("value", message.Metadata["my_metadata"]);
        Assert.Single(message.Attachments);
        var attachment = message.Attachments.Single();
        Assert.Equal("Attachment.txt", attachment.Name);
        Assert.Equal("text/plain", attachment.ContentType);
        Assert.Equal("Attachment content", Encoding.UTF8.GetString(Convert.FromBase64String(attachment.Content)));
        Assert.Equal(DefaultPostmarkStream, message.MessageStream);
        Assert.Single(message.Headers);
        Assert.Single(message.Headers, h => h.Name == "custom-header" && h.Value == "value");
    }

    [Fact]
    public async Task UnsubscribeLinkIsReplaced()
    {
        var email = new EmailMessage
        {
            TextContent = $"Text content{UnsubscribeLinkStartSentinal}{UnsubscribeLinkEndSentinal}",
            HtmlContent = $"<p>Some content {UnsubscribeLinkHtmlEncodedStartSentinal}{UnsubscribeLinkHtmlEncodedEndSentinal}</p>",
        };

        var message = await provider.ToPostmarkMessageAsync(new MockPostmarkAdapter(), email);

        Assert.Equal(
            $"Text content{UnsubscribeLinkStartSentinal}{PostmarkUnsubscribePlaceholder}{UnsubscribeLinkEndSentinal}",
            message.TextBody);
        Assert.Equal(
            $@"<p>Some content {UnsubscribeLinkHtmlEncodedStartSentinal}<a href=""{PostmarkUnsubscribePlaceholder}"">here</a>{UnsubscribeLinkHtmlEncodedEndSentinal}</p>",
            message.HtmlBody);
    }

    [Theory]
    [InlineData(0, "OK", EmailResultStatus.Success, "Delivery pending")]
    [InlineData(400, "Not OK", EmailResultStatus.Error, "400: Not OK")]
    [InlineData(400, null, EmailResultStatus.Error, "400: Unknown error")]
    public void CreationOfEmailResultWorksAsExpected(int errorCode, string? message, EmailResultStatus status, string resultMessage)
    {
        var email = new EmailMessage();
        var response = new PostmarkResponse
        {
            ErrorCode = errorCode,
            Message = message,
        };

        var result = PostmarkEmailProvider.ToEmailResult(email, response);

        Assert.Same(email, result.Email);
        Assert.Same(response, result.OriginalResult);
        Assert.Equal(status, result.Status);
        Assert.Equal(resultMessage, result.StatusMessage);
    }

    [Theory]
    [MemberData(nameof(EmailBatchingTestCases))]
    public void EmailsAreBatchedAsExpected(EmailSizeTestCase test)
    {
        var rejected = new List<EmailResult>();
        var emails = test.Emails.Select(e => e.GenerateEmail()).ToList();
        var batches = BatchEmails(emails, rejected).ToList();

        Assert.Equal(test.Batches.Count, batches.Count);
        foreach (var (expectedSubjects, actualBatch) in Enumerable.Zip(test.Batches, batches))
        {
            Assert.Equal(expectedSubjects.Count, actualBatch.Count);
            foreach (var (expectedSubject, actualEmail) in Enumerable.Zip(expectedSubjects, actualBatch))
            {
                Assert.Equal(expectedSubject, actualEmail.Subject);
            }
        }

        Assert.Equal(test.RejectedSubjects.Count, rejected.Count);
        foreach (var (expectedRejected, actualRejected) in Enumerable.Zip(test.RejectedSubjects, rejected))
        {
            Assert.Equal(expectedRejected, actualRejected.Email.Subject);
        }
    }

    public static IEnumerable<object[]> EmailBatchingTestCases()
    {
        var testCases = new List<EmailSizeTestCase>
        {
            EmailSizeTestCase.Accepted(new(TextSize: MaxBodySizeBytes)),
            EmailSizeTestCase.Rejected(new(TextSize: MaxBodySizeBytes+1)),
            EmailSizeTestCase.Accepted(new(HtmlSize: MaxBodySizeBytes)),
            EmailSizeTestCase.Rejected(new(HtmlSize: MaxBodySizeBytes+1)),
            EmailSizeTestCase.Accepted(new(TextSize: MaxBodySizeBytes-4, HtmlSize: MaxBodySizeBytes-4)
            {
                AttachmentSizes = {8},
            }),
            EmailSizeTestCase.Rejected(new(TextSize: MaxBodySizeBytes, HtmlSize: MaxBodySizeBytes)
            {
                AttachmentSizes = {1},
            }),
            new EmailSizeTestCase
            {
                Emails = Enumerable.Range(0, 501)
                    .Select(e => EmailSize.Total(e.ToString(), 1))
                    .ToList(),
                Batches =
                {
                    Enumerable.Range(0, 500).Select(e => e.ToString()).ToList(),
                    Enumerable.Range(500, 1).Select(e => e.ToString()).ToList(),
                }
            },
            new EmailSizeTestCase
            {
                Emails =
                {
                    EmailSize.Total("1", MaxMessageSizeBytes),
                    EmailSize.Total("2", MaxMessageSizeBytes),
                    EmailSize.Total("3", MaxMessageSizeBytes),
                    EmailSize.Total("4", MaxMessageSizeBytes),
                    EmailSize.Total("5", MaxMessageSizeBytes),
                    EmailSize.Total("6", MaxMessageSizeBytes),
                    EmailSize.Total("7", MaxMessageSizeBytes),
                },
                Batches =
                {
                    new List<string>{"1", "2", "3", "4", "5"},
                    new List<string>{"6", "7"},
                }
            },
        };
        return testCases.Select(t => new[] { t });
    }

    public record class EmailSizeTestCase
    {
        public IList<EmailSize> Emails { get; set; } = new List<EmailSize>();
        public IList<IList<string>> Batches { get; set; } = new List<IList<string>>();
        public IList<string> RejectedSubjects { get; set; } = new List<string>();

        public static EmailSizeTestCase Accepted(EmailSize email) => new()
        {
            Emails = { email },
            Batches = { new List<string> { email.Id } },
        };

        public static EmailSizeTestCase Rejected(EmailSize email) => new()
        {
            Emails = { email },
            RejectedSubjects = { email.Id },
        };
    }

    public record class EmailSize(string Id = "id", int TextSize = 0, int HtmlSize = 0)
    {
        public IList<int> AttachmentSizes { get; init; } = new List<int>();

        public static EmailSize Total(string id, int totalSize) => new EmailSize
        {
            Id = id,
            AttachmentSizes = { totalSize },
        };

        public EmailMessage GenerateEmail() => new EmailMessage
        {
            Subject = Id,
            TextContent = new string('t', TextSize),
            HtmlContent = new string('h', HtmlSize),
            Attachments = AttachmentSizes.Select((s, i) => new EmailAttachment
            {
                Name = i.ToString(),
                MimeType = "text/plain",
                Base64Content = new string('b', s),
            }).ToList(),
        };
    }

    private sealed class MockPostmarkApiService : IPostmarkApiService
    {
        public MockPostmarkAdapter GlobalMock = new MockPostmarkAdapter { Tenant = null };
        public IDictionary<string, MockPostmarkAdapter> Mocks { get; } = new Dictionary<string, MockPostmarkAdapter>();

        public IPostmarkAdapter? GetTenantClient(Tenant? tenant)
            => throw new NotImplementedException();

        public async Task<IPostmarkAdapter> GetOrCreateClientAsync(Tenant? tenant)
        {
            if (tenant is null)
            {
                return await Task.FromResult(GlobalMock);
            }

            if (!Mocks.TryGetValue(tenant.Abbreviation, out var value))
            {
                value = new MockPostmarkAdapter { Tenant = tenant };
                Mocks[tenant.Abbreviation] = value;
            }

            return value;
        }

        public string? GetMailingListStreamId(MailingList mailingList)
            => throw new NotImplementedException();

        public Task<string> GetOrCreateMailingListStreamIdAsync(IPostmarkAdapter client, MailingList mailingList)
            => Task.FromResult(mailingList.Slug);

        public Task DeleteMailingListStreamAsync(MailingList mailingList)
            => throw new NotImplementedException();

        public Task<IEnumerable<IPostmarkAdapter>> GetAllClients()
            => throw new NotImplementedException();
    }

    private sealed class MockPostmarkAdapter : IPostmarkAdapter
    {
        public Tenant? Tenant { get; init; }
        public IList<IList<PostmarkMessage>> SentBatches { get; } = [];

        public Task<IEnumerable<PostmarkResponse>> SendMessagesAsync(params PostmarkMessage[] messages)
        {
            SentBatches.Add(messages);
            return Task.FromResult(messages.Select(m => new PostmarkResponse()));
        }

        public Task<PostmarkMessageStream> CreateMessageStream(string id, MessageStreamType type, string name, string? description = null)
            => throw new NotImplementedException();

        public Task<WebhookConfigurationListingResponse> GetWebhookConfigurationsAsync(string? messageStream = null)
            => throw new NotImplementedException();

        public Task<WebhookConfiguration> CreateWebhookConfigurationAsync(string url, string? messageStream = null, HttpAuth? httpAuth = null, IEnumerable<HttpHeader>? httpHeaders = null, WebhookConfigurationTriggers? triggers = null)
            => throw new NotImplementedException();

        public Task<WebhookConfiguration> EditWebhookConfigurationAsync(long configurationId, string url, HttpAuth? httpAuth = null, IEnumerable<HttpHeader>? httpHeaders = null, WebhookConfigurationTriggers? triggers = null)
            => throw new NotImplementedException();

        public Task<PostmarkSuppressionListing> ListSuppressions(PostmarkSuppressionQuery query, string messageStream = "outbound")
            => throw new NotImplementedException();

        public Task<PostmarkBulkReactivationResult> DeleteSuppressions(IEnumerable<PostmarkSuppressionChangeRequest> suppressionChanges, string messageStream = "outbound")
            => throw new NotImplementedException();

        public Task<PostmarkMessageStreamArchivalConfirmation> ArchiveMessageStream(string id)
            => throw new NotImplementedException();
    }
}
