using System.Text.Json;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.Scores;
using OrchestrateApi.Storage;

namespace OrchestrateApi.Tests.Feature.Scores;

public class ScoreAttachmentStorageStrategyTests : AutoRollbackTestHarness
{
    private ScoreAttachmentStorageStrategy StorageStrategy { get; } = new();

    [Fact]
    public void CorrectLinkingEntityIsCreated()
    {
        var linkingEntity = Assert.IsType<ScoreAttachment>(StorageStrategy.CreateLinkingEntity(
            Guid.Empty,
            new EntityAttachmentLinkData { EntityId = 5 }));
        Assert.Equal(Guid.Empty, linkingEntity.BlobId);
        Assert.Equal(5, linkingEntity.ScoreId);
    }

    [Fact]
    public async Task ReturnsCorrectLinkedBlobIds()
    {
        var (score, blobId) = GenerateScoreAndAttachment(StorageStrategy.Name, "Searched");
        GenerateScoreAndAttachment(StorageStrategy.Name, "NotSearched");
        GenerateScoreAndAttachment("Other", null);
        await Context.SaveChangesAsync();

        var blobIds = await StorageStrategy
            .LinkedBlobIds(Context, new EntityAttachmentLinkData { EntityId = score!.Id })
            .ToListAsync();
        Assert.Single(blobIds);
        Assert.Contains(blobIds, e => e == blobId);
    }

    [Fact]
    public async Task ReturnsCorrectValidBlobIds()
    {
        var (_, blobA) = GenerateScoreAndAttachment(StorageStrategy.Name, "A");
        var (_, blobB) = GenerateScoreAndAttachment(StorageStrategy.Name, "B");
        GenerateScoreAndAttachment("Other", null);
        await Context.SaveChangesAsync();

        var blobIds = await StorageStrategy.ValidBlobIds(Context).ToListAsync();
        Assert.Equal(2, blobIds.Count);
        Assert.Contains(blobIds, e => e == blobA);
        Assert.Contains(blobIds, e => e == blobB);
    }

    public (Score?, Guid) GenerateScoreAndAttachment(string strategy, string? scoreName)
    {
        var blob = Context.Add(new OrchestrateBlob
        {
            Name = "test.txt",
            MimeType = "text/plain",
            StorageStrategy = new OrchestrateBlobStorageStrategyData
            {
                Name = strategy,
                AccessData = JsonSerializer.SerializeToDocument(
                    new EntityAttachmentAccessData { Sensitivity = EntityAttachmentSensitivity.Secured }),
            }
        }).Entity;

        Score? score = null;
        if (scoreName != null)
        {
            score = Context.Add(new Score { Title = scoreName }).Entity;
            Context.Add(new ScoreAttachment { Score = score, BlobId = blob.Id });
        }

        return (score, blob.Id);
    }
}
