using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature;
using OrchestrateApi.Security;

namespace OrchestrateApi.Tests.Feature;

public class LandingServiceTests : FullIntegrationTestHarness
{
    private readonly LandingService landingService;

    public LandingServiceTests()
    {
        this.landingService = GetService<LandingService>();
    }

    [Fact]
    public async Task PersonalDetailsArePopulatedCorrectly()
    {
        var member = new Member
        {
            FirstName = "Jack",
            LastName = "Beanstalk",
            Details = new MemberDetails
            {
                Email = "jack@beanstalk.com",
                Address = "Magic Castle",
                PhoneNo = "123456",
                FeeClass = FeeClass.Special,
            },
            MemberInstrument =
            {
                new MemberInstrument {InstrumentName = "Flute"},
                new MemberInstrument {InstrumentName = "Pan pipes"},
            },
        };
        Context.Add(member);
        await Context.SaveChangesAsync();

        var dto = await this.landingService.BuildPersonalDataDtoAsync(member.Id);

        var details = dto.Details;
        Assert.Equal("Jack Beanstalk", details.Name);
        Assert.Equal("jack@beanstalk.com", details.Email);
        Assert.Equal("Magic Castle", details.Address);
        Assert.Equal("123456", details.PhoneNo);
        Assert.Equal(FeeClass.Special, details.FeeClass);
        Assert.Equal(["Flute", "Pan pipes"], details.Instruments);
    }

    [Fact]
    public async Task ActiveMailingListsArePopulated()
    {
        var member = new Member
        {
            FirstName = "F",
            LastName = "L",
            Details = new() { Address = ".", PhoneNo = "." },
        };
        var ensemble = new Ensemble { Name = "E", EnsembleMembership = { new() { Member = member } } };
        Context.Add(new MailingList
        {
            Name = "Member ML",
            Slug = "member-ml",
            Members = { member },
        });
        Context.Add(new MailingList
        {
            Name = "Ensemble ML",
            Slug = "ensemble-ml",
            Ensembles = { ensemble },
        });
        await Context.SaveChangesAsync();

        var dto = await this.landingService.BuildPersonalDataDtoAsync(member.Id);
        Assert.Equal(2, dto.MailingLists.Count());
        Assert.Single(dto.MailingLists, e => e.Name == "Member ML");
        Assert.Single(dto.MailingLists, e => e.Name == "Ensemble ML");
    }

    [Fact]
    public async Task TenantDetailsArePopulatedCorrectly()
    {
        var dto = await this.landingService.BuildTenantDataDtoAsync(new[] { FeaturePermission.PlatformAccess });
        Assert.Equal(OrchestrateSeed.GroupName, dto.Details.Name);
        Assert.Equal(OrchestrateSeed.Website, dto.Details.Website);
    }

    [Fact]
    public async Task NoPlatformAccessResultsInThrow()
    {
        await Assert.ThrowsAsync<InvalidOperationException>(
            () => this.landingService.BuildTenantDataDtoAsync(Array.Empty<FeaturePermission>()));
    }

    [Fact]
    public async Task AssetSummaryIsCalculatedCorrectly()
    {
        var dto = await BuildTenantDataAsync(FeaturePermission.AssetRead);
        Assert.NotNull(dto.Assets);
        Assert.Equal(2, dto.Assets.Count);
        Assert.Equal(1, dto.Assets.OnLoanCount);
    }

    [Fact]
    public async Task NoAssetSummaryForNoPermission()
    {
        var dto = await BuildTenantDataAsync();
        Assert.Null(dto.Assets);
    }

    [Theory]
    [InlineData(FeaturePermission.ScoreRead)]
    [InlineData(null)]
    public async Task MusicLibrarySummaryIsCalculatedCorrectly(FeaturePermission? fp)
    {
        var existingScores = await Context.Score.ToListAsync();
        var ensembles = await Context.Ensemble.Where(e => e.Status == EnsembleStatus.Active).ToListAsync();
        Context.Add(new Score
        {
            Title = "Unplayed",
        });
        Context.Add(new Concert
        {
            Occasion = "Something",
            Date = DateTime.UtcNow.AddYears(-1).AddDays(1),
            Performance =
            {
                new Performance
                {
                    Ensemble = ensembles[0],
                    PerformanceScores =
                    {
                        new PerformanceScore { Score = existingScores[0] },
                        new PerformanceScore { Score = existingScores[1] },
                    }
                },
            }
        });
        Context.Add(new Concert
        {
            Occasion = "Something else",
            Date = DateTime.UtcNow.AddYears(-1).AddDays(3),
            Performance =
            {
                new Performance
                {
                    Ensemble = ensembles[1],
                    PerformanceScores =
                    {
                        // Same piece performed shouldn't be counted twice
                        new PerformanceScore{ Score = existingScores[1] },
                    }
                },
            }
        });
        await Context.SaveChangesAsync();

        var dto = await BuildTenantDataAsync(fp);
        if (fp.HasValue)
        {
            Assert.NotNull(dto.MusicLibrary);
            Assert.Equal(3, dto.MusicLibrary.Count);
            Assert.Equal(1, dto.MusicLibrary.NeverPlayedCount);
            Assert.Equal(2, dto.MusicLibrary.PerformedInLastYearCount);
        }
        else
        {
            Assert.Null(dto.MusicLibrary);
        }
    }

    [Theory]
    [InlineData(FeaturePermission.ContactsRead)]
    [InlineData(null)]
    public async Task ContactsSummaryIsCalculatedCorrectly(FeaturePermission? fp)
    {
        var existingScores = await Context.Score.ToListAsync();
        var ensembles = await Context.Ensemble.Where(e => e.Status == EnsembleStatus.Active).ToListAsync();
        Context.Add(new Contact { Name = "1" });
        Context.Add(new Contact { Name = "2", ArchivedOn = DateTime.UtcNow.AddMinutes(-1) });
        Context.Add(new Contact { Name = "3", ArchivedOn = DateTime.UtcNow.AddMinutes(1) });
        await Context.SaveChangesAsync();

        var dto = await BuildTenantDataAsync(fp);
        if (fp.HasValue)
        {
            Assert.NotNull(dto.Contacts);
            Assert.Equal(1, dto.Contacts.CurrentCount);
        }
        else
        {
            Assert.Null(dto.Contacts);
        }
    }

    [Theory]
    [InlineData(FeaturePermission.EnsembleRead)]
    [InlineData(null)]
    public async Task EnsembleSummariesIsGeneratedCorrectly(FeaturePermission? fp)
    {
        Context.Add(new Ensemble
        {
            Name = "Disbanded",
            Status = EnsembleStatus.Disbanded,
        });
        await Context.SaveChangesAsync();

        var dto = await BuildTenantDataAsync(fp);
        if (fp.HasValue)
        {
            Assert.Equal(3, dto.EnsembleSummaries.Count());
            Assert.DoesNotContain(dto.EnsembleSummaries, e => e.Name == "Disbanded");
            var concertBand = dto.EnsembleSummaries.Single(e => e.Name == OrchestrateSeed.ConcertBandName);
            Assert.NotNull(concertBand.FirstConcertDate);
            Assert.Equal(new DateTime(2016, 10, 8), concertBand.FirstConcertDate.Value, TimeSpan.FromDays(1));
            Assert.Equal(3, concertBand.ConcertCount);
            Assert.Equal(1, concertBand.MemberCount);
        }
        else
        {
            Assert.Empty(dto.EnsembleSummaries);
        }
    }

    [Theory]
    [InlineData(FeaturePermission.MemberRead)]
    [InlineData(null)]
    public async Task MembershipSnapshotIsCalculatedCorrectly(FeaturePermission? fp)
    {
        var memberToLeave = await Context.Member.FirstAsync(e => e.DateLeft == null);
        memberToLeave.DateLeft = DateTime.UtcNow.AddDays(-3);
        Context.Add(new Member
        {
            FirstName = "Joined",
            LastName = "InLast12Months",
            DateJoined = DateTime.UtcNow.AddDays(-10),
        });
        Context.Add(new Member
        {
            FirstName = "JoinedAndLeft",
            LastName = "InLast12Months",
            DateJoined = DateTime.UtcNow.AddMonths(-3),
            DateLeft = DateTime.UtcNow.AddDays(-4),
        });
        await Context.SaveChangesAsync();

        var dto = await BuildTenantDataAsync(fp);
        if (fp.HasValue)
        {
            Assert.NotNull(dto.MembershipSnapshot);
            Assert.Equal(1, dto.MembershipSnapshot.LeftCount);
            Assert.Equal(1, dto.MembershipSnapshot.JoinedCount);
            Assert.Equal(1, dto.MembershipSnapshot.JoinedAndLeftCount);
            Assert.Equal(1, dto.MembershipSnapshot.StayedCount);
        }
        else
        {
            Assert.Null(dto.MembershipSnapshot);
        }
    }

    [Fact]
    public async Task LastConcertDetermined()
    {
        var dto = await BuildTenantDataAsync(FeaturePermission.ConcertRead);
        Assert.NotNull(dto.LastConcert);
        Assert.Equal("Christmas Gig", dto.LastConcert.Occasion);
        var performance = Assert.Single(dto.LastConcert.Performances);
        Assert.Equal(OrchestrateSeed.ConcertBandName, performance.EnsembleName);
        Assert.Equal(1, performance.MemberCount);
        Assert.Equal(1, performance.PieceCount);
    }

    [Fact]
    public async Task NullReturnedIfNoLatestConcert()
    {
        await Context.Concert.ExecuteDeleteAsync();
        var dto = await BuildTenantDataAsync(FeaturePermission.ConcertRead);
        Assert.Null(dto.LastConcert);
    }

    [Fact]
    public async Task NullReturnedIfNoPermission()
    {
        var dto = await BuildTenantDataAsync();
        Assert.Null(dto.LastConcert);
    }

    [Fact]
    public async Task OpenAndLastClosedBillingPeriodReturned()
    {
        var dto = await BuildTenantDataAsync(FeaturePermission.MemberBillingRead);
        Assert.Equal(2, dto.LatestBillingPeriods.Count());

        var semester1 = dto.LatestBillingPeriods.First();
        Assert.Equal("2021 - Semester 1", semester1.Name);
        Assert.Equal(new DateTime(2021, 6, 30), semester1.Due, TimeSpan.FromDays(1));
        Assert.Equal(2, semester1.InvoiceCount);
    }

    [Fact]
    public async Task NoneReturnedIfNoBillingPeriods()
    {
        await Context.MemberBillingPeriod.ExecuteDeleteAsync();
        var dto = await BuildTenantDataAsync(FeaturePermission.MemberBillingRead);
        Assert.Empty(dto.LatestBillingPeriods);
    }

    [Fact]
    public async Task NoneReturnedIfNoPermission()
    {
        var dto = await BuildTenantDataAsync();
        Assert.Empty(dto.LatestBillingPeriods);
    }

    [Fact]
    public async Task MailingListSummaryGeneratedCorrectly()
    {
        var cutoff = LandingService.MailingListUsageDaysCutOff;
        Context.Add(new MailingList
        {
            Name = "ML",
            Slug = "ml",
            Messages =
            {
                new MailingListMessageMetadata{Sent = DateTime.UtcNow.AddDays(-(cutoff + 1))},
                new MailingListMessageMetadata{Sent = DateTime.UtcNow.AddDays(-2)},
                new MailingListMessageMetadata{Sent = DateTime.UtcNow.AddDays(-3)},
                new MailingListMessageMetadata{Sent = DateTime.UtcNow.AddDays(-4)},
            }
        });
        await Context.SaveChangesAsync();

        var dto = await BuildTenantDataAsync(FeaturePermission.MailingListRead);
        var mailingList = Assert.Single(dto.MailingListUsage);
        Assert.Equal(3, mailingList.UsageCount);
    }

    [Fact]
    public async Task MailingListSummaryEmptyIfNoPermission()
    {
        var dto = await BuildTenantDataAsync();
        Assert.Empty(dto.MailingListUsage);
    }

    private Task<TenantDataDto> BuildTenantDataAsync(FeaturePermission? fp = null)
    {
        FeaturePermission[] permissions = fp.HasValue
            ? [FeaturePermission.PlatformAccess, fp.Value]
            : [FeaturePermission.PlatformAccess];
        return this.landingService.BuildTenantDataDtoAsync(permissions);
    }
}
