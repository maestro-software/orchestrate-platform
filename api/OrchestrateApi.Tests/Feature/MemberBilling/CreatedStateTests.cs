using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.MemberBilling;

namespace OrchestrateApi.Tests.Feature.MemberBilling
{
    public class CreatedStateTests : BillingPeriodStateTestHarness
    {
        public CreatedStateTests()
        {
            BillingConfig.ConcessionRate = 0.5M;
            BillingConfig.AssociationPeriodFeeDollars = 40;
            BillingConfig.EarlyBirdDiscountDollars = 10;
            BillingConfig.InvoicePrefix = "INV";
            BillingConfig.NextInvoiceNumber = 50;
        }

        private IQueryable<MemberInvoice> BillingPeriodInvoices =>
            Context.MemberInvoice.Where(e => e.MemberBillingPeriodId == BillingPeriod.Id);

        protected override MemberBillingPeriod BillingPeriod { get; } = new MemberBillingPeriod
        {
            Name = "Test billing period",
            State = MemberBillingPeriodState.Created,
            EarlyBirdDue = DateTime.UtcNow.Date,
            Due = DateTime.UtcNow.AddDays(7),
        };

        [Fact]
        public async Task DoesntTransitionIfNotGeneratingInvoices()
        {
            await StateContext.SendInvoices();
            Assert.Equal(MemberBillingPeriodState.Created, BillingPeriod.State);

            await StateContext.PerformTemporalTasks();
            Assert.Equal(MemberBillingPeriodState.Created, BillingPeriod.State);

            await StateContext.CloseBillingPeriod();
            Assert.Equal(MemberBillingPeriodState.Created, BillingPeriod.State);
        }

        [Fact]
        public async Task WillNotGenerateAnInvoiceForASpecialMember()
        {
            var member = await SetupForSingleMember();
            var details = await Context.Member.Select(e => e.Details!).FirstAsync(e => e.MemberId == member.Id);
            details.FeeClass = FeeClass.Special;
            await Context.SaveChangesAsync();

            await StateContext.GenerateInvoices();

            Assert.Equal(0, await BillingPeriodInvoices.CountAsync());
            Assert.NotNull(BillingPeriod.Generated);
            Assert.Equal(DateTime.UtcNow, BillingPeriod.Generated.Value, TimeSpan.FromSeconds(1));
            Assert.Equal(MemberBillingPeriodState.Draft, BillingPeriod.State);
        }

        [Fact]
        public async Task WillNotGenerateAnInvoiceForLeftMembers()
        {
            var member = await SetupForSingleMember();
            member.DateLeft = DateTime.UtcNow.AddDays(-1);
            await Context.SaveChangesAsync();

            await StateContext.GenerateInvoices();

            Assert.Equal(0, await BillingPeriodInvoices.CountAsync());
            Assert.NotNull(BillingPeriod.Generated);
            Assert.Equal(DateTime.UtcNow, BillingPeriod.Generated.Value, TimeSpan.FromSeconds(1));
            Assert.Equal(MemberBillingPeriodState.Draft, BillingPeriod.State);
        }

        [Fact]
        public async Task WillNotGenerateAnInvoiceIfOneAlreadyExists()
        {
            var member = await SetupForSingleMember();
            var existingInvoice = new MemberInvoice
            {
                Member = member,
                MemberBillingPeriod = BillingPeriod,
                Reference = string.Empty,
            };
            await Context.MemberInvoice.AddAsync(existingInvoice);
            await Context.SaveChangesAsync();

            await StateContext.GenerateInvoices();

            Assert.Equal(1, await BillingPeriodInvoices.CountAsync());
            Assert.Same(existingInvoice, await GetMemberInvoice(member));
            Assert.NotNull(BillingPeriod.Generated);
            Assert.Equal(DateTime.UtcNow, BillingPeriod.Generated.Value, TimeSpan.FromSeconds(1));
            Assert.Equal(MemberBillingPeriodState.Draft, BillingPeriod.State);
        }
        [Theory]
        [InlineData("generator@email.com", false)]
        [InlineData("not.generator@email.com", true)]
        [InlineData(null, true)]
        public async Task WillOnlySendEmailWhenGeneratorIsNotContactEmail(string? generatorEmail, bool willSendEmail)
        {
            BillingConfig.NotificationContactEmail = "generator@email.com";
            BillingPeriod.NotificationContactNotified = null;
            var member = await SetupForSingleMember();

            await StateContext.GenerateInvoices(generatorEmail);

            var notificationsSent = MockEmailService.AllSentEmails.Count(e => e is BillingPeriodGeneratedNotificationEmail);
            Assert.Equal(willSendEmail, notificationsSent == 1);
            Assert.Equal(willSendEmail, BillingPeriod.NotificationContactNotified.HasValue);
        }

        private async Task<Member> SetupForSingleMember()
        {
            var member = await Context.Member.FirstAsync(e => e.DateLeft == null);
            var membership = await Context.EnsembleMembership.Where(e => e.MemberId == member.Id).ToListAsync();
            Context.EnsembleMembership.RemoveRange(membership);

            var otherMembers = await Context.Member.Where(e => e.Id != member.Id).ToListAsync();
            Context.Member.RemoveRange(otherMembers);

            await Context.SaveChangesAsync();

            return member;
        }

        private async Task<MemberInvoice?> GetMemberInvoice(Member member)
        {
            return await BillingPeriodInvoices.Where(e => e.MemberId == member.Id)
                .Include(e => e.LineItems)
                .FirstOrDefaultAsync();
        }
    }
}
