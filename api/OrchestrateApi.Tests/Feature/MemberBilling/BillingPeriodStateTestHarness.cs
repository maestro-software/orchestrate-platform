using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.MemberBilling;

namespace OrchestrateApi.Tests.Feature.MemberBilling
{
    public abstract class BillingPeriodStateTestHarness : AutoRollbackTestHarness
    {
        private readonly Lazy<BillingPeriodStateContext> lazyStateContext;
        private readonly ILoggerFactory loggerFactory = TestHelper.BuildMockLoggerFactory();

        protected BillingPeriodStateTestHarness()
        {
            AppConfig = new MockAppConfiguration();
            MockEmailService = new MockEmailService();
            MockMemberInvoiceSender = new MemberInvoiceSender(
                MockEmailService,
                new MockMemberInvoiceRenderingService());

            this.lazyStateContext = new Lazy<BillingPeriodStateContext>(() => BuildStateMachine(BillingPeriod));

            Tenant = new Tenant
            {
                TimeZone = "Australia/Perth",
            };
            BillingConfig = new MemberBillingConfig
            {
                InvoicePrefix = string.Empty,
                BankBsb = string.Empty,
                BankAccountNo = string.Empty,
                NotificationContactName = string.Empty,
                NotificationContactEmail = "test@contact.com",
            };
        }

        protected MockAppConfiguration AppConfig { get; }
        protected MockEmailService MockEmailService { get; }
        protected MemberInvoiceSender MockMemberInvoiceSender { get; }
        protected BillingPeriodStateContext StateContext => this.lazyStateContext.Value;
        protected Tenant Tenant { get; }
        protected MemberBillingConfig BillingConfig { get; }
        protected abstract MemberBillingPeriod BillingPeriod { get; }

        protected BillingPeriodStateContext BuildStateMachine(MemberBillingPeriod billingPeriod)
        {
            return new BillingPeriodStateContext(
                this.loggerFactory,
                Tenant,
                BillingConfig,
                billingPeriod,
                Context,
                AppConfig,
                MockEmailService,
                MockMemberInvoiceSender);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                this.loggerFactory.Dispose();
            }
        }
    }
}
