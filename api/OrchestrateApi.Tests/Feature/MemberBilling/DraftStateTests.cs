using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Tests.Feature.MemberBilling
{
    public class DraftStateTests : BillingPeriodStateTestHarness
    {
        public DraftStateTests()
        {
            Tenant.TimeZone = "Australia/Perth";
            BillingConfig.EarlyBirdDiscountDollars = 10;
            BillingConfig.EarlyBirdDiscountDaysValid = 3;
        }

        protected override MemberBillingPeriod BillingPeriod { get; } = new MemberBillingPeriod
        {
            Name = "Test billing period",
            State = MemberBillingPeriodState.Draft,
            Due = DateTime.UtcNow.AddDays(7),
        };

        [Fact]
        public async Task DoesntTransitionIfNotSendingInvoices()
        {
            await StateContext.GenerateInvoices();
            Assert.Equal(MemberBillingPeriodState.Draft, BillingPeriod.State);

            await StateContext.PerformTemporalTasks();
            Assert.Equal(MemberBillingPeriodState.Draft, BillingPeriod.State);

            await StateContext.CloseBillingPeriod();
            Assert.Equal(MemberBillingPeriodState.Draft, BillingPeriod.State);
        }

        [Fact]
        public async Task IfEarlyBirdDueIsAlreadySetItWontBeUpdated()
        {
            var existingEarlyBirdDueDate = DateTime.UtcNow.AddHours(1);
            BillingPeriod.EarlyBirdDue = existingEarlyBirdDueDate;
            await StateContext.SendInvoices();

            Assert.Equal(existingEarlyBirdDueDate, BillingPeriod.EarlyBirdDue);
        }

        [Theory]
        [InlineData(-10)]
        [InlineData(-1)]
        [InlineData(0)]
        public async Task IfEarlyBirdDiscountIsNotPositiveEarlyBirdDueWontBeSet(int discount)
        {
            BillingConfig.EarlyBirdDiscountDollars = discount;

            await StateContext.SendInvoices();

            Assert.Null(BillingPeriod.EarlyBirdDue);
        }

        [Fact]
        public async Task AutoSetEarlyBirdDateWillBeEndOfDayInTargetTimezone()
        {
            await StateContext.SendInvoices();

            var expectedEarlyBirdDue = DateTime.UtcNow
                .AddDays(BillingConfig.EarlyBirdDiscountDaysValid)
                .AddHours(8) // Perth timezone
                .Date
                .AddHours(15)
                .AddMinutes(59)
                .AddSeconds(59);
            Assert.Equal(expectedEarlyBirdDue, BillingPeriod.EarlyBirdDue);

            Assert.True(BillingPeriod.Sent.HasValue);
            Assert.Equal(DateTime.UtcNow, BillingPeriod.Sent.Value, TimeSpan.FromSeconds(1));
        }

        [Fact]
        public async Task SendingInvoicesOnlySendsInvoicesThatHaventBeenSentAlready()
        {
            var member = await Context.Member.Include(e => e.Details).FirstAsync();
            var memberInvoiceToBeSent = new MemberInvoice
            {
                MemberBillingPeriod = BillingPeriod,
                Member = member,
                Reference = "",
            };
            await Context.MemberInvoice.AddAsync(memberInvoiceToBeSent);
            await Context.MemberInvoice.AddAsync(new MemberInvoice
            {
                MemberBillingPeriod = BillingPeriod,
                Member = await Context.Member.Skip(1).FirstAsync(),
                Reference = "",
                Sent = DateTime.UtcNow,
            });
            await Context.SaveChangesAsync();

            await StateContext.SendInvoices();

            Assert.True(BillingPeriod.Sent.HasValue);
            Assert.Equal(DateTime.UtcNow, BillingPeriod.Sent.Value, TimeSpan.FromSeconds(1));
            Assert.Single(MockEmailService.AllSentEmails);
            Assert.NotNull(MockEmailService.LastEmail);
            Assert.Equal(member.Details!.Email, MockEmailService.LastEmail.To?.Address);
            Assert.NotNull(memberInvoiceToBeSent.Sent);
            Assert.Equal(DateTime.UtcNow, memberInvoiceToBeSent.Sent.Value, TimeSpan.FromSeconds(1));
            Assert.Equal(MemberBillingPeriodState.Open, BillingPeriod.State);
        }
    }
}
