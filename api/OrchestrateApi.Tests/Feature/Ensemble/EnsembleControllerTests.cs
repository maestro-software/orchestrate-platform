using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.Ensembles;

namespace OrchestrateApi.Tests.Feature.Ensembles
{
    public class EnsembleControllerTests : AutoRollbackTestHarness
    {
        public EnsembleControllerTests()
        {
            EnsembleController = new EnsembleController(
                Context,
                new OrchestratePersistenceManager(Context, null!, null!, null!, new StaticTenantService(OrchestrateSeed.TenantId)));
        }

        private EnsembleController EnsembleController { get; }

        [Fact]
        public async Task GetEnsembleStatsReturnsCorrectValues()
        {
            var stats = (await EnsembleController.GetEnsembleStatsAsync(EnsembleStatus.Active, default)).ToList();

            Assert.Equal(3, stats.Count);
            for (int i = 1; i < 3; i++)
            {
                Assert.True(stats[i - 1].CurrentMemberCount <= stats[i].CurrentMemberCount);
            }

            var concertBand = stats.First(e => e.Name == "Concert Band");
            Assert.Equal(1, concertBand.CurrentMemberCount);
            Assert.Equal(3, concertBand.PerformanceCount);
            Assert.Equal(1, concertBand.Membership[FeeClass.Concession.ToString()]);
        }

        [Theory]
        [InlineData(EnsembleStatus.Active, 2)]
        [InlineData(EnsembleStatus.Disbanded, 1)]
        public async Task GetEnsembleStatsFiltersForStatusCorrectly(EnsembleStatus status, int expectedCount)
        {
            var ensemble = await Context.Ensemble.FirstAsync();
            ensemble.Status = EnsembleStatus.Disbanded;
            await Context.SaveChangesAsync();

            var stats = await EnsembleController.GetEnsembleStatsAsync(status, default);

            Assert.Equal(expectedCount, stats.Count());
        }

        [Fact]
        public async Task UnknownEnsembleReturnsNotFound()
        {
            var result = await EnsembleController.GetEnsembleSummaryAsync(-1, default);
            Assert.IsType<NotFoundResult>(result.Result);
        }

        [Fact]
        public async Task GetEnsembleSummaryGetsCorrectData()
        {
            var ensemble = await Context.Ensemble.FirstAsync(e => e.Name == "Concert Band");
            var stats = (await EnsembleController.GetEnsembleSummaryAsync(ensemble.Id, default)).Value!;

            Assert.Equal(1, stats.CurrentMemberCount);
            Assert.Equal(1, stats.PastMemberCount);
            Assert.Equal(3, stats.PerformanceCount);
            Assert.Equal(new DateTime(2016, 10, 8), stats.FirstConcertDate!.Value, TimeSpan.FromDays(1));
            Assert.Equal(new DateTime(2016, 12, 13), stats.LastConcertDate!.Value, TimeSpan.FromDays(1));
            Assert.Equal(1, stats.Membership.First(e => e.FeeClass == FeeClass.Concession).Count);
            Assert.Null(stats.Membership.FirstOrDefault(e => e.FeeClass == FeeClass.Full));
            Assert.Null(stats.Membership.FirstOrDefault(e => e.FeeClass == FeeClass.Special));

            var stats2016 = stats.YearlyStats.First(e => e.Year == 2016);
            Assert.Equal(3, stats2016.PerformanceCount);
            Assert.Equal(1, stats2016.MemberJoinedCount);
            Assert.Equal(0, stats2016.MemberStayedCount);
            Assert.Equal(0, stats2016.MemberLeftCount);
            Assert.Equal(1, stats2016.MemberJoinedAndLeftCount);

            for (var year = 2017; year <= DateTime.UtcNow.Year; year++)
            {
                var yearStats = stats.YearlyStats.First(e => e.Year == year);
                Assert.Equal(0, yearStats.MemberJoinedCount);
                Assert.Equal(1, yearStats.MemberStayedCount);
                Assert.Equal(0, yearStats.MemberLeftCount);
                Assert.Equal(0, yearStats.MemberJoinedAndLeftCount);
            }
        }

        [Theory]
        [InlineData(2018, 2018, null)]
        [InlineData(2018, 2019, null)]
        [InlineData(2018, 2020, MembershipCategory.Left)]
        [InlineData(2018, 2021, MembershipCategory.Stayed)]
        [InlineData(2018, null, MembershipCategory.Stayed)]
        [InlineData(2020, 2020, MembershipCategory.JoinedAndLeft)]
        [InlineData(2020, 2021, MembershipCategory.Joined)]
        [InlineData(2020, null, MembershipCategory.Joined)]
        [InlineData(2021, 2021, null)]
        [InlineData(2021, 2022, null)]
        [InlineData(2021, null, null)]
        public async Task CategorisationOfMembershipInYearWorksCorrectly(int yearJoined, int? yearLeft, MembershipCategory? category)
        {
            var membership = await Context.EnsembleMembership.FirstAsync();
            membership.DateJoined = new DateTime(yearJoined, 1, 1).AsUtc();
            membership.DateLeft = yearLeft.HasValue ? new DateTime(yearLeft.Value, 12, 31).AsUtc() : null;

            // Remove other membership to guarantee there's 0 or 1 members in the candidate category
            var otherMembership = await Context.EnsembleMembership.Where(e => e.Id != membership.Id).ToListAsync();
            Context.RemoveRange(otherMembership);

            // Force a YearlyStats entry below regardless of membership
            var concertEntry = Context.Add(new Concert { Occasion = "test", Date = new DateTime(2020, 1, 1).AsUtc() });
            Context.Add(new Performance { Concert = concertEntry.Entity, EnsembleId = membership.EnsembleId });

            await Context.SaveChangesAsync();

            var stats = (await EnsembleController.GetEnsembleSummaryAsync(membership.EnsembleId, default)).Value!;
            var yearStats = stats.YearlyStats.First(e => e.Year == 2020);

            Assert.Equal(category == MembershipCategory.Joined ? 1 : 0, yearStats.MemberJoinedCount);
            Assert.Equal(category == MembershipCategory.Stayed ? 1 : 0, yearStats.MemberStayedCount);
            Assert.Equal(category == MembershipCategory.Left ? 1 : 0, yearStats.MemberLeftCount);
            Assert.Equal(category == MembershipCategory.JoinedAndLeft ? 1 : 0, yearStats.MemberJoinedAndLeftCount);
        }

        [Fact]
        public async Task MemberPerformanceCountsAreCorrect()
        {
            var concertBand = await Context.Ensemble.FirstAsync(e => e.Name == "Concert Band");
            var bobMarley = await Context.Member.FirstAsync(e => e.FirstName == "Bob");
            var glennStevens = await Context.Member.FirstAsync(e => e.FirstName == "Glenn");
            var iggyPop = await Context.Member.FirstAsync(e => e.FirstName == "Iggy");

            var performanceCounts = await EnsembleController.GetMemberPerformanceCountsAsync(concertBand.Id, default);

            Assert.Equal(2, performanceCounts[bobMarley.Id]);
            Assert.Equal(1, performanceCounts[glennStevens.Id]);
            Assert.False(performanceCounts.ContainsKey(iggyPop.Id));
        }

        public enum MembershipCategory
        {
            Joined, Stayed, Left, JoinedAndLeft,
        }
    }
}
