using System.Globalization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;
using OrchestrateApi.Feature.MailingLists;
using OrchestrateApi.Security;

using static OrchestrateApi.Email.EmailHelpers;
using static OrchestrateApi.Feature.MailingLists.MailingListIncomingEmailHandler;

namespace OrchestrateApi.Tests.Feature.MailingLists;

public class MailingListIncomingEmailHandlerTests : FullIntegrationTestHarness
{
    // Corresponds with OrchestrateApi.Tests/appsettings.json
    private const string ApexDomain = "test.domain.com";

    private const string MailingListAddress = $"{OrchestrateSeed.GroupAbbr}#test-list@{ApexDomain}";

    private readonly MailingListIncomingEmailHandler handler;
    private readonly MockEmailService emailService;

    public MailingListIncomingEmailHandlerTests()
    {
        this.handler = GetSpecificService<IIncomingEmailHandler, MailingListIncomingEmailHandler>();
        this.emailService = GetServiceAs<IEmailService, MockEmailService>();
    }

    // Handles multiple addresses in the to column only one of which might be
    // a mailing list email, so we need to handle it
    [Theory]
    [InlineData("not.email")]
    [InlineData($"invalid@{ApexDomain}")]
    [InlineData("invalid@other.domain")]
    public async Task DropsInvalidEmailAddresses(string email)
    {
        var incomingEmail = new IncomingEmail
        {
            From = new() { Email = "from@email.com" },
            To = { new() { Email = email } },
        };

        await this.handler.HandleAsync(incomingEmail);

        Assert.Null(this.emailService.LastEmail);
    }

    [Theory]
    [InlineData($"unknown#list@{ApexDomain}")]
    [InlineData($"{OrchestrateSeed.GroupAbbr}#unknown@{ApexDomain}")]
    public async Task UndeliverableEmailSentForUnknownMailingList(string toAddress)
    {
        await this.handler.HandleAsync(new IncomingEmail
        {
            From = new() { Email = "from@email.com" },
            To = { new() { Email = toAddress } },
            TextContent = "Hello",
            HtmlContent = "<p>Hello</p>",
        });

        Assert.NotNull(this.emailService.LastEmail);
        Assert.Contains("Hello", this.emailService.LastEmail.TextContent);
        Assert.Contains("doesn't exist", this.emailService.LastEmail.TextContent);
        Assert.Contains("<p>Hello</p>", this.emailService.LastEmail.HtmlContent);
        Assert.Contains("doesn't exist", this.emailService.LastEmail.HtmlContent);
    }

    [Theory]
    [InlineData(StandardRole.Administrator, true)]
    [InlineData(StandardRole.CommitteeMember, true)]
    [InlineData(StandardRole.Editor, false)]
    [InlineData(StandardRole.Viewer, false)]
    [InlineData(null, false)]
    public async Task SpecificRolesCanSendToGlobalSendOnly(string? role, bool isAllowed)
    {
        if (role != null)
        {
            var userManager = GetService<UserManager<OrchestrateUser>>();
            var user = new OrchestrateUser { UserName = "user", Email = "user@email.com" };
            var result = await userManager.CreateAsync(user);
            Assert.True(result.Succeeded, string.Join(", ", result.Errors.Select(e => e.Description)));

            result = await userManager.AddToRoleAsync(user, role);
            Assert.True(result.Succeeded, string.Join(", ", result.Errors.Select(e => e.Description)));
        }

        var mailingList = AddMailingList(allowedSenders: AllowedSenders.GlobalSendOnly);
        AddMailingListContact(mailingList, "Some Person", "contact@email.com");
        await Context.SaveChangesAsync();

        await this.handler.HandleAsync(new IncomingEmail
        {
            From = new() { Name = "User", Email = "user@email.com" },
            To = { new() { Email = MailingListAddress } },
            Subject = "Subject",
            HtmlContent = "<p>Hello</p>",
        });

        var sentEmail = Assert.Single(this.emailService.AllSentEmails);

        if (isAllowed)
        {
            Assert.Equal(new EmailMessageAddress("User via Orchestrate", MailingListAddress), sentEmail.From);
            Assert.Equal(new EmailMessageAddress("Some Person", "contact@email.com"), sentEmail.To);
            Assert.Contains("Subject", sentEmail.Subject);
        }
        else
        {
            Assert.Equal(this.emailService.DefaultFrom, sentEmail.From);
            Assert.Equal(new EmailMessageAddress("User", "user@email.com"), sentEmail.To);
            Assert.Contains("don't have permission", sentEmail.HtmlContent);
        }
    }

    [Theory]
    [InlineData("known@email.com", true)]
    [InlineData("unknown@email.com", false)]
    public async Task RecipientsOnlyBlocksUnknownEmails(string fromEmail, bool isAllowed)
    {
        var mailingList = AddMailingList(allowedSenders: AllowedSenders.RecipientsOnly);
        AddMailingListContact(mailingList, "Known Person", "known@email.com");
        AddMailingListContact(mailingList, "Some Person", "some@email.com");
        await Context.SaveChangesAsync();

        await this.handler.HandleAsync(new IncomingEmail
        {
            From = new() { Name = "User", Email = fromEmail },
            To = { new() { Email = MailingListAddress } },
            Subject = "Subject",
            HtmlContent = "<p>Hello</p>"
        });

        if (isAllowed)
        {
            var email = Assert.Single(this.emailService.AllSentEmails, e => e.To!.Address == "some@email.com");
            Assert.Equal(new EmailMessageAddress("User via Orchestrate", MailingListAddress), email.From);
            Assert.Equal(new EmailMessageAddress("Some Person", "some@email.com"), email.To);
            Assert.Contains("Subject", email.Subject);
        }
        else
        {
            var email = Assert.Single(this.emailService.AllSentEmails);
            Assert.Equal(this.emailService.DefaultFrom, email.From);
            Assert.Equal(new EmailMessageAddress("User", fromEmail), email.To);
            Assert.Contains("don't have permission", email.HtmlContent);
        }
    }

    [Theory]
    [InlineData(StandardRole.Administrator)]
    [InlineData(StandardRole.CommitteeMember)]
    public async Task UsersWithGlobalSendCanSendToRecipientOnlyLists(string role)
    {
        var userManager = GetService<UserManager<OrchestrateUser>>();
        var user = new OrchestrateUser { UserName = "user", Email = "user@email.com" };
        var result = await userManager.CreateAsync(user);
        Assert.True(result.Succeeded, string.Join(", ", result.Errors.Select(e => e.Description)));

        result = await userManager.AddToRoleAsync(user, role);
        Assert.True(result.Succeeded, string.Join(", ", result.Errors.Select(e => e.Description)));

        var mailingList = AddMailingList(allowedSenders: AllowedSenders.RecipientsOnly);
        AddMailingListContact(mailingList, "Some Person", "some@email.com");
        await Context.SaveChangesAsync();

        await this.handler.HandleAsync(new IncomingEmail
        {
            From = new() { Name = "User", Email = user.Email },
            To = { new() { Email = MailingListAddress } },
            Subject = "Subject",
        });

        var email = Assert.Single(this.emailService.AllSentEmails, e => e.To!.Address == "some@email.com");
        Assert.Equal(new EmailMessageAddress("User via Orchestrate", MailingListAddress), email.From);
        Assert.Equal(new EmailMessageAddress("Some Person", "some@email.com"), email.To);
        Assert.Contains("Subject", email.Subject);
    }

    [Fact]
    public async Task AllowedSenderEveryoneAllowsUnknownEmailAddressToSend()
    {
        var mailingList = AddMailingList(allowedSenders: AllowedSenders.Anyone);
        AddMailingListContact(mailingList, "Some Person", "some@email.com");
        await Context.SaveChangesAsync();

        await this.handler.HandleAsync(new IncomingEmail
        {
            From = new() { Name = "User", Email = "unknown@email.com" },
            To = { new() { Email = MailingListAddress } },
            Subject = "Subject",
        });

        var email = Assert.Single(this.emailService.AllSentEmails, e => e.To!.Address == "some@email.com");
        Assert.Equal(new EmailMessageAddress("User via Orchestrate", MailingListAddress), email.From);
        Assert.Equal(new EmailMessageAddress("Some Person", "some@email.com"), email.To);
        Assert.Contains("Subject", email.Subject);
    }

    [Fact]
    public async Task SentEmailHasCorrectValues()
    {
        var mailingList = AddMailingList(allowedSenders: AllowedSenders.Anyone);
        AddMailingListContact(mailingList, "Some Person", "contact@email.com");
        await Context.SaveChangesAsync();

        await this.handler.HandleAsync(new IncomingEmail
        {
            From = new() { Name = "User", Email = "user@email.com" },
            To = { new() { Email = MailingListAddress } },
            Subject = "Subject",
            TextContent = "Text",
            HtmlContent = "<p>Html</p>",
            Attachments = { new() { Name = "File.txt", MimeType = "text/plain", Base64Content = string.Empty } },
        });

        Assert.Single(this.emailService.AllSentEmails);
        Assert.NotNull(this.emailService.LastEmail);
        var email = this.emailService.LastEmail;

        Assert.Equal(new EmailMessageAddress("User via Orchestrate", MailingListAddress), email.From);
        Assert.Equal(new EmailMessageAddress("Some Person", "contact@email.com"), email.To);
        Assert.Equal("Subject", email.Subject);
        Assert.StartsWith("Text", email.TextContent);
        Assert.StartsWith("<p>Html</p>", email.HtmlContent);
        var attachment = Assert.Single(email.Attachments);
        Assert.Equal("File.txt", attachment.Name);
        Assert.Equal("text/plain", attachment.MimeType);
        Assert.Empty(attachment.Base64Content);
        Assert.Equal(MailingListEmailType, email.Type);
        Assert.NotNull(email.SendContext);
        Assert.Equal(OrchestrateSeed.GroupAbbr, email.SendContext.Tenant.Abbreviation);
        Assert.NotNull(email.SendContext.MailingList);
        Assert.Equal(mailingList.Slug, email.SendContext.MailingList.Slug);
        Assert.Equal(
            mailingList.Id.ToString(CultureInfo.InvariantCulture),
            email.Metadata[MailingListIdMetadataKey]);
        Assert.Equal(mailingList.Slug, email.Metadata[MailingListSlugMetadataKey]);
    }

    [Theory]
    [InlineData(ReplyStrategy.MailingList, "Test Mailing List", MailingListAddress)]
    [InlineData(ReplyStrategy.Sender, "User", "user@email.com")]
    [InlineData((ReplyStrategy)(-1), null, null)]
    public async Task ReplyToIsSetAppropriately(ReplyStrategy replyStrategy, string? replyToName, string? replyToAddress)
    {
        var mailingList = AddMailingList(replyTo: replyStrategy);
        AddMailingListContact(mailingList, "Some Person", "some@email.com");
        await Context.SaveChangesAsync();

        await this.handler.HandleAsync(new IncomingEmail
        {
            From = new() { Name = "User", Email = "user@email.com" },
            To = { new() { Email = MailingListAddress } },
            Subject = "Subject",
        });

        var email = Assert.Single(this.emailService.AllSentEmails, e => e.To!.Address == "some@email.com");
        Assert.Equal(new EmailMessageAddress("User via Orchestrate", MailingListAddress), email.From);
        Assert.Equal(new EmailMessageAddress("Some Person", "some@email.com"), email.To);

        if (replyToName != null && replyToAddress != null)
        {
            Assert.Equal(new EmailMessageAddress(replyToName, replyToAddress), email.ReplyTo);
        }
        else
        {
            Assert.Null(email.ReplyTo);
        }
    }

    [Fact]
    public async Task SenderIsntIncludedInRecipientList()
    {
        var mailingList = AddMailingList(allowedSenders: AllowedSenders.RecipientsOnly);
        AddMailingListContact(mailingList, "Known Person", "known@email.com");
        AddMailingListContact(mailingList, "Some Person", "some@email.com");
        await Context.SaveChangesAsync();

        await this.handler.HandleAsync(new IncomingEmail
        {
            From = new() { Name = "Known Person", Email = "known@email.com" },
            To = { new() { Email = MailingListAddress } },
            Subject = "Subject",
        });

        var email = Assert.Single(this.emailService.AllSentEmails);
        Assert.Equal(new EmailMessageAddress("Known Person via Orchestrate", MailingListAddress), email.From);
        Assert.Equal(new EmailMessageAddress("Some Person", "some@email.com"), email.To);
        Assert.Contains("Subject", email.Subject);
    }

    [Fact]
    public async Task WillHandleMultipleEmailsInToField()
    {
        var mailingList1 = AddMailingList();
        AddMailingListContact(mailingList1, "Some Person", "some@email.com");
        var mailingList2 = AddMailingList(name: "Test List 2", slug: "test-list2");
        AddMailingListContact(mailingList2, "Other Person", "other@email.com");
        await Context.SaveChangesAsync();

        var list2Email = $"{OrchestrateSeed.GroupAbbr}#test-list2@{ApexDomain}";
        await this.handler.HandleAsync(new IncomingEmail
        {
            From = new() { Name = "User", Email = "user@email.com" },
            To =
            {
                new() { Email = MailingListAddress },
                new() { Email = list2Email }
            },
            Subject = "Subject",
        });

        Assert.Equal(2, this.emailService.AllSentEmails.Count);

        var email = Assert.Single(this.emailService.AllSentEmails, e => e.To!.Address == "some@email.com");
        Assert.Equal(new EmailMessageAddress("User via Orchestrate", MailingListAddress), email.From);
        Assert.Equal("Subject", email.Subject);

        email = Assert.Single(this.emailService.AllSentEmails, e => e.To!.Address == "other@email.com");
        Assert.Equal(new EmailMessageAddress("User via Orchestrate", list2Email), email.From);
        Assert.Equal("Subject", email.Subject);
    }

    [Fact]
    public async Task DuplicateMailingListsWillOnlyBeSentOnce()
    {
        var mailingList1 = AddMailingList();
        AddMailingListContact(mailingList1, "Some Person", "some@email.com");
        await Context.SaveChangesAsync();

        await this.handler.HandleAsync(new IncomingEmail
        {
            From = new() { Name = "User", Email = "user@email.com" },
            To = { new() { Email = $"{OrchestrateSeed.GroupAbbr}#test-list@{ApexDomain}" } },
            Cc = { new() { Email = $"{OrchestrateSeed.GroupAbbr}#Test-List@{ApexDomain}" } },
            Bcc = { new() { Email = $"{OrchestrateSeed.GroupAbbr}#TEST-LIST@{ApexDomain}" } },
            Subject = "Subject",
        });

        var email = Assert.Single(this.emailService.AllSentEmails);
        Assert.Equal(new EmailMessageAddress("User via Orchestrate", MailingListAddress), email.From);
        Assert.Equal(new EmailMessageAddress("Some Person", "some@email.com"), email.To);
        Assert.Equal("Subject", email.Subject);
    }

    [Fact]
    public async Task OnlyActiveMembersAndContactsAreAddedToRecipientList()
    {
        var mailingList = AddMailingList();
        mailingList.Ensembles.Add(await Context.Ensemble.SingleAsync(e => e.Name == "Concert Band"));
        mailingList.Members.Add(await Context.Member.SingleAsync(e => e.Details!.Email == OrchestrateSeed.IggyPopEmail));
        mailingList.Contacts.Add(new Contact { Name = "active", Email = "active@e.com" });
        mailingList.Contacts.Add(
            new Contact { Name = "inactive", Email = "inactivate@e.com", ArchivedOn = DateTime.UtcNow.AddDays(-1) });
        await Context.SaveChangesAsync();

        await this.handler.HandleAsync(new IncomingEmail
        {
            From = new() { Name = "User", Email = "user@email.com" },
            To = { new() { Email = MailingListAddress } },
            Subject = "Subject",
        });

        Assert.Equal(2, this.emailService.AllSentEmails.Count);
        Assert.All(this.emailService.AllSentEmails, e => Assert.Equal("Subject", e.Subject));
        Assert.Single(this.emailService.AllSentEmails, e => e.To!.Address == OrchestrateSeed.BobMarleyEmail);
        Assert.Single(this.emailService.AllSentEmails, e => e.To!.Address == "active@e.com");
    }

    [Fact]
    public async Task RecipientListIsDeduplicated()
    {
        var mailingList = AddMailingList();
        mailingList.Ensembles.Add(await Context.Ensemble.SingleAsync(e => e.Name == "Concert Band"));
        mailingList.Members.Add(await Context.Member.SingleAsync(e => e.Details!.Email == OrchestrateSeed.BobMarleyEmail));
        AddMailingListContact(mailingList, "Bob Marley", OrchestrateSeed.BobMarleyEmail);
        await Context.SaveChangesAsync();

        await this.handler.HandleAsync(new IncomingEmail
        {
            From = new() { Name = "User", Email = "user@email.com" },
            To = { new() { Email = MailingListAddress } },
            Subject = "Subject",
        });

        var email = Assert.Single(this.emailService.AllSentEmails);
        Assert.Equal(OrchestrateSeed.BobMarleyEmail, email.To!.Address);
        Assert.Equal("Subject", email.Subject);
    }

    [InlineData("Plain Text", "<p>HTML<p/>")]
    [InlineData(null, "<p>HTML<p/>")]
    [InlineData("Plain Text", null)]
    [Theory]
    public async Task StandardFooterIsAddedToEmail(string? textContent, string? htmlContent)
    {
        var mailingList = AddMailingList();
        mailingList.Contacts.Add(new Contact { Name = "Test User", Email = "test@user.com" });
        await Context.SaveChangesAsync();

        await this.handler.HandleAsync(new IncomingEmail
        {
            From = new() { Name = "Other", Email = "other@email.com" },
            To = { new() { Email = MailingListAddress } },
            Subject = "Subject",
            TextContent = textContent ?? string.Empty,
            HtmlContent = htmlContent ?? string.Empty,
        });

        var email = Assert.Single(this.emailService.AllSentEmails);
        if (textContent is not null)
        {
            Assert.NotNull(email.TextContent);
            var split = email.TextContent.Split("\n\n---\n\n");
            Assert.Equal(2, split.Length);
            Assert.Equal(textContent + FooterSentinal, split[0]);
            Assert.Contains("because you are a member of", split[1]);
            Assert.Contains($"{UnsubscribeLinkStartSentinal}{UnsubscribeLinkEndSentinal}", split[1]);
        }
        if (htmlContent is not null)
        {
            Assert.NotNull(email.HtmlContent);
            var split = email.HtmlContent.Split("<hr/>");
            Assert.Equal(2, split.Length);
            Assert.Equal(htmlContent + FooterSentinal, split[0].Trim());
            Assert.Contains("because you are a member of", split[1]);
            Assert.Contains($"{UnsubscribeLinkHtmlEncodedStartSentinal}", split[1]);
            Assert.Contains($"{UnsubscribeLinkHtmlEncodedEndSentinal}", split[1]);
        }
    }

    [InlineData("Plain Text", "<p>HTML<p/>")]
    [InlineData(null, "<p>HTML<p/>")]
    [InlineData("Plain Text", null)]
    [Theory]
    public async Task CustomFooterIsAddedToEmail(string? textContent, string? htmlContent)
    {
        var mailingList = AddMailingList();
        mailingList.Footer = "Custom\nFooter";
        mailingList.Contacts.Add(new Contact { Name = "Test User", Email = "test@user.com" });
        await Context.SaveChangesAsync();

        await this.handler.HandleAsync(new IncomingEmail
        {
            From = new() { Name = "Other", Email = "other@email.com" },
            To = { new() { Email = MailingListAddress } },
            Subject = "Subject",
            TextContent = textContent ?? string.Empty,
            HtmlContent = htmlContent ?? string.Empty,
        });

        var email = Assert.Single(this.emailService.AllSentEmails);
        if (textContent is not null)
        {
            Assert.NotNull(email.TextContent);
            var split = email.TextContent.Split("\n\n---\n\n");
            Assert.Equal(3, split.Length);
            Assert.Equal(textContent + FooterSentinal, split[0]);
            Assert.Equal(mailingList.Footer, split[1]);
            Assert.Contains("because you are a member of", split[2]);
        }
        if (htmlContent is not null)
        {
            Assert.NotNull(email.HtmlContent);
            var split = email.HtmlContent.Split("<hr/>");
            Assert.Equal(3, split.Length);
            Assert.Equal(htmlContent + FooterSentinal, split[0].Trim());
            Assert.Equal("Custom<br/>Footer", split[1].Trim());
            Assert.Contains("because you are a member of", split[2]);
        }
    }

    [Fact]
    public async Task FooterIsntAddedIfIsAlreadyPresent()
    {
        var mailingList = AddMailingList();
        mailingList.Contacts.Add(new Contact { Name = "Test User", Email = "test@user.com" });
        await Context.SaveChangesAsync();

        await this.handler.HandleAsync(new IncomingEmail
        {
            From = new() { Name = "Other", Email = "other@email.com" },
            To = { new() { Email = MailingListAddress } },
            Subject = "Subject",
            TextContent = "Text Body" + FooterSentinal + "footer",
            HtmlContent = "<p>HTML</p>" + FooterSentinal + "footer",
        });

        var email = Assert.Single(this.emailService.AllSentEmails);
        Assert.Equal("Text Body" + FooterSentinal + "footer", email.TextContent);
        Assert.Equal("<p>HTML</p>" + FooterSentinal + "footer", email.HtmlContent);
    }

    [InlineData(MessageIdHeaderName, "<msg@mail.server>", MessageIdHeaderName)]
    [InlineData(MessageIdHeaderName, null, MessageIdHeaderName)]
    [InlineData("MESSAGE-ID", "<msg@mail.server>", MessageIdHeaderName)]
    [InlineData(InReplyToHeaderName, "<msg@mail.server>", InReplyToHeaderName)]
    [InlineData(InReplyToHeaderName, null, InReplyToHeaderName)]
    [InlineData("IN-REPLY-TO", "<msg@mail.server>", InReplyToHeaderName)]
    [InlineData(ReferencesHeaderName, "<msg@mail.server>", ReferencesHeaderName)]
    [InlineData(ReferencesHeaderName, "<msg@mail.server> <msg2@mail.server>", ReferencesHeaderName)]
    [InlineData(ReferencesHeaderName, null, ReferencesHeaderName)]
    [InlineData("REFERENCES", "<msg@mail.server>", ReferencesHeaderName)]
    [Theory]
    public async Task SelectHeadersArePropogatedToSentEmails(
        string sourceHeaderName, string? headerValue, string expectedHeaderName)
    {
        var mailingList = AddMailingList();
        mailingList.Contacts.Add(new Contact { Name = "Test User", Email = "test@user.com" });
        await Context.SaveChangesAsync();

        var headers = new List<IncomingEmailHeader>();
        if (headerValue is not null)
        {
            headers.Add(new(sourceHeaderName, headerValue));
        }

        await this.handler.HandleAsync(new IncomingEmail
        {
            From = new() { Name = "Other", Email = "other@email.com" },
            To = { new() { Email = MailingListAddress } },
            Headers = headers,
        });

        var email = Assert.Single(this.emailService.AllSentEmails);
        Assert.Equal(headerValue is not null, email.Headers.TryGetValue(expectedHeaderName, out var sentHeaderValue));
        Assert.Equal(headerValue, sentHeaderValue);
    }

    public static IEnumerable<object[]> SizeRejectedEmails()
    {
        return new List<IncomingEmail>
        {
            new()
            {
                From = new() { Name = "Big", Email = "big@text.com" },
                Subject = "Big Text",
                TextContent = new string('t', MaxBodySizeBytes + 1),
            },
            new()
            {
                From = new() { Name = "Big", Email = "big@html.com" },
                Subject = "Big Html",
                HtmlContent = new string('t', MaxBodySizeBytes + 1),
            },
            new()
            {
                From = new() { Name = "Big", Email = "big@attachment.com" },
                Subject = "Big Attachment",
                Attachments =
                {
                    new()
                    {
                        Name = string.Empty,
                        MimeType = string.Empty,
                        Base64Content = new string('b', MaxMessageSizeBytes+1),
                    }
                }
            },
        }.Select(e => new[] { e });
    }

    [Theory]
    [MemberData(nameof(SizeRejectedEmails))]
    public async Task EmailsThatAreTooBigAreRejected(IncomingEmail email)
    {
        await this.handler.HandleAsync(email);
        var sentEmail = Assert.Single(this.emailService.AllSentEmails);

        Assert.Contains(email.Subject, sentEmail.Subject);
        Assert.NotNull(sentEmail.To);
        Assert.Equal(email.From.Name, sentEmail.To.Name);
        Assert.Equal(email.From.Email, sentEmail.To.Address);
        Assert.Contains("too large", sentEmail.TextContent);
        Assert.Contains("too large", sentEmail.HtmlContent);
    }

    private MailingList AddMailingList(
        string name = "Test Mailing List",
        string slug = "test-list",
        AllowedSenders allowedSenders = AllowedSenders.Anyone,
        ReplyStrategy replyTo = ReplyStrategy.MailingList)
    {
        var entry = Context.Add(new MailingList
        {
            Name = name,
            Slug = slug,
            AllowedSenders = allowedSenders,
            ReplyTo = replyTo,
        });
        return entry.Entity;
    }

    private static void AddMailingListContact(MailingList list, string name, string address)
    {
        list.Contacts.Add(new Contact
        {
            Name = name,
            Email = address,
        });
    }
}
