using Microsoft.AspNetCore.Mvc;
using OrchestrateApi.Common;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.MailingLists;

namespace OrchestrateApi.Tests.Feature.MailingLists;

public class CreateMailingListDialogControllerTests : FullIntegrationTestHarness
{
    private readonly CreateMailingListDialogController controller;

    public CreateMailingListDialogControllerTests()
    {
        this.controller = ConstructController<CreateMailingListDialogController>();
    }

    [Fact]
    public async Task CorrectDataIsReturned()
    {
        var result = await this.controller.GetData();
        Assert.Equal(StatusCodes.Status200OK, result.GetStatusCode());

        Assert.Equal(OrchestrateSeed.GroupAbbr, result.Value!.AddressParameters.TenantSlug);
        Assert.Equal("test.domain.com", result.Value!.AddressParameters.Domain);
    }

    [Fact]
    public async Task MailingListIsCreated()
    {
        var response = await this.controller.CreateMailingList(new CreateMailingListDto
        {
            Name = "Mailing List",
            Slug = "mailing-list",
            AllowedSenders = AllowedSenders.RecipientsOnly,
            ReplyTo = ReplyStrategy.Sender,
            Footer = "Some footer",
        });
        var mailingListDto = response.Value;
        Assert.NotNull(mailingListDto);

        var mailingList = Context.MailingList.Single(e => e.Id == mailingListDto.Id);
        Assert.Equal("Mailing List", mailingList.Name);
        Assert.Equal("Some footer", mailingList.Footer);
    }

    [Fact]
    public async Task BadRequestIsReturnedIfTooManyAreTriedToBeCreated()
    {
        var configuration = GetService<IAppConfiguration>();
        for (var i = 0; i < configuration.MaxMailingListCount; i++)
        {
            Context.Add(new MailingList { Name = $"ML{i}", Slug = $"ml{i}" });
        }
        await Context.SaveChangesAsync();

        var response = await this.controller.CreateMailingList(new CreateMailingListDto
        {
            Name = "New",
            Slug = "new",
            AllowedSenders = AllowedSenders.RecipientsOnly,
            ReplyTo = ReplyStrategy.MailingList,
        });

        var result = Assert.IsType<BadRequestObjectResult>(response.Result);
        Assert.IsType<BasicErrorResponse>(result.Value);
    }
}
