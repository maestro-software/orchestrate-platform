using System.Text.Json;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.Assets;
using OrchestrateApi.Storage;

namespace OrchestrateApi.Tests.Feature.Assets;

public class AssetAttachmentStorageStrategyTests : AutoRollbackTestHarness
{
    private AssetAttachmentStorageStrategy StorageStrategy { get; } = new();

    [Fact]
    public void CorrectLinkingEntityIsCreated()
    {
        var linkingEntity = Assert.IsType<AssetAttachment>(StorageStrategy.CreateLinkingEntity(
            Guid.Empty,
            new EntityAttachmentLinkData { EntityId = 5 }));
        Assert.Equal(Guid.Empty, linkingEntity.BlobId);
        Assert.Equal(5, linkingEntity.AssetId);
    }

    [Fact]
    public async Task ReturnsCorrectLinkedBlobIds()
    {
        var (asset, blobId) = GenerateAssetAndAttachment(StorageStrategy.Name, "Searched");
        GenerateAssetAndAttachment(StorageStrategy.Name, "NotSearched");
        GenerateAssetAndAttachment("Other", null);
        await Context.SaveChangesAsync();

        var blobIds = await StorageStrategy
            .LinkedBlobIds(Context, new EntityAttachmentLinkData { EntityId = asset!.Id })
            .ToListAsync();
        Assert.Single(blobIds);
        Assert.Contains(blobIds, e => e == blobId);
    }

    [Fact]
    public async Task ReturnsCorrectValidBlobIds()
    {
        var (_, blobA) = GenerateAssetAndAttachment(StorageStrategy.Name, "A");
        var (_, blobB) = GenerateAssetAndAttachment(StorageStrategy.Name, "B");
        GenerateAssetAndAttachment("Other", null);
        await Context.SaveChangesAsync();

        var blobIds = await StorageStrategy.ValidBlobIds(Context).ToListAsync();
        Assert.Equal(2, blobIds.Count);
        Assert.Contains(blobIds, e => e == blobA);
        Assert.Contains(blobIds, e => e == blobB);
    }

    public (Asset?, Guid) GenerateAssetAndAttachment(string strategy, string? assetName)
    {
        var blob = Context.Add(new OrchestrateBlob
        {
            Name = "test.txt",
            MimeType = "text/plain",
            StorageStrategy = new OrchestrateBlobStorageStrategyData
            {
                Name = strategy,
                AccessData = JsonSerializer.SerializeToDocument(new EntityAttachmentAccessData
                {
                    Sensitivity = EntityAttachmentSensitivity.Secured
                }),
            }
        }).Entity;

        Asset? asset = null;
        if (assetName != null)
        {
            asset = Context.Add(new Asset { Description = assetName }).Entity;
            Context.Add(new AssetAttachment { Asset = asset, BlobId = blob.Id });
        }

        return (asset, blob.Id);
    }
}
