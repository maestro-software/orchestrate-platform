using OrchestrateApi.DAL.Model;
using OrchestrateApi.PaymentProcessor;
using OrchestrateApi.Platform;

namespace OrchestrateApi.Tests.PaymentProcessor;

public class PaymentProcessorServiceTests : AutoRollbackTestHarness
{
    private readonly PaymentProcessorService service;
    private readonly MockEmailService emailService;

    public PaymentProcessorServiceTests()
    {
        this.emailService = new MockEmailService();
        this.service = new PaymentProcessorService(
            Context,
            this.emailService,
            new FrontEnd(new MockAppConfiguration { FrontEndUri = new Uri("https://orchestrate.community/") }),
            TestHelper.BuildMockLogger<PaymentProcessorService>());
    }

    [Theory]
    [InlineData(PlanType.Trial, -1, true)]
    [InlineData(PlanType.Trial, 1, false)]
    [InlineData(PlanType.Medium, -1, false)]
    public async Task OnlyExpiredTrialsAreMovedToFreePlan(PlanType plan, int endsInDays, bool becomesFree)
    {
        var tenant = new Tenant
        {
            Name = "Test",
            Abbreviation = "test",
            ContactAddress = "test@email.com",
            Plan =
            {
                Type = plan,
                PeriodEnd = DateTime.UtcNow.AddDays(endsInDays),
            }
        };
        Context.Add(tenant);
        await Context.SaveChangesAsync();

        await this.service.TransitionExpiredTrials();

        if (becomesFree)
        {
            Assert.Equal(PlanType.Free, tenant.Plan.Type);
            Assert.Null(tenant.Plan.PeriodEnd);
            Assert.NotNull(this.emailService.LastEmail);
            Assert.Equal("Test", this.emailService.LastEmail.To?.Name);
            Assert.Equal("test@email.com", this.emailService.LastEmail.To?.Address);
            Assert.Contains("Test", this.emailService.LastEmail.HtmlContent);
            Assert.Contains("/group/test/settings;section=subscription", this.emailService.LastEmail.HtmlContent);
        }
        else
        {
            Assert.Equal(plan, tenant.Plan.Type);
            Assert.Equal(DateTime.UtcNow.AddDays(endsInDays), tenant.Plan.PeriodEnd.Value, TimeSpan.FromSeconds(1));
            Assert.Null(this.emailService.LastEmail);
        }
    }
}
