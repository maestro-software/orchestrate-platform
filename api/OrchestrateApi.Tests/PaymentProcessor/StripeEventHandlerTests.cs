using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.PaymentProcessor;
using Stripe;

namespace OrchestrateApi.Tests.PaymentProcessor;

public class StripeEventHandlerTests : AutoRollbackTestHarness
{
    private readonly StripeEventHandler eventHandler;
    private readonly MockPaymentProcessor paymentProcessor;

    public StripeEventHandlerTests()
    {
        this.paymentProcessor = new MockPaymentProcessor();
        this.eventHandler = new StripeEventHandler(
            Context, this.paymentProcessor, TestHelper.BuildMockLogger<StripeEventHandler>());
    }

    [Fact]
    public async Task SubscriptionCreatedSetsPlanTypeAndPeriodEnd()
    {
        var tenant = await CommonSetup();

        var @event = BuildEvent(EventTypes.CustomerSubscriptionCreated, new Subscription
        {
            Id = MockPaymentProcessor.MockSubscriptionId,
            CustomerId = MockPaymentProcessor.MockCustomerId,
            CurrentPeriodEnd = new DateTime(2023, 2, 28, 0, 0, 0, DateTimeKind.Utc),
            Items = new StripeList<SubscriptionItem>
            {
                Data = new List<SubscriptionItem>
                {
                    new SubscriptionItem
                    {
                        Price = new Price {ProductId = $"prod_{PlanType.Medium}"},
                    }
                }
            },
        });
        await this.eventHandler.HandleEvent(@event);

        Assert.Equal(PlanType.Medium, tenant.Plan.Type);
        Assert.Equal(new DateTime(2023, 2, 28, 0, 0, 0, DateTimeKind.Utc), tenant.Plan.PeriodEnd);
    }

    [Theory]
    [InlineData(SubscriptionStatuses.Active, true)]
    [InlineData(SubscriptionStatuses.Canceled, false)]
    public async Task InvoicePaidUpdatesPeriodEndIfSubscriptionActive(string subscriptionStatus, bool periodIsUpdated)
    {
        var tenant = await CommonSetup();
        this.paymentProcessor.Subscription = new Subscription
        {
            Id = MockPaymentProcessor.MockSubscriptionId,
            CustomerId = MockPaymentProcessor.MockCustomerId,
            Status = subscriptionStatus,
            CurrentPeriodEnd = new DateTime(2023, 2, 28, 0, 0, 0, DateTimeKind.Utc),
            Items = new StripeList<SubscriptionItem>
            {
                Data = new List<SubscriptionItem>
                {
                    new SubscriptionItem
                    {
                        Price = new Price {ProductId = $"prod_{PlanType.Medium}"},
                    }
                }
            },
        };

        var @event = BuildEvent(EventTypes.InvoicePaid, new Invoice
        {
            CustomerId = MockPaymentProcessor.MockCustomerId,
            SubscriptionId = MockPaymentProcessor.MockSubscriptionId,
        });
        await this.eventHandler.HandleEvent(@event);

        if (periodIsUpdated)
        {
            Assert.Equal(new DateTime(2023, 2, 28, 0, 0, 0, DateTimeKind.Utc), tenant.Plan.PeriodEnd);
        }
        else
        {
            Assert.Null(tenant.Plan.PeriodEnd);
        }
    }

    [Fact]
    public async Task SubscriptionUpdatedSetsPlanType()
    {
        var tenant = await CommonSetup();

        var @event = BuildEvent(EventTypes.SubscriptionScheduleUpdated, new Subscription
        {
            Id = MockPaymentProcessor.MockSubscriptionId,
            CustomerId = MockPaymentProcessor.MockCustomerId,
            CurrentPeriodEnd = new DateTime(2023, 2, 28, 0, 0, 0, DateTimeKind.Utc),
            Items = new StripeList<SubscriptionItem>
            {
                Data = new List<SubscriptionItem>
                {
                    new SubscriptionItem
                    {
                        Price = new Price {ProductId = $"prod_{PlanType.Medium}"},
                    }
                }
            },
        });
        await this.eventHandler.HandleEvent(@event);

        Assert.Equal(PlanType.Medium, tenant.Plan.Type);
        Assert.Null(tenant.Plan.PeriodEnd);
    }

    [Fact]
    public async Task SubscriptionCancelledSetsPlanTypeToFree()
    {
        var tenant = await CommonSetup();

        var @event = BuildEvent(EventTypes.SubscriptionScheduleCanceled, new Subscription
        {
            Id = MockPaymentProcessor.MockSubscriptionId,
            CustomerId = MockPaymentProcessor.MockCustomerId,
            CurrentPeriodEnd = new DateTime(2023, 2, 28, 0, 0, 0, DateTimeKind.Utc),
            Items = new StripeList<SubscriptionItem>
            {
                Data = new List<SubscriptionItem>
                {
                    new SubscriptionItem
                    {
                        Price = new Price {ProductId = $"prod_{PlanType.Medium}"},
                    }
                }
            },
        });
        await this.eventHandler.HandleEvent(@event);

        Assert.Equal(PlanType.Free, tenant.Plan.Type);
        Assert.Null(tenant.Plan.PeriodEnd);
    }

    private async Task<Tenant> CommonSetup()
    {
        var tenant = await Context.Tenant.SingleAsync();
        tenant.Plan.StripeCustomerId = MockPaymentProcessor.MockCustomerId;
        await Context.SaveChangesAsync();

        return tenant;
    }

    private static Event BuildEvent(string type, IHasObject obj)
    {
        return new Event
        {
            Type = type,
            Data = new EventData
            {
                Object = obj,
            },
        };
    }
}
