using OrchestrateApi.Common;

namespace OrchestrateApi.Tests.Common
{
    public class StringExtensionsTests
    {
        [Theory]
        [InlineData("PascalCaseToLower", "pascal_case_to_lower")]
        [InlineData("camelCaseToLower", "camel_case_to_lower")]
        [InlineData("__withLeadingUnderscores", "__with_leading_underscores")]
        [InlineData("", "")]
        [InlineData(null, null)]
        public void ToSnakeCaseWorksAsExpected(string? input, string? expected)
        {
            Assert.Equal(expected, input.ToSnakeCase());
        }

        [Theory]
        [InlineData("PascalCaseToLower", "pascalCaseToLower")]
        [InlineData("camelCaseToLower", "camelCaseToLower")]
        [InlineData("__withLeadingUnderscores", "__withLeadingUnderscores")]
        [InlineData("", "")]
        [InlineData(null, null)]
        public void ToCamelCaseWorksAsExpected(string? input, string? expected)
        {
            Assert.Equal(expected, input!.ToCamelCase());
        }

        [Theory]
        [InlineData("", "")]
        [InlineData("Hello", "Hello")]
        [InlineData("sixcha", "sixcha")]
        [InlineData("sevchar", "sevchar")]
        [InlineData("eight chars", "eight c...")]
        public void ShortenWorksAsExpected(string input, string expected)
        {
            Assert.Equal(expected, input.Shorten(7, "..."));
        }
    }
}
