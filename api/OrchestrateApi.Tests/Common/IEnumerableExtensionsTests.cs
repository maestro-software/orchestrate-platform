using OrchestrateApi.Common;

namespace OrchestrateApi.Tests.Common
{
    public class IEnumerableExtensionsTests
    {
        [Theory]
        [InlineData(0, 2, 4, 6)]
        [InlineData(1, 0, 1, 2)]
        [InlineData(2, 1, 2, 3, 4)]
        [InlineData(3, 1, 3, 5)]
        public void WhereWithDiscardCountActionCallsWithCorrectNumber(int expectedNumDiscarded, params int[] values)
        {
            var called = false;
            values.WhereWithDiscardCountAction(v => v % 2 == 0, numDiscarded =>
            {
                called = true;
                Assert.Equal(expectedNumDiscarded, numDiscarded);
            });

            Assert.Equal(expectedNumDiscarded > 0, called);
        }

        [Fact]
        public void ToDataUrlDetectsAndPopulatesMimeTypeCorrectly()
        {
            var png = (SentinelFileType)FileType.Png;
            var image = png.Sentinel.Concat(new byte[] { 0xCA, 0xFE, 0xB0, 0x0B }).ToArray();
            Assert.Equal("data:image/png;base64,iVBORw0KGgrK/rAL", image.ToDataUrl());
        }

        [Fact]
        public void WhereNotNullFiltersAsExpected()
        {
            Assert.True(new[] { new Ref(1), new Ref(2), null, new Ref(3) }
                .WhereNotNull()
                .SequenceEqual(new[] { new Ref(1), new Ref(2), new Ref(3) }));
        }

        private sealed record class Ref(int Prop);
    }
}
