using OrchestrateApi.Common;

namespace OrchestrateApi.Tests.Common
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("", "CA1812")]
    public class BackgroundTaskQueueTests
    {
        [Fact]
        public async Task WillCorrectlyPassDataAndResolveServices()
        {
            using var integrationEnvironment = new IntegrationEnvironment(services =>
            {
                services.AddTransient<TestTransientService>();
                services.AddSingleton<BackgroundTaskHostedService>();
            });
            var backgroundTaskQueue = integrationEnvironment.GetService<IBackgroundTaskQueue>();
            var backgroundTaskHostedService = integrationEnvironment.GetService<BackgroundTaskHostedService>();

            var testWorkItem = new TestWorkItem { AssertServiceTypeExists = typeof(TestTransientService) };
            await backgroundTaskQueue.QueueAsync(testWorkItem);

            Assert.False(testWorkItem.WasCalled);

            // Don't await this or we'll get a deadlock
            _ = backgroundTaskHostedService.StartAsync(CancellationToken.None);

            if (await Task.WhenAny(testWorkItem.HasBeenCalledTask, Task.Delay(1000)) == testWorkItem.HasBeenCalledTask)
            {
                Assert.True(testWorkItem.WasCalled);
            }
            else
            {
                Assert.Fail("Timed out waiting for background work item to complete");
            }
        }

        private sealed class TestTransientService { }

        private sealed class TestWorkItem : IBackgroundWorkItem
        {
            public bool WasCalled { get; private set; }
            public required Type AssertServiceTypeExists { get; set; }
            public Task HasBeenCalledTask => HasBeenCalled.Task;
            private TaskCompletionSource<bool> HasBeenCalled { get; } = new TaskCompletionSource<bool>();

            public Task DoWork(IServiceProvider services, CancellationToken cancellationToken)
            {
                WasCalled = true;
                Assert.NotNull(services.GetService(AssertServiceTypeExists));
                HasBeenCalled.SetResult(true);
                return HasBeenCalledTask;
            }
        }
    }
}
