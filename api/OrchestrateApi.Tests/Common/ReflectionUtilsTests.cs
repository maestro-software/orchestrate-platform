using OrchestrateApi.Common;

namespace OrchestrateApi.Tests.Common
{
    public class ReflectionUtilsTests
    {
        [Fact]
        public void GetMethodInfoReturnsPlainAndGenericMethodInfos()
        {
            var t = new TestClass();
            var methodInfo = ReflectionUtils.GetMethodInfo(() => t.PlainMethod());
            Assert.Same(typeof(TestClass).GetMethod("PlainMethod"), methodInfo);

            methodInfo = ReflectionUtils.GetMethodInfo(() => t.GenericMethod<int>());
            Assert.Same(
                typeof(TestClass).GetMethod("GenericMethod")!.MakeGenericMethod(typeof(int)),
                methodInfo);
        }

        [Fact]
        public void GetGenericMethodInfoReturnsGenericMethodInfo()
        {
            var t = new TestClass();
            var methodInfo = ReflectionUtils.GetGenericMethodInfo(() => t.GenericMethod<int>());
            Assert.Same(typeof(TestClass).GetMethod("GenericMethod"), methodInfo);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("", "CA1822")]
        private sealed class TestClass
        {
            public void PlainMethod() { }
            public T? GenericMethod<T>() { return default; }
        }
    }
}
