using System.Text.Json;
using OrchestrateApi.Common;

namespace OrchestrateApi.Tests.Common;

public class JsonDocumentConverterTests
{
    public const string Json = """
        [
            {
                "SomeProp": "Value"
            },
            {
                "NestedProp": {
                    "Property": 1
                }
            }
        ]
    """;

    private JsonSerializerOptions JsonOptions { get; } = new()
    {
        Converters = { new JsonDocumentConverter() },
    };

    [Fact]
    public void WritingWithoutNamingPolicyMakesNoChanges()
    {
        var document = JsonDocument.Parse(Json);

        var writtenDocument = JsonSerializer.Serialize(document, JsonOptions);

        Assert.NotNull(writtenDocument);
        Assert.Contains("SomeProp", writtenDocument);
        Assert.DoesNotContain("someProp", writtenDocument);
        Assert.Contains("Value", writtenDocument);
        Assert.DoesNotContain("value", writtenDocument);
        Assert.Contains("NestedProp", writtenDocument);
        Assert.DoesNotContain("nestedProp", writtenDocument);
        Assert.Contains("Property", writtenDocument);
        Assert.DoesNotContain("property", writtenDocument);
    }

    [Fact]
    public void WritingWithCamelcaseNamingPolicyMakesChangesPropertyNames()
    {
        var document = JsonDocument.Parse(Json);
        JsonOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;

        var writtenDocument = JsonSerializer.Serialize(document, JsonOptions);

        Assert.NotNull(writtenDocument);
        Assert.DoesNotContain("SomeProp", writtenDocument);
        Assert.Contains("someProp", writtenDocument);
        Assert.Contains("Value", writtenDocument);
        Assert.DoesNotContain("value", writtenDocument);
        Assert.DoesNotContain("NestedProp", writtenDocument);
        Assert.Contains("nestedProp", writtenDocument);
        Assert.DoesNotContain("Property", writtenDocument);
        Assert.Contains("property", writtenDocument);
    }
}
