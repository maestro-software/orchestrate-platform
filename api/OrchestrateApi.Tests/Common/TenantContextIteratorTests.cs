using OrchestrateApi.Common;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Tests.Common;

public class TenantContextIteratorTests : AutoRollbackTestHarness
{
    private readonly TenantContextIterator iterator;

    public TenantContextIteratorTests()
    {
        this.iterator = new TenantContextIterator(
            Context,
            TenantService,
            TestHelper.BuildMockLogger<TenantContextIterator>());
    }

    [Fact]
    public async Task InCorrectTenantScope()
    {
        var tenants = Enumerable.Range(0, 100)
            .Select(i => new Tenant { Name = $"Tenant{i}", Abbreviation = $"tenant{i}" })
            .ToList();
        Context.AddRange(tenants);
        await Context.SaveChangesAsync();

        var seenTenants = new HashSet<Guid>();
        await this.iterator.ForEachTenant("test", (tenantContext) =>
        {
            seenTenants.Add(tenantContext.Id);
            Assert.Equal(tenantContext.Id, TenantService.TenantId);

            if (tenantContext.Id == OrchestrateSeed.TenantId)
            {
                throw new InvalidOperationException("Shouldn't stop execution");
            }

            return Task.CompletedTask;
        });

        var expectedTenants = tenants.Select(e => e.Id).Append(OrchestrateSeed.TenantId).ToHashSet();
        Assert.Equal(expectedTenants, seenTenants);
    }
}
