using System.Text;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Storage;

namespace OrchestrateApi.Tests.Storage;

public class StorageControllerTests : FullIntegrationTestHarness
{
    public StorageControllerTests()
    {
        StorageController = new StorageController(
            Context,
            GetService<ITenantService>(),
            GetService<StorageService>(),
            TestHelper.BuildMockLogger<StorageController>());
    }

    private StorageController StorageController { get; }
    private StorageService StorageService => GetService<StorageService>();
    private MockStorageProvider StorageProvider => GetServiceAs<IStorageProvider, MockStorageProvider>();

    [Theory]
    [InlineData("Unknown", """ null """, null, false)]
    [InlineData("Mock", """ {"Invalid": true } """, null, false)]
    [InlineData("Mock", """ {"CanRead": 1, "CanRead": 1} """, null, false)]
    [InlineData("Mock", """ {"canRead": true, "canWrite": true} """, null, true)]
    [InlineData("Mock", """ {"CanRead": true, "CanWrite": true} """, null, true)]
    [InlineData("Mock", """ {"Extra": true, "CanRead": true, "CanWrite": true} """, null, true)]
    [InlineData("MockLink", """ {"CanRead": true, "CanWrite": true} """, """ {"Invalid": true} """, false)]
    [InlineData("MockLink", """ {"CanRead": true, "CanWrite": true} """, """ {"nameContains": true} """, false)]
    [InlineData("MockLink", """ {"CanRead": true, "CanWrite": true} """, """ {"NameContains": 1} """, false)]
    [InlineData("MockLink", """ {"CanRead": true, "CanWrite": true} """, """ {"nameContains": "1"} """, true)]
    [InlineData("MockLink", """ {"CanRead": true, "CanWrite": true} """, """ {"NameContains": "1"} """, true)]
    public async Task InvalidStorageStrategyOrAccessDataOnUploadReturnsBadRequest(
        string strategyName, string accessData, string? linkData, bool isValid)
    {
        using var ms = new MemoryStream(Encoding.UTF8.GetBytes("Test content"));
        StorageController.ControllerContext = ContextWithFile("test.txt", ms);

        var response = await StorageController.UploadBlobAsync(new UploadBlobDto
        {
            StorageStrategyName = strategyName,
            AccessData = accessData,
            LinkData = linkData,
        }, default);

        if (isValid)
        {
            Assert.NotNull(response.Value);
            Assert.Equal("test.txt", response.Value.Single().Name);
        }
        else
        {
            Assert.IsType<BadRequestObjectResult>(response.Result);
        }
    }

    [Fact]
    public async Task ForbiddenIsReturnedForStorageStrategyWithNoWritePermission()
    {
        var response = await StorageController.UploadBlobAsync(new UploadBlobDto
        {
            StorageStrategyName = "Mock",
            AccessData = JsonSerializer.Serialize(new MockAccessData(true, false)),
            LinkData = null,
        }, default);
        Assert.IsType<ForbidResult>(response.Result);
    }

    [Theory]
    [InlineData("valid.txt", true)]
    [InlineData("invalid.txt", false)]
    public async Task BadRequestIsReturnedForInvalidFile(string filename, bool isValid)
    {
        using var ms = new MemoryStream(Encoding.UTF8.GetBytes("Test content"));
        StorageController.ControllerContext = ContextWithFile(filename, ms);

        var response = await StorageController.UploadBlobAsync(new UploadBlobDto
        {
            StorageStrategyName = "MockValidating",
            AccessData = JsonSerializer.Serialize(new MockAccessData(true, true)),
        }, default);

        if (isValid)
        {
            Assert.NotNull(response.Value);
            Assert.Equal(filename, response.Value.Single().Name);
        }
        else
        {
            Assert.IsType<BadRequestObjectResult>(response.Result);
        }
    }

    [Theory]
    [InlineData(null, "abc", "cde", "efg")]
    [InlineData("c", "abc", "cde")]
    [InlineData("e", "cde", "efg")]
    [InlineData("h")]
    public async Task ListingBlobsByStorageStrategyWithoutLinkDataFiltersAsExpected(string? nameFilter, params string[] expectedFiles)
    {
        static OrchestrateBlob CreateBlob(string name) => new OrchestrateBlob
        {
            MimeType = "text/plain",
            Name = name,
            StorageStrategy = new()
            {
                Name = "MockLink",
                AccessData = JsonSerializer.SerializeToDocument(new MockAccessData(true, true)),
            },
        };

        Context.Add(CreateBlob("abc"));
        Context.Add(CreateBlob("cde"));
        Context.Add(CreateBlob("efg"));
        await Context.SaveChangesAsync();

        var response = await StorageController.ListBlobsAsync(new ListBlobsDto
        {
            StorageStrategyName = "MockLink",
            LinkData = nameFilter != null ? JsonSerializer.Serialize(new MockLinkData(nameFilter)) : null,
        }, default);

        Assert.NotNull(response.Value);
        var list = response.Value.ToList();
        Assert.Equal(expectedFiles.Length, list.Count);
        foreach (var expected in expectedFiles)
        {
            Assert.Contains(list, e => e.Name == expected);
        }
    }

    [Theory]
    [InlineData(null)]
    [InlineData(false)]
    [InlineData(true)]
    public async Task FileContentIsReturnedOnlyIfUserHasPermission(bool? canRead)
    {
        var blobId = Guid.NewGuid();
        if (canRead.HasValue)
        {
            var blob = await StorageService.CreateBlobAsync(
                new MockUploadedBlob("test.txt", "text/plain", "Test contents"),
                new StorageStrategyDto("Mock", new MockAccessData(canRead.Value, false)));
            blobId = blob.Id;

            // Ensure our below read actually returns the contents
            Assert.Equal(Encoding.UTF8.GetBytes("Test contents"), StorageProvider.WrittenData);
            StorageProvider.ReadData = StorageProvider.WrittenData!;
        }

        var response = await StorageController.GetBlobContentAsync(blobId, default);

        if (canRead.GetValueOrDefault(false))
        {
            var result = (FileStreamResult)response;
            Assert.Equal("test.txt", result.FileDownloadName);
            Assert.Equal("text/plain", result.ContentType);
            using var fileContents = new MemoryStream();
            await result.FileStream.CopyToAsync(fileContents);
            Assert.Equal("Test contents", Encoding.UTF8.GetString(fileContents.ToArray()));
        }
        else
        {
            Assert.IsType<NotFoundResult>(response);
        }
    }

    [Fact]
    public async Task UnknownOrUnwritableBlobIdReturnsNotFoundWhenUpdatingAccessData()
    {
        Assert.IsType<NotFoundResult>(await StorageController.UpdateAccessDataAsync(
            Guid.NewGuid(),
            JsonSerializer.SerializeToDocument(new MockAccessData(true, true)),
            default));

        var blob = await StorageService.CreateBlobAsync(
            new MockUploadedBlob("test.txt", "text/plain", "Test contents"),
            new StorageStrategyDto("Mock", new MockAccessData(false, false)));
        Assert.IsType<NotFoundResult>(await StorageController.UpdateAccessDataAsync(
            blob.Id,
            JsonSerializer.SerializeToDocument(new MockAccessData(true, true)),
            default));
    }

    [Fact]
    public async Task AccessDataIsValidated()
    {
        var blob = await StorageService.CreateBlobAsync(
            new MockUploadedBlob("test.txt", "text/plain", "Test contents"),
            new StorageStrategyDto("Mock", new MockAccessData(true, true)));

        Assert.IsType<BadRequestObjectResult>(await StorageController.UpdateAccessDataAsync(
            blob.Id,
            JsonDocument.Parse(""" {"Invalid": true} """),
            default));
    }

    [Fact]
    public async Task UpdateToNoWriteAccessIsBlocked()
    {
        var blob = await StorageService.CreateBlobAsync(
            new MockUploadedBlob("test.txt", "text/plain", "Test contents"),
            new StorageStrategyDto("Mock", new MockAccessData(true, true)));

        Assert.IsType<ForbidResult>(await StorageController.UpdateAccessDataAsync(
            blob.Id,
            JsonDocument.Parse(""" {"CanRead": true, "CanWrite": false} """),
            default));
    }

    [Fact]
    public async Task UpdateAccessDataDoesWhatItSaysOnTheBox()
    {
        var blob = await StorageService.CreateBlobAsync(
            new MockUploadedBlob("test.txt", "text/plain", "Test contents"),
            new StorageStrategyDto("Mock", new MockAccessData(true, true)));

        Assert.IsType<OkResult>(await StorageController.UpdateAccessDataAsync(
            blob.Id,
            JsonDocument.Parse(""" {"CanRead": false, "CanWrite": true} """),
            default));

        var accessData = await Context.Blobs.Where(e => e.Id == blob.Id).Select(e => e.StorageStrategy.AccessData).SingleAsync();
        Assert.Equal(
            JsonSerializer.Deserialize<MockAccessData>(accessData),
            JsonSerializer.Deserialize<MockAccessData>(""" {"CanRead": false, "CanWrite": true} """));
    }

    [Theory]
    [InlineData(null)]
    [InlineData(false)]
    [InlineData(true)]
    public async Task BlobIsOnlyDeletedIfUserHasWritePermission(bool? canWrite)
    {
        var blobId = Guid.NewGuid();

        if (canWrite.HasValue)
        {
            var blob = await StorageService.CreateBlobAsync(
                new MockUploadedBlob("test.txt", "text/plain", "Test contents"),
                new StorageStrategyDto("Mock", new MockAccessData(true, canWrite.Value)));
            blobId = blob.Id;
        }

        var response = await StorageController.DeleteBlobAsync(blobId, default);

        if (canWrite.GetValueOrDefault(false))
        {
            Assert.IsType<OkResult>(response);
        }
        else
        {
            Assert.IsType<NotFoundResult>(response);
        }
    }

    private static ControllerContext ContextWithFile(string fileName, MemoryStream contents) => new ControllerContext
    {
        HttpContext = new DefaultHttpContext
        {
            Request = {
                Form = new FormCollection(null, new FormFileCollection
                {
                    new FormFile(contents, 0, contents.Length, "file0", fileName)
                    {
                        Headers = new HeaderDictionary(),
                        ContentType = "text/plain",
                    },
                })
            }
        }
    };
}
