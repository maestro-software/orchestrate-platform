using System.Security.Claims;
using System.Text;
using System.Text.Json;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Storage;

namespace OrchestrateApi.Tests.Storage;

public class StorageServiceTests : AutoRollbackTestHarness
{
    public StorageServiceTests()
    {
        StorageProvider = new MockStorageProvider();
        StorageStrategy = new MockStorageStrategy();
        StorageService = new StorageService(
            TenantService,
            Context,
            StorageProvider,
            new[] { StorageStrategy },
            TestHelper.BuildMockLogger<StorageService>());
    }

    private StorageService StorageService { get; }
    private MockStorageProvider StorageProvider { get; }
    private MockStorageStrategy StorageStrategy { get; }
    private ClaimsPrincipal User { get; } = new ClaimsPrincipal();

    [Fact]
    public async Task GetReadableBlobReturnsNullOrBlobDependingOnAccess()
    {
        var entry = Context.Add(CreateBlob(canRead: false));
        await Context.SaveChangesAsync();
        Assert.Null(await StorageService.GetReadableBlobAsync(User, entry.Entity.Id));

        entry = Context.Add(CreateBlob(canRead: true));
        await Context.SaveChangesAsync();
        Assert.Same(entry.Entity, await StorageService.GetReadableBlobAsync(User, entry.Entity.Id));
    }

    [Fact]
    public async Task GetReadableBlobsReturnsBlobsWithAccess()
    {
        var entry1 = Context.Add(CreateBlob(canRead: true));
        var entry2 = Context.Add(CreateBlob(canRead: false));
        var entry3 = Context.Add(CreateBlob(canRead: true));
        await Context.SaveChangesAsync();

        var blobs = await StorageService.GetReadableBlobsAsync(User, new[] { entry1.Entity.Id, entry2.Entity.Id, entry3.Entity.Id });
        Assert.Equal(2, blobs.Count);
        Assert.Contains(entry1.Entity, blobs);
        Assert.Contains(entry3.Entity, blobs);
    }

    [Fact]
    public async Task GetReadableBlobsReturnsEmptyArrayForNoAccess()
    {
        var entry1 = Context.Add(CreateBlob(canRead: false));
        var entry2 = Context.Add(CreateBlob(canRead: false));
        await Context.SaveChangesAsync();

        var blobs = await StorageService.GetReadableBlobsAsync(User, new[] { entry1.Entity.Id, entry2.Entity.Id });
        Assert.Empty(blobs);
    }

    [Fact]
    public async Task GetWritableBlobReturnsNullOrBlobDependingOnAccess()
    {
        var entry = Context.Add(CreateBlob(canWrite: false));
        await Context.SaveChangesAsync();
        Assert.Null(await StorageService.GetWritableBlobAsync(User, entry.Entity.Id));

        entry = Context.Add(CreateBlob(canWrite: true));
        await Context.SaveChangesAsync();
        Assert.Same(entry.Entity, await StorageService.GetWritableBlobAsync(User, entry.Entity.Id));
    }

    [Fact]
    public async Task InvalidStorageStrategyOrAccessDataThrowsOnUpdateStorageStrategy()
    {
        var entry = Context.Add(CreateBlob());

        await Assert.ThrowsAsync<InvalidOperationException>(() => StorageService.UpdateBlobStorageStrategyAsync(
            entry.Entity,
            new StorageStrategyDto("Unknown", new MockAccessData(true, true))));
        await Assert.ThrowsAsync<InvalidOperationException>(() => StorageService.UpdateBlobStorageStrategyAsync(
            entry.Entity,
            new StorageStrategyDto(StorageStrategy.Name, null)));
    }

    [Fact]
    public async Task ReadBlobReturnsDataFromProvider()
    {
        var blob = CreateBlob();
        StorageProvider.ReadData = Encoding.UTF8.GetBytes("Hello");
        using var data = await StorageService.ReadBlobAsync(blob);
        Assert.NotNull(data);
        using var reader = new StreamReader(data.Stream);
        Assert.Equal("Hello", await reader.ReadToEndAsync());
    }

    [Fact]
    public async Task CreateBlobCreateRowAndWritesToProvider()
    {
        var upload = new MockUploadedBlob("test.txt", "text/plain", "Some contents");
        var blob = await StorageService.CreateBlobAsync(
            upload,
            new StorageStrategyDto(StorageStrategy.Name, new MockAccessData(true, true)));

        Assert.Equal("test.txt", blob.Name);
        Assert.Equal("text/plain", blob.MimeType);
        Assert.Equal(DateTime.UtcNow, blob.Created, TimeSpan.FromSeconds(1));
        Assert.Equal(DateTime.UtcNow, blob.Modified, TimeSpan.FromSeconds(1));
        Assert.Equal(StorageStrategy.Name, blob.StorageStrategy.Name);
        Assert.Equal(new MockAccessData(true, true), JsonSerializer.Deserialize<MockAccessData>(blob.StorageStrategy.AccessData));
        Assert.Equal(Encoding.UTF8.GetBytes("Some contents"), StorageProvider.WrittenData);
    }

    [Fact]
    public async Task FailedWriteByProviderThrows()
    {
        StorageProvider.WritesSuccessfully = false;
        var upload = new MockUploadedBlob("test.txt", "text/plain", "Some contents");
        await Assert.ThrowsAsync<InvalidOperationException>(() => StorageService.CreateBlobAsync(
            upload,
            new StorageStrategyDto(StorageStrategy.Name, new MockAccessData(true, true))));
    }

    [Fact]
    public async Task InvalidStorageStrategyOrAccessDataThrowsOnCreate()
    {
        var upload = new MockUploadedBlob("", "", "");

        await Assert.ThrowsAsync<InvalidOperationException>(() => StorageService.CreateBlobAsync(
            upload,
            new StorageStrategyDto("Unknown", new MockAccessData(true, true))));
        await Assert.ThrowsAsync<InvalidOperationException>(() => StorageService.CreateBlobAsync(
            upload,
            new StorageStrategyDto(StorageStrategy.Name, null)));
    }

    [Fact]
    public async Task WritingToExistingBlobUpdatesMetadata()
    {
        var entry = Context.Add(CreateBlob());
        await Context.SaveChangesAsync();

        var blob = await StorageService.WriteBlobAsync(
            entry.Entity.Id,
            new MockUploadedBlob("update.txt", "text/mock", "Updated content"));

        Assert.NotNull(blob);
        Assert.Equal("update.txt", blob.Name);
        Assert.Equal("text/mock", blob.MimeType);
        Assert.Equal(DateTime.MinValue, blob.Created);
        Assert.Equal(DateTime.UtcNow, blob.Modified, TimeSpan.FromSeconds(1));
        Assert.Equal(Encoding.UTF8.GetBytes("Updated content"), StorageProvider.WrittenData);
    }

    [Fact]
    public async Task WritingToUnknownBlobReturnsNull()
    {
        Assert.Null(await StorageService.WriteBlobAsync(Guid.NewGuid(), new MockUploadedBlob("", "", "")));
    }

    [Fact]
    public async Task DeletionGoesThroughToProviderCorrectly()
    {
        var entry = Context.Add(CreateBlob());
        await Context.SaveChangesAsync();

        StorageProvider.DeletesSuccessfully = false;
        await Assert.ThrowsAsync<InvalidOperationException>(() => StorageService.DeleteBlobAsync(entry.Entity));
        Assert.NotNull(Context.Blobs.SingleOrDefault(e => e.Id == entry.Entity.Id));

        StorageProvider.DeletesSuccessfully = true;
        await StorageService.DeleteBlobAsync(entry.Entity);
        Assert.Null(Context.Blobs.SingleOrDefault(e => e.Id == entry.Entity.Id));
    }

    [Fact]
    public async Task OrphanedBlobLeavesUnknownStorageStrategiesAlone()
    {
        var blob = Context.Add(CreateBlob()).Entity;
        blob.StorageStrategy.Name = "Unknown";
        await Context.SaveChangesAsync();

        await StorageService.CleanupOrphanedBlobsAsync();

        Assert.NotNull(await Context.Blobs.FindAsync(new object[] { blob.Id }));
    }

    [Fact]
    public async Task OrphanedBlobCleanupDeletesInvalidBlobs()
    {
        var valid = Context.Add(CreateBlob()).Entity;
        var invalid = Context.Add(CreateBlob()).Entity;
        await Context.SaveChangesAsync();

        StorageStrategy.StaticValidBlobIds = new[] { valid.Id };
        await StorageService.CleanupOrphanedBlobsAsync();

        Assert.NotNull(await Context.Blobs.FindAsync(new object[] { valid.Id }));
        Assert.Null(await Context.Blobs.FindAsync(new object[] { invalid.Id }));
    }

    private OrchestrateBlob CreateBlob(bool canRead = true, bool canWrite = true) => new OrchestrateBlob
    {
        MimeType = "text/plain",
        Name = Guid.NewGuid().ToString(),
        StorageStrategy = new OrchestrateBlobStorageStrategyData
        {
            Name = StorageStrategy.Name,
            AccessData = JsonSerializer.SerializeToDocument(new MockAccessData(canRead, canWrite)),
        },
    };
}
