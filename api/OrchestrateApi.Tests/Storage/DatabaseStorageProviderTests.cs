using System.Text;
using System.Text.Json;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Storage;

namespace OrchestrateApi.Tests.Storage;

public class DatabaseStorageProviderTests : AutoRollbackTestHarness
{
    private readonly DatabaseStorageProvider provider;

    public DatabaseStorageProviderTests()
    {
        this.provider = new DatabaseStorageProvider(Context);
    }

    [Fact]
    public async Task DataIsHandledOverLifecycleAsExpected()
    {
        var entry = Context.Add(new OrchestrateBlob
        {
            Name = "test.txt",
            MimeType = "text/plain",
            StorageStrategy = new OrchestrateBlobStorageStrategyData { Name = "None" },
        });
        await Context.SaveChangesAsync();

        var blob = entry.Entity;
        using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes("Test content")))
        {
            await this.provider.WriteBlobAsync(blob, memoryStream, default);
        }

        using (var bytes = await this.provider.ReadBlobAsync(blob, default))
        {
            Assert.NotNull(bytes);
            using var memoryStream = new MemoryStream();
            await bytes.Stream.CopyToAsync(memoryStream);
            Assert.Equal("Test content", Encoding.UTF8.GetString(memoryStream.ToArray()));
        }

        Assert.True(await this.provider.DeleteBlobAsync(blob, default));

        // This provider doesn't actually delete the content on a deletion request
        using (var bytes = await this.provider.ReadBlobAsync(blob, default))
        {
            Assert.NotNull(bytes);
            using var memoryStream = new MemoryStream();
            await bytes.Stream.CopyToAsync(memoryStream);
            Assert.Equal("Test content", Encoding.UTF8.GetString(memoryStream.ToArray()));
        }

        Context.Remove(blob);
        await Context.SaveChangesAsync();

        // Because it get's deleted with the entity
        using (var bytes = await this.provider.ReadBlobAsync(blob, default))
        {
            Assert.Null(bytes);
        }
    }
}
