namespace OrchestrateApi.Tests
{
    public partial class OrchestrateSeed
    {
        public const string GroupName = "Motley Crue";
        public const string GroupAbbr = "mc";
        public const string Website = "https://motley.com";
        public const string BobMarleyEmail = "bmarley@domain.com";
        public const string GlennStevensEmail = "gstevens@domain.com";
        public const string IggyPopEmail = "iggy@pop.com";

        public const string ConcertBandName = "Concert Band";
    }
}
