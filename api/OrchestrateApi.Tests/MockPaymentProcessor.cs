using OrchestrateApi.Common;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.PaymentProcessor;
using Stripe;

namespace OrchestrateApi.Tests;

public class MockPaymentProcessor : IPaymentProcessor
{
    public const string MockCustomerId = "cust_MOCK";
    public const string MockSubscriptionId = "sub_MOCK";
    public const string MockPaymentMethodId = "pm_MOCK";

    public Customer? Customer { get; set; }
    public Subscription? Subscription { get; set; }
    public IList<Invoice> PaidInvoices { get; set; } = Array.Empty<Invoice>();
    public Invoice? UpcomingInvoice { get; set; }
    public PaymentMethod? PaymentMethod { get; set; }
    public Uri CheckoutUriBase { get; set; } = new Uri("https://google.com");

    public ISet<string> NotForProfitAppliedCustomerIds { get; } = new HashSet<string>();


    public Task ApplyNotForProfitDiscountAsync(Customer customer)
    {
        NotForProfitAppliedCustomerIds.Add(customer.Id);
        return Task.CompletedTask;
    }

    public Task<Stripe.BillingPortal.Session> CreateBillingPortalSessionAsync(string customerId, Uri returnUrl)
    {
        throw new NotImplementedException();
    }

    public async Task<Result<Stripe.Checkout.Session>> CreateCheckoutSessionAsync(CheckoutSessionOptions options)
    {
        var builder = new UriBuilder(CheckoutUriBase)
        {
            Query = $"plan={options.Plan.Type}&returnUrl={options.ReturnUrl.AbsoluteUri}",
        };

        return await Task.FromResult(new Stripe.Checkout.Session
        {
            Url = builder.Uri.AbsoluteUri,
        });
    }

    public Task<Customer> CreateCustomerAsync(string name, string email)
    {
        return Task.FromResult(new Customer
        {
            Id = $"cust_{Guid.NewGuid()}",
            Name = name,
            Email = email,
        });
    }

    public Task<Customer?> GetCustomerAsync(string customerId)
        => Task.FromResult(customerId == MockCustomerId ? Customer : null);

    public Task<Customer?> GetCustomerWithSubscriptionsAsync(string customerId)
        => Task.FromResult(customerId == MockCustomerId ? Customer : null);

    public async Task<IList<Invoice>> GetMostRecentPaidInvoicesAsync(string customerId, int last = 12)
        => await Task.FromResult(PaidInvoices.Take(last).ToList());

    public Task<PaymentMethod?> GetPaymentMethodAsync(string paymentMethodId)
        => Task.FromResult(paymentMethodId == MockPaymentMethodId ? PaymentMethod : null);

    public Task<Subscription?> GetSubscriptionAsync(string subscriptionId)
        => Task.FromResult(subscriptionId == MockSubscriptionId ? Subscription : null);

    public Task<Invoice?> GetUpcomingInvoiceAsync(string customerId, string subscriptionId)
        => Task.FromResult(customerId == MockCustomerId ? UpcomingInvoice : null);
}
