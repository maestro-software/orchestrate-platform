using Breeze.Persistence;
using OrchestrateApi.DAL;

namespace OrchestrateApi.Tests.DAL
{
    public class MockTypedEntityInfo<T> : IEntityInfo<T>
    {
        public MockTypedEntityInfo(T entity, EntityState entityState)
        {
            Entity = entity;
            EntityState = entityState;
        }

        public EntityInfo Raw => throw new InvalidOperationException();

        public T Entity { get; set; }

        public EntityState EntityState { get; set; }
        public Dictionary<string, object?> OriginalValuesMap { get; } = new();
        public Dictionary<string, object?> UnmappedValuesMap { get; } = new();
    }
}
