namespace OrchestrateApi.Tests;

public class AutoMapperTests
{
    [Fact]
    public void EnsureAutoMapperProfilesAreValid()
    {
        var profiles = typeof(Startup).Assembly
            .GetTypes()
            .Where(t => typeof(AutoMapper.Profile).IsAssignableFrom(t))
            .Select(t => (AutoMapper.Profile?)Activator.CreateInstance(t));
        var configuration = new AutoMapper.MapperConfiguration(cfg => cfg.AddProfiles(profiles));
        configuration.AssertConfigurationIsValid();
    }
}
